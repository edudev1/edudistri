<?php

namespace Database\Factories;

use App\Models\Transportista;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TransportistaFactory extends Factory
{
    protected $model = Transportista::class;

    public function definition()
    {
        return [
			'ci' => $this->faker->name,
			'nit' => $this->faker->name,
			'nombres' => $this->faker->name,
			'apellidos' => $this->faker->name,
			'num_celular' => $this->faker->name,
			'num_celular_ref' => $this->faker->name,
			'direccion' => $this->faker->name,
			'create_at' => $this->faker->name,
        ];
    }
}

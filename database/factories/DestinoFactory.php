<?php

namespace Database\Factories;

use App\Models\Destino;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DestinoFactory extends Factory
{
    protected $model = Destino::class;

    public function definition()
    {
        return [
			'tipo_destino' => $this->faker->name,
			'nombre_destino' => $this->faker->name,
        ];
    }
}

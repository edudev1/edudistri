<?php

namespace Database\Factories;

use App\Models\Vehiculo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class VehiculoFactory extends Factory
{
    protected $model = Vehiculo::class;

    public function definition()
    {
        return [
			'Placa' => $this->faker->name,
			'Marca' => $this->faker->name,
			'Color' => $this->faker->name,
			'num_chasis' => $this->faker->name,
			'codigo_motor' => $this->faker->name,
			'Capacidad_arrastre' => $this->faker->name,
			'crpva' => $this->faker->name,
			'num_ci_propietario' => $this->faker->name,
			'fecha_registro' => $this->faker->name,
			'tipo_vehiculo_id' => $this->faker->name,
			'transporte_cia_id' => $this->faker->name,
			'transportista_id' => $this->faker->name,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\PedidosDestino;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PedidosDestinoFactory extends Factory
{
    protected $model = PedidosDestino::class;

    public function definition()
    {
        return [
			'fecha_pedido' => $this->faker->name,
			'tipo_pago' => $this->faker->name,
			'cantidad' => $this->faker->name,
			'atendido' => $this->faker->name,
			'destino_id' => $this->faker->name,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\TransporteCia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TransporteCiaFactory extends Factory
{
    protected $model = TransporteCia::class;

    public function definition()
    {
        return [
			'nombre' => $this->faker->name,
			'direccion' => $this->faker->name,
			'num_telefono' => $this->faker->name,
			'email' => $this->faker->name,
			'create_at' => $this->faker->name,
        ];
    }
}

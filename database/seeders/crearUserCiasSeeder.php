<?php

namespace Database\Seeders;

use App\Models\TransporteCia;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class crearUserCiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cias = TransporteCia::all();
        foreach ($cias as $key => $cia) {
            $user = User::where('email', $cia->email)->first();
            if(is_null($user)){
                $user = User::create([
                    'name' => $cia->nombre,
                    'email' => $cia->email,
                    'email_verified_at' => now(),
                    'password' => 'password',

                ])->assignRole('Sindicato');
            }

            $user->transporteCias()->attach($cia);
        }
    }
}

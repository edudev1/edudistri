<?php

namespace Database\Seeders;

use App\Models\Destino;
use App\Models\PedidosDestino;
use App\Models\PedidoVehiculoCarga;
use App\Models\TipoVehiculo;
use DateTime;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class cargarPedidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = database_path('seeders/datos/pedidos.csv');
        // Abrir el archivo CSV en modo lectura
        $handle = fopen($archivo, 'r');
        // Leer la primera línea del archivo (encabezados)
        $encabezados = fgetcsv($handle);
        // Leer el resto del archivo
        $tiposVehiculos = TipoVehiculo::all()->pluck('id','tipo')->toArray();
        // dd($tiposVehiculos);
        $cargasXTipo = [
                'TRAILER'     => 560,
                'DIRECCIONAL' => 400,
                'ALZAPATA'    => 300,
                'SENCILLO'    => 200,
            ];

        while (($data = fgetcsv($handle)) !== false) {
            // Verificar cada campo en la fila actual
            $fila = explode(';',$data[0]);
            // dd($fila);
            $destino = Destino::where('nombre_destino', $fila[1])->first();
            if($destino){
                //crear pedido destino
                $pedidoDestino = $this->crearPedidoDestino($fila[0],$fila[2],$fila[3],$destino->id);
                $tipo = 'TRAILER';
                $this->crearPedidoVehiculoCarga($cargasXTipo[$tipo], $tiposVehiculos[$tipo], $pedidoDestino->id,$fila[4]);
                $tipo = 'DIRECCIONAL';
                $this->crearPedidoVehiculoCarga($cargasXTipo[$tipo], $tiposVehiculos[$tipo], $pedidoDestino->id,$fila[5]);
                $tipo = 'ALZAPATA';
                $this->crearPedidoVehiculoCarga($cargasXTipo[$tipo], $tiposVehiculos[$tipo], $pedidoDestino->id,$fila[6]);
                $tipo = 'SENCILLO';
                $this->crearPedidoVehiculoCarga($cargasXTipo[$tipo], $tiposVehiculos[$tipo], $pedidoDestino->id,$fila[7]);

            }
        }

        // Cerrar el archivo
        fclose($handle);
    }
    public function crearPedidoVehiculoCarga($carga, $tipoVehiculoId, $pedidoDestinoId, $cantidad) {
        // dd([$carga, $tipoVehiculoId, $pedidoDestinoId, $cantidad]);
        for ($i=0; $i < $cantidad; $i++) {
            PedidoVehiculoCarga::create([
                                        'carga_vehiculo' => $carga,
                                        'pedidos_destino_id' => $pedidoDestinoId,
                                        'tipo_vehiculo_id' => $tipoVehiculoId,
                                        ]);
        }

    }
    public function crearPedidoDestino($fecha, $tipoPago, $cantidad, $destinoId){
        $fecha = DateTime::createFromFormat('d/m/Y', $fecha);
        return PedidosDestino::create([
            'fecha_pedido' => $fecha->format('Y-m-d H:i:s.u'),
            'tipo_pago' => $tipoPago,
            'cantidad' => $cantidad,
            'destino_id' => $destinoId,
        ]);
    }
}

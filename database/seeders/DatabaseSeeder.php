<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->truncateTablas([
            'model_has_permissions'
            ,'model_has_roles'
            ,'role_has_permissions'
            ,'roles'
            ,'permissions'
            ,'users'
            ,'datos'
            ,'user_transporte_cias'
            ,'transportistas'
            ,'transporte_cias'
            ,'tipo_vehiculos'
            ,'vehiculos'
            ,'destinos'
            ,
            'programaciones'
            ,'pedidos_vehiculos_carga'
            ,'pedidos_destino'
            ,'conocimiento_cargas'
            ,'cias_habilitar_placas'
            ,'colas'
        ]);

        $this->call([
            RoleSeeder::class,
            UsersSeeder::class,
            DatosSeeder::class,
            cargarTipoVehiculoSeeder::class,
            cargarCiasSeeder::class,
            cargarVehiculosTransportistaSeeder::class,
            crearUserCiasSeeder::class,
            cargarTiposDestinoSeeder::class,
        ]);

    }
    protected function truncateTablas(array $tablas)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}

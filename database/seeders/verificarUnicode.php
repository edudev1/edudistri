<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class verificarUnicode extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = database_path('seeders/datos/errores.csv');
        $rutaError = database_path('seeders/datos/errores_sql.txt');
        // Abrir el archivo CSV en modo lectura
        $handle = fopen($archivo, 'r');
        // Leer la primera línea del archivo (encabezados)
        $encabezados = fgetcsv($handle);

        // Campos que pueden generar el error
        $camposInvalidos = [];

        // Leer el resto del archivo
        while (($data = fgetcsv($handle)) !== false) {
            // Verificar cada campo en la fila actual
            foreach ($data as $indice => $valor) {
                // Intentar convertir el valor a UTF-8
                $valorUtf8 = mb_convert_encoding($valor, 'UTF-8', 'UCS-2');

                // Verificar si hay un error de conversión
                if ($valor !== $valorUtf8) {
                    $campo = $encabezados[$indice];
                    $camposInvalidos[] = $campo. ' ' . $valor . PHP_EOL;
                }
            }
        }

        // Cerrar el archivo
        fclose($handle);

        // Mostrar los campos inválidos
        if (!empty($camposInvalidos)) {
            echo "Los siguientes campos contienen caracteres inválidos para SQL Server: ";

            $this->escribirArrayEnArchivo($rutaError,$camposInvalidos);
        } else {
            echo "No se encontraron campos inválidos.";
        }
    }

    function escribirArrayEnArchivo($ruta, $array) {
        // Convertir el array a una cadena de texto
        $texto = implode(",", $array);
        $texto = $texto . PHP_EOL;
        // Escribir el contenido en el archivo al final del archivo
        file_put_contents($ruta, $texto, FILE_APPEND);

        // Comprobar si se ha escrito correctamente
        if (file_exists($ruta)) {
            echo "El archivo se ha creado correctamente.\n";
        }
    }
}

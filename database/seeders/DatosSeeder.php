<?php

namespace Database\Seeders;

use App\Models\Dato;
use Illuminate\Database\Seeder;

class DatosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $meses = ['Ninguno', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        // foreach ($meses as $mes) {
        //     Dato::create([
        //         'tipo' => 'meses',
        //         'valor' => $mes,
        //     ]);
        // }

        $departamentos = ['BEN', 'CBB', 'CHU', 'LPB', 'ORO', 'PAN', 'PTS', 'SRZ', 'TJA'];
        foreach ($departamentos as $deparamento) {
            Dato::create([
                'tipo' => 'departamentos',
                'valor' => $deparamento,
            ]);
        }

        $tiposDestinos = ['LS', 'LI', 'LP'];
        foreach ($tiposDestinos as $tipodestino) {
            Dato::create([
                'tipo' => 'tipo-destino',
                'valor' => $tipodestino,
            ]);
        }
        $tipoColas = ['UVS', 'UVI', 'UVP'];
        foreach ($tipoColas as $cola) {
            Dato::create([
                'tipo' => 'tipo-cola',
                'valor' => $cola,
            ]);
        }
        $tipoPagos = ['PS', 'PA'];
        foreach ($tipoPagos as $pago) {
            Dato::create([
                'tipo' => 'tipo-pago',
                'valor' => $pago,
            ]);
        }
    }
}

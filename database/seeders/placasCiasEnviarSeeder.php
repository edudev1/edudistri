<?php

namespace Database\Seeders;

use App\Models\CiasHabilitarPlaca;
use App\Models\Vehiculo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class placasCiasEnviarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cias = DB::table('transporte_cias as tc')
                ->select('tc.id','tc.nombre', DB::raw('count(*) as total'))
                ->join('vehiculos as v', 'v.transporte_cia_id', '=', 'tc.id')
                ->groupBy('tc.id','tc.nombre')
                ->orderBy('total')
                ->get();

        foreach ($cias as $sindicato) {
            $cantPlacas = ceil($sindicato->total * 0.1);
            // echo $cantPlacas.'   ';
            $selectPlacas = [];
            for ($i=0; $i < $cantPlacas; $i++) {
                $placasIds = Vehiculo::where('transporte_cia_id', $sindicato->id)->select('id','placa','transporte_cia_id')->get()->toArray();
                // dd($placasIds);
                $indice = array_rand($placasIds);
                $selectPlacas[] = $placasIds[$indice];
                unset($placasIds[$indice]);
                $placasIds = array_values($placasIds);
            }
            $this->habilitarPlaca($selectPlacas);
        }
    }

    function habilitarPlaca($selectPlacas) {
        foreach ($selectPlacas as $value) {
            $existe = CiasHabilitarPlaca::where('placa', $value['placa'])->where('programado', 0)->get();
            // dd(count($existe));
            if(count($existe)>0){
                continue;
            }
            CiasHabilitarPlaca::create([
                'placa' => $value['placa'],
                'cia_id'=>$value['transporte_cia_id'],
                'enviado' => '1',
            ]);
        }
    }
}

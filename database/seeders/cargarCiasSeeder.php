<?php

namespace Database\Seeders;

use App\Models\TransporteCia;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class cargarCiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = fopen(database_path('seeders/datos/sindicatos_datos.csv'), 'r');
        $salta = true;
        while (($fila = fgetcsv($archivo)) !== false) {
            if ($salta) {
                $salta = false;
                continue;
            }
            $fila = explode(';',$fila[0]);
            // print_r($fila);
            TransporteCia::create([
                    'nombre' => $fila[0],
                    'direccion' => $fila[1],
                    'num_telefono' => $fila[2],
                    'email' => $fila[3],
                ]);
        }
        fclose($archivo);
    }
}

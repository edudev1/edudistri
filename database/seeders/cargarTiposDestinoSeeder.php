<?php

namespace Database\Seeders;

use App\Models\Destino;
use App\Models\TipoVehiculo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class cargarTiposDestinoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = fopen(database_path('seeders/datos/tipos_destino.csv'), 'r');
        $salta = true;
        while (($fila = fgetcsv($archivo)) !== false) {
            if ($salta) {
                $salta = false;
                continue;
            }
            $fila = explode(';',$fila[0]);
            // print_r($fila);
            // dd($fila);
            Destino::create([
                    'tipo_destino' => $fila[2],
                    'nombre_destino' => $fila[0],
                    'departamento' => $fila[1],
                ]);
        }
        fclose($archivo);
    }
}

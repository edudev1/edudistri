<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => 'admin123',

        ])->assignRole('Administrador');
        User::create([
            'name' => 'Operador',
            'email' => 'operador@gmail.com',
            'email_verified_at' => now(),
            'password' => 'admin123',

        ])->assignRole('Operador');
        User::create([
            'name' => 'Auxiliar',
            'email' => 'auxiliar@gmail.com',
            'email_verified_at' => now(),
            'password' => 'admin123',

        ])->assignRole('Auxiliar');
        User::create([
            'name' => 'Sindicato',
            'email' => 'sindicato@gmail.com',
            'email_verified_at' => now(),
            'password' => 'admin123',

        ])->assignRole('Sindicato');
    }
}

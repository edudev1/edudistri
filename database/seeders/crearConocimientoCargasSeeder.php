<?php

namespace Database\Seeders;

use App\Models\CiasHabilitarPlaca;
use App\Models\Cola;
use App\Models\ConocimientoCarga;
use App\Models\Destino;
use App\Models\Vehiculo;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class crearConocimientoCargasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $placasHabilitadas = CiasHabilitarPlaca::where('programado', 0)
                            ->where('enviado', 1)
                            ->get();
        $destinos = Destino::all()->toArray();
        foreach ($placasHabilitadas as $value) {
            $vehiculo = Vehiculo::where('placa', $value->placa)->where('activo', '1')->first();

            if($this->verificarSiExisteEnCola($vehiculo->id)){
                continue;
            }
            $indice = array_rand($destinos);
            $ultimoDestino = $destinos[$indice];
            $tipoCola = config('app.cola_destino');
            // dd( $tipoCola[$ultimoDestino['tipo_destino']]);
            DB::beginTransaction();
            try {
                $fechaCarguio = Carbon::now()->subDays(rand(1,50));
                $conocimientoCarga = ConocimientoCarga::create([
                    'numero_carga' => rand(1000, 1000000),
                    'placa' => $vehiculo->placa,
                    'fecha_carguio' => $fechaCarguio,
                    'habilitado' => 1,
                    'ultimo_destino_id' => $ultimoDestino['id'],
                    'cias_habilitar_placas_id' => $value->id,
                ]);
                //crear registro de cola
                $cola = Cola::create([
                    'tipo_cola' => $tipoCola[$ultimoDestino['tipo_destino']],
                    'fecha' => $fechaCarguio,
                    'vehiculo_id' => $vehiculo->id,
                    'conocimiento_carga_id' => $conocimientoCarga->id,
                ]);
                $value->programado = 1;
                $value->save();
                DB::commit();
                echo '\n placa: '.$vehiculo->placa ;
            } catch (\Exception $e) {
                // En caso de error, deshacemos la transacción
                DB::rollback();
                echo $e->getMessage();

            }
        }
    }

    function verificarSiExisteEnCola($vehiculoId){
        $result = Cola::where('vehiculo_id', $vehiculoId)
            ->where('asignado', '0')
            ->get();
        if(count($result)>1){
            return true;
        }
        return false;
    }
}

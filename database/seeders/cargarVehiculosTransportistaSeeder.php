<?php

namespace Database\Seeders;

use App\Models\TipoVehiculo;
use App\Models\TransporteCia;
use App\Models\Transportista;
use App\Models\Vehiculo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class cargarVehiculosTransportistaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = fopen(database_path('seeders/datos/datos_vehiculo_cia_conductor.csv'), 'r');
        $rutaError = database_path('seeders/datos/errores.txt');
        $salta = true;
        $sindicatos = TransporteCia::all()->pluck('id','nombre')->toArray();
        $tiposVehiculo = TipoVehiculo::all()->pluck('id','tipo')->toArray();
        while (($fila = fgetcsv($archivo)) !== false) {
            if ($salta) {
                $salta = false;
                continue;
            }
            DB::beginTransaction();
            try {
                $fila = explode(';',$fila[0]);
                //buscar transportista por ci vehiculo
                $tranportista = Transportista::where('ci', $fila[10])->first();
                if(is_null($tranportista)){
                    //crear un nuevo transportista
                    $tranportista = Transportista::create([
                        'ci' => $fila[10],
                        'nit' => $fila[11],
                        'nombres' => mb_convert_encoding($fila[12], 'ISO-8859-1', 'UTF-8'),
                        'apellidos' => mb_convert_encoding($fila[13], 'ISO-8859-1', 'UTF-8'),
                        'num_celular' => $fila[14],
                        'num_celular_ref' => $fila[15],
                        'direccion' => mb_convert_encoding($fila[16], 'ISO-8859-1', 'UTF-8'),
                    ]);
                }

                //creamos un vehiculo
                Vehiculo::create([
                    'placa' => $fila[1],
                    'marca' => $fila[2],
                    'color' => $fila[3],
                    'num_chasis' => $fila[4],
                    'codigo_motor' => $fila[5],
                    'capacidad_arrastre' => $fila[6],
                    'crpva' => $fila[7],

                    'tipo_vehiculo_id' => $tiposVehiculo[$fila[8]],
                    'transporte_cia_id' => $sindicatos[$fila[9]],
                    'transportista_id' => $tranportista->id,
                ]);
                DB::commit();
            } catch (\Exception $e) {
                // En caso de error, deshacemos la transacción
                DB::rollback();
                $fila['error'] = $e->getMessage();
                $this->escribirArrayEnArchivo($rutaError,$fila);
            }

        }
        fclose($archivo);
    }

    function escribirArrayEnArchivo($ruta, $array) {
        // Convertir el array a una cadena de texto
        $texto = implode(",", $array);
        $texto = $texto . PHP_EOL;
        // Escribir el contenido en el archivo al final del archivo
        file_put_contents($ruta, $texto, FILE_APPEND);

        // Comprobar si se ha escrito correctamente
        if (file_exists($ruta)) {
            echo "El archivo se ha creado correctamente.\n";
        }
    }
}

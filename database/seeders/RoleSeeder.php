<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $roleAdmin = Role::create(['name' => 'Administrador']);
        // $roleOpe = Role::create(['name' => 'Operador']);
        // $roleAux = Role::create(['name' => 'Auxiliar']);
        // $roleSindi = Role::create(['name' => 'Sindicato']);

        $roleAdmin = Role::where('name', 'Administrador')->first();
        $roleOpe = Role::where('name', 'Operador')->first();
        $roleAux = Role::where('name', 'Auxiliar')->first();
        // // $roleSindi = Role::where('name', 'Sindicato')->first();

        // Permission::create(['name' => 'admin.access'])->syncRoles([$roleAdmin]);
        // Permission::create(['name' => 'eperador.access'])->syncRoles([$roleOpe]);
        // Permission::create(['name' => 'auxiliar.access'])->syncRoles([$roleAux]);
        // Permission::create(['name' => 'sindicato.access'])->syncRoles([$roleSindi]);

        /** permisos para admin y operadores */
        // Permission::create(['name' => 'trasnportista.control'])->syncRoles([$roleAdmin, $roleOpe]);
        // Permission::create(['name' => 'vehiculos.control'])->syncRoles([$roleAdmin, $roleOpe]);
        // Permission::create(['name' => 'pedidos.control'])->syncRoles([$roleAdmin, $roleOpe]);
        // Permission::create(['name' => 'reportes.aux.control'])->syncRoles([$roleAdmin, $roleOpe]);
        // Permission::create(['name' => 'programacion.aux.control'])->syncRoles([$roleAdmin, $roleOpe]);

        /**permisos para colas */
        // Permission::create(['name' => 'ver.colas'])->syncRoles([$roleAdmin, $roleOpe, $roleAux]);
        // Permission::create(['name' => 'revisar.placas'])->syncRoles([$roleAux]);

        /** Permisos para reportes auxiliar */
        Permission::create(['name' => 'rpt.ver']);
        Permission::create(['name' => 'rpt.conocimientos.cargas']);

        $roleAdmin->syncPermissions(['admin.access',
                                   'trasnportista.control',
                                   'vehiculos.control',
                                   'pedidos.control',
                                   'reportes.aux.control',
                                   'programacion.aux.control',
                                   'ver.colas',
                                   'rpt.ver',
                                   ]);
        $roleOpe->syncPermissions(['eperador.access',
                                   'trasnportista.control',
                                   'vehiculos.control',
                                   'pedidos.control',
                                   'reportes.aux.control',
                                   'programacion.aux.control',
                                   'ver.colas',
                                   'rpt.ver',
                                   ]);
        $roleAux->syncPermissions(['auxiliar.access',
                                   'trasnportista.control',
                                   'vehiculos.control',
                                   'ver.colas',
                                   'revisar.placas',
                                   'rpt.ver',
                                   'rpt.conocimientos.cargas',
                                ]);
    }
}

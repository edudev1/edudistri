<?php

namespace Database\Seeders;

use App\Models\TipoVehiculo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class cargarTipoVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $archivo = fopen(database_path('seeders/datos/tipo_vehiculo.csv'), 'r');
        $salta = true;
        while (($fila = fgetcsv($archivo)) !== false) {
            if ($salta) {
                $salta = false;
                continue;
            }
            $fila = explode(';',$fila[0]);
            // print_r($fila);
            TipoVehiculo::create([
                    'tipo' => $fila[0],
                    'descripcion' => $fila[1],
                ]);
        }
        fclose($archivo);
    }
}

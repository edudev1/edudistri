<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('conocimiento_cargas_jde', function (Blueprint $table) {
            $table->id();
            $table->integer('numero_carga');
            $table->string('ciudad');
            $table->date('fecha');
            $table->string('placa');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('conocimiento_cargas_jde');
    }
};

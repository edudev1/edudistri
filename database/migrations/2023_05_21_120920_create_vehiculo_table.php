<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placa')->unique();
            $table->string('marca')->nullable();
            $table->string('color')->nullable();
            $table->string('num_chasis')->nullable();
            $table->string('codigo_motor')->nullable();
            $table->integer('capacidad_arrastre');
            $table->string('crpva')->nullable();
            $table->string('num_ci_propietario')->nullable();
            $table->dateTime('fecha_registro',4)->useCurrent();
            $table->boolean('activo')->default('1');
            $table->unsignedBigInteger('tipo_vehiculo_id');
            $table->unsignedBigInteger('transporte_cia_id');
            $table->unsignedBigInteger('transportista_id');
            $table->foreign('tipo_vehiculo_id')->references('id')->on('tipo_vehiculos');
            $table->foreign('transporte_cia_id')->references('id')->on('transporte_cias');
            $table->foreign('transportista_id')->references('id')->on('transportistas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehiculos');
    }
};

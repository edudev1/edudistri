<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('colas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo_cola'); //UVI, UVS, UVP
            $table->dateTime('fecha',4);
            $table->boolean('asignado')->default(0);
            $table->boolean('preasignado')->default(0);
            $table->unsignedBigInteger('vehiculo_id');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('colas');
    }
};

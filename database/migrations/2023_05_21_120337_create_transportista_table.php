<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transportistas', function (Blueprint $table) {
            $table->id();
            $table->string('ci')->unique();
            $table->string('nit')->nullable();
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('num_celular')->nullable();
            $table->string('num_celular_ref')->nullable();
            $table->string('direccion')->nullable();
            // $table->dateTime('created_at',4)->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transportistas');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('programaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',50);
            $table->dateTime('fecha',4);
            $table->integer('cantidad');
            $table->string('tipo_asignacion')->default('Normal');//asignacion normal,por complemento
            $table->string('motivo_anulado')->nullable();
            $table->boolean('anulado')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_update_id')->nullable();
            $table->unsignedBigInteger('pedidos_vehiculo_carga_id');
            $table->unsignedBigInteger('vehiculo_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
            $table->foreign('pedidos_vehiculo_carga_id')->references('id')->on('pedidos_vehiculos_carga');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('programaciones');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cias_habilitar_placas', function (Blueprint $table) {
            $table->unsignedBigInteger('cia_id')->nullable();
            $table->foreign('cia_id')->references('id')->on('transporte_cias');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cias_habilitar_placas', function (Blueprint $table) {
            $table->dropForeign(['cia_id']);
        });
    }
};

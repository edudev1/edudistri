<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('colas', function (Blueprint $table) {
            $table->string('observacion', 200)->nullable()->after('preasignado');
            $table->unsignedBigInteger('conocimiento_carga_id')->nullable();
            $table->foreign('conocimiento_carga_id')->references('id')->on('conocimiento_cargas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('colas', function (Blueprint $table) {
            $table->dropForeign(['conocimiento_carga_id']);
            $table->dropColumn(['conocimiento_carga_id', 'observacion']);
        });
    }
};

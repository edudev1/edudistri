<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pedidos_vehiculos_carga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('carga_vehiculo');
            $table->boolean('asignado')->default(0);
            $table->boolean('complemento')->default(0);
            $table->boolean('publicar')->default(0);//1 paraque vean los sindicatos
            $table->unsignedBigInteger('pedidos_destino_id');
            $table->unsignedBigInteger('tipo_vehiculo_id');
            $table->foreign('pedidos_destino_id')->references('id')->on('pedidos_destino');
            $table->foreign('tipo_vehiculo_id')->references('id')->on('tipo_vehiculos');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pedidos_vehiculos_carga');
    }
};

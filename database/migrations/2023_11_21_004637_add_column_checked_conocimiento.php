<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cias_habilitar_placas', function (Blueprint $table) {
            $table->boolean('conocimiento_enviado')->after('fecha')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cias_habilitar_placas', function (Blueprint $table) {
            $table->dropColumn(['conocimiento_enviado']);
        });
    }
};

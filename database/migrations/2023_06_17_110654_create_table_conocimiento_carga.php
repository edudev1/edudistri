<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('conocimiento_cargas', function (Blueprint $table) {
            $table->id();
            $table->integer('numero_carga');
            $table->string('placa');
            $table->date('fecha_carguio');
            $table->boolean('habilitado')->default(false)->comment('operador habilita placa');
            $table->boolean('carga_asignada')->default(false)->comment('Cuando el vehiculo es asignado con carga');
            $table->unsignedBigInteger('ultimo_destino_id');
            $table->unsignedBigInteger('cias_habilitar_placas_id');

            $table->foreign('ultimo_destino_id')->references('id')->on('destinos');
            $table->foreign('cias_habilitar_placas_id')->references('id')->on('cias_habilitar_placas');

            $table->timestamps();

            // $table->timestamp('created_at',4);
            // $table->timestamp('updated_at',4);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('conocimiento_cargas');
    }
};

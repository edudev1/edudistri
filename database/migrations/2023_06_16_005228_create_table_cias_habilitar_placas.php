<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cias_habilitar_placas', function (Blueprint $table) {
            $table->id();
            $table->string('placa','20');
            $table->boolean('enviado')->default(0);
            $table->boolean('programado')->default(0);
            $table->timestamp('fecha',4)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cias_habilitar_placas');
    }
};

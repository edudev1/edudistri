<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pedidos_destino', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_pedido',4);
            $table->string('tipo_pago');//PS, PA
            $table->integer('cantidad');
            $table->boolean('atendido')->default(0);
            $table->unsignedBigInteger('destino_id');
            $table->foreign('destino_id')->references('id')->on('destinos');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pedidos_destino');
    }
};

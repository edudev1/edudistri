<?php

use App\Http\Controllers\Auxiliar\AuxiliarController;
use App\Http\Controllers\Auxiliar\ConformidadCargaController;
use App\Http\Controllers\Cias\CiasController;
use App\Http\Controllers\DevController;
use App\Http\Controllers\ExcelExport\AdminExcelController;
use App\Http\Controllers\ExcelExport\UserExcelController;
use App\Http\Controllers\ImportJdeController;
use App\Http\Controllers\Operador\OperadorController;
use App\Http\Controllers\Reportes\ReportesController;
use App\Http\Controllers\Reportes\ReportesProgramacionController;
use App\Http\Controllers\UtilPlacasController;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
/** rutas para desarrollo
 * se borran al final
 */
Route::post('borrando/datos/tablas', [DevController::class, 'borrarDatosTablas'])->name('dev.borrar.datos');

/** fin rutas desarrollo */


Route::get('/', function () {
    return view('inicio');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
//Route Hooks - Do not delete//
Route::prefix('sdc')->middleware('auth')->group(function(){
    Route::view('pedidos/destino', 'livewire.pedidos-destinos.index')->name('pedidos.destino.index');
	Route::view('destinos', 'livewire.destinos.index')->name('destinos.index');
    Route::view('vehiculo', 'livewire.vehiculos.index')->name('vehivulos.index');
	Route::view('tipo/vehiculo', 'livewire.tipo-vehiculos.index')->name('tipo.vehiculos.index');
	Route::view('companias/transporte', 'livewire.transporte-cias.index')->name('cia.ransporte.index');
	Route::view('transportistas', 'livewire.transportistas.index')->name('transportistas.index');
	Route::view('datos', 'livewire.datos.index')->name('datos.index');
});

/**rutas para usuarios de cias */
Route::prefix('sdc/transportistas')->middleware('auth')->group(function(){
    Route::get('habilitar/placas', [CiasController::class, 'index'] )->name('cias.placas.index');
    Route::get('historial/envios', [CiasController::class, 'historialEnvios'] )->name('cias.historial.envios');
    Route::post('buscar/historial/envios', [CiasController::class, 'historialEnviosBuscar'] )->name('cias.buscar.historial.envios');
    Route::get('habilitar/placas/complementos', [CiasController::class, 'verVomplementos'] )->name('cias.ver.complementos');
    Route::get('ver/placas/lista/habilitadas', [CiasController::class, 'verPlacasHabilitadas'] )->name('cias.ver.placas.habilitadas');
    Route::post('ver/placas/lista/habilitadass', [CiasController::class, 'buscarPlacasHabilitadas'] )->name('cias.buscar.placas.habilitadas');
});
/***rutas para auxilar */
Route::prefix('sdc/auxiliar')->middleware('auth')->group(function(){
    Route::get('habilitar/placas', [AuxiliarController::class, 'index'] )->name('aux.placas.index');
    Route::get('habilitar/placas/compania/{cia}', [AuxiliarController::class, 'verPlacasCompania'] )->name('aux.placas.compania');
    Route::get('colas/vehiculos', [AuxiliarController::class, 'colas'] )->name('aux.colas.index');
    Route::post('colas/eliminar/{cola}', [AuxiliarController::class, 'deletePlacaCola'] )->name('aux.colas.delete.placa');
    Route::post('colas/confirmar/placa/envida', [AuxiliarController::class, 'confirmarPlacaEnviada'] )->name('aux.colas.confirmar.placa');
    Route::post('colas/elimiar/placa/lista/enviados', [AuxiliarController::class, 'eliminarPlacaEnviada'] )->name('aux.colas.eliminar.placa');
});

/***rutas para operador */
Route::prefix('sdc/operador')->middleware('auth')->group(function(){
    Route::get('programacion/placas', [OperadorController::class, 'programacion'] )->name('ope.programacion');
    Route::post('generar/programacion', [OperadorController::class, 'generarProgramacion'] )->name('ope.generar.programacion');
    Route::get('ver/programacion/{fecha}', [OperadorController::class, 'verProgramacion'] )->name('ope.ver.programacion');
    Route::get('ver/complemento/{fecha}', [OperadorController::class, 'verComplemento'] )->name('ope.ver.complemento');
    Route::get('reportes', [OperadorController::class, 'reportes'] )->name('ope.reportes');
    Route::post('complemento/publicar', [OperadorController::class, 'publicarComplemento'] )->name('ope.publicar.complemento');
    Route::get('complemento/publicados', [OperadorController::class, 'verComplementoPublicado'] )->name('ope.ver.complemento.publicado');
});

/**rutas para reportes */
Route::prefix('sdc/reportes')->middleware('auth')->group(function(){
    Route::get('programacion', [ReportesController::class, 'programacion'] )->name('rpt.programacion');
    Route::get('colas/{tipo}', [ReportesController::class, 'colasTipo'] )->name('rpt.colas');
    Route::get('programacion/imprimir/{fecha}', [ReportesController::class, 'imprimirProgramacion'] )->name('rpt.impr.programacion');
    Route::get('programacion/excel/{fecha}', [ReportesController::class, 'programacionExcel'] )->name('rpt.excel.programacion');
    Route::get('programacion/imprimir/sindicatos/{fecha}', [ReportesController::class, 'imprimirProgramacionDetalle'] )->name('rpt.impr.programacion.sindicato');
    Route::get('pedidos/pendientes', [ReportesController::class, 'imprimirPedidosPendientes'] )->name('rpt.impr.pedidos.pendientes');
});

    Route::view('pedidos_destino', 'livewire.pedidos_destino.index')->middleware('auth');
	Route::view('destinos', 'livewire.destinos.index')->middleware('auth');
/**rutas para importar desde excel */
Route::prefix('sdc/import/excel')->middleware('auth')->group(function(){
    Route::get('conocimiento/carga/jde', [ImportJdeController::class, 'importConocimientoJdeShow'] )->name('import.excel.conocimiento.jde.show');
    Route::post('conocimiento/carga/jde', [ImportJdeController::class, 'importConocimientoJdeStore'] )->name('import.excel.conocimiento.jde.store');
});

/** ver reporte de placas */
Route::prefix('sdc/')->middleware('auth')->group(function(){
    Route::get('buscar/viajes/placa', [UtilPlacasController::class, 'buscarViajesPlaca'] )->name('buscar.viajes.placa');
});
/** rutas para reportes */
Route::prefix('sdc/reportes/')->name('rpt.')->middleware('auth')->group(function(){
    Route::get('programacion/sindicatos', [ReportesProgramacionController::class, 'programacionesXSindicatoView'] )->name('programacion.cias.show');
    Route::post('programacion/sindicatos', [ReportesProgramacionController::class, 'programacionesXSindicato'] )->name('programacion.cias');
});

/**rutas para excel */
Route::get('/exportar/excel', [UserExcelController::class, 'exportUsers']);
Route::prefix('excel/exportar')->name('excel.exports.')->middleware('auth')->group(function(){
    Route::get('/colas', [AdminExcelController::class, 'exportColas'])->name('colas');
    Route::get('/Pedidos', [AdminExcelController::class, 'exportPedidos'])->name('pedidos');
    Route::get('/placas/habilitadas/sindicatos/{cia_id}', [AdminExcelController::class, 'exportPlacasHabilitadasSindicato'])->name('sindicado.placa.habilitada');
});

/** rutas para ver conocimientos de carga o conformidad */
Route::prefix('sdc/')->middleware('auth')->group(function(){
    Route::get('ver/conformidad', [ConformidadCargaController::class, 'verConformidad'] )->name('ver.conformidad');
    Route::post('ver/conformidad', [ConformidadCargaController::class, 'buscarConformidad'] )->name('buscar.conformidad');
});

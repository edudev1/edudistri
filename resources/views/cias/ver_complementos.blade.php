@extends('adminlte::page')

@section('title', 'Cia transporte')

@section('content_header')
{{-- <div class="btn-group">
    <a href="#" class="btn btn-sm btn-info text-white">Habilitar placas</a>
</div> --}}
<h1>Enviar placas para complementos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @livewire('cias.complemento-vehiculos-requeridos')
        @livewire('cias.enviar-placa-complemento')
    </div>
</div>
@stop

@section('footer')
<span class="mx-2 font-weight-bold">Nombre:</span> {{session()->get('cia_name')}}
<span class="mx-2 font-weight-bold">Telefono:</span> {{session()->get('cia_tel')}}
<span class="mx-2 font-weight-bold">email:</span> {{session()->get('cia_email')}}
<span class="mx-2 font-weight-bold">Dirección:</span> {{session()->get('cia_dir')}}

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

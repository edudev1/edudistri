@extends('adminlte::page')

@section('title', 'Reporte de envios')

@section('content_header')
<div class="my-3">
    <h1>Buscar historial de envio de placas</h1>
</div>
<form action="{{route('cias.buscar.historial.envios')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_ini">De</label>
                <input class="form-control" type="date" name="fecha_ini" id="fecha_ini_id" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_fin">De</label>
                <input class="form-control" type="date" name="fecha_fin" id="fecha_fin_id">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <div class="input-group-append">
                    <button type="submit" class="btn  btn-primary">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</form>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @isset($listaPlacas)
                <table class="table table-sm table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha</th>
                            <th>Placa</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listaPlacas as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->fecha}}</td>
                            <td>{{$row->placa}}</td>
                            <td>{{$row->enviado=='1'?'Enviado':''}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endisset
        </div>
    </div>
</div>
@stop

@section('footer')
<span class="mx-2 font-weight-bold">Nombre:</span> {{session()->get('cia_name')}}
<span class="mx-2 font-weight-bold">Telefono:</span> {{session()->get('cia_tel')}}
<span class="mx-2 font-weight-bold">email:</span> {{session()->get('cia_email')}}
<span class="mx-2 font-weight-bold">Dirección:</span> {{session()->get('cia_dir')}}

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('plugins.inputmask', true)
@section('js')
<script>


</script>
@stop

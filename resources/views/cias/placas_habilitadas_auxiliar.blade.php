@extends('adminlte::page')

@section('title', 'Reporte de envios')

@section('content_header')
<div class="my-3">
    <h1>Ver placas habilitadas por el auxiliar</h1>
</div>
<form action="{{route('cias.buscar.placas.habilitadas')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha">Fecha</label>
                <input class="form-control" type="date" name="fecha" id="fecha_id">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <div class="input-group-append">
                    <button type="submit" class="btn  btn-primary">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</form>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @isset($placasHabilitadas)
                <table class="table table-sm table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha</th>
                            <th>Placa</th>
                            <th>Tipo Vehículo</th>
                            <th>Ultimo Destino</th>
                            <th>Fecha Carguio</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($placasHabilitadas as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->fecha_habilitado}}</td>
                            <td>{{$row->placa}}</td>
                            <td>{{$row->tipo}}</td>
                            <td>{{$row->ultimo_destino}}</td>
                            <td>{{$row->fecha_carguio}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endisset
        </div>
    </div>
</div>
@stop

@section('footer')
<span class="mx-2 font-weight-bold">Nombre:</span> {{session()->get('cia_name')}}
<span class="mx-2 font-weight-bold">Telefono:</span> {{session()->get('cia_tel')}}
<span class="mx-2 font-weight-bold">email:</span> {{session()->get('cia_email')}}
<span class="mx-2 font-weight-bold">Dirección:</span> {{session()->get('cia_dir')}}

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('plugins.inputmask', true)
@section('js')
<script>


</script>
@stop

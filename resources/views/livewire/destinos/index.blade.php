@extends('adminlte::page')

@section('title', 'Destinos')

@section('content_header')
    <h1>Administrar Destinos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @livewire('destinos')
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript">
        window.livewire.on('closeModal', () => {
            $('#createDataModal').modal('hide');
        });
        window.livewire.on('closeModalEdit', () => {
            $('#updateDataModal').modal('hide');
        });
    </script>
@stop

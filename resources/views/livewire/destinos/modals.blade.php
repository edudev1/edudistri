<!-- Add Modal -->
<div wire:ignore.self class="modal fade" id="createDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createDataModalLabel">Crear Destino</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
           <div class="modal-body">
				<form>
                    <div class="form-group">
                        <label for="tipo_destino"></label>
                        {!! html()->select('tipo_destino', $lista_destinos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_destino','id'=>'tipo_destino'])
                                  ->placeholder('Tipo Destino') !!}
                        @error('tipo_destino') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="departamentos"></label>
                        {!! html()->select('departamentos', $listaDepartamentos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'departamentoSelected','id'=>'departamentoSelected'])
                                  ->placeholder('Departamento') !!}
                        @error('departamentoSelected') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="nombre_destino"></label>
                        <input wire:model="nombre_destino" type="text" class="form-control" id="nombre_destino" placeholder="Nombre Destino">@error('nombre_destino') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div wire:ignore.self class="modal fade" id="updateDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Actualizar Destino</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
					<input type="hidden" wire:model="selected_id">
                    <div class="form-group">
                        <label for="tipo_destino"></label>
                        {!! html()->select('tipo_destino', $lista_destinos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_destino','id'=>'tipo_destino'])
                                  ->placeholder('Tipo Destino') !!}
                        @error('tipo_destino') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="departamentos"></label>
                        {!! html()->select('departamentos', $listaDepartamentos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'departamentoSelected','id'=>'departamentoSelected'])
                                  ->placeholder('Departamento') !!}
                        @error('departamentoSelected') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="nombre_destino"></label>
                        <input wire:model="nombre_destino" type="text" class="form-control" id="nombre_destino" placeholder="Nombre Destino">@error('nombre_destino') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary">Guardar</button>
            </div>
       </div>
    </div>
</div>

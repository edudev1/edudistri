<!-- Add Modal -->
@php
$config = [
    'format' => 'DD-MM-YYYY',
    'dayViewHeaderFormat' => 'MMM YYYY',
    'minDate' => "js:moment().startOf('day')",
    'maxDate' => "js:moment().add(20, 'days').endOf('day')",
    'locale' => 'es'
];
@endphp
<div wire:ignore.self class="modal fade" id="createDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createDataModalLabel">Create Pedido</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
           <div class="modal-body">
				<form>
                    <div class="form-group">
                        <input wire:model='keyDestino' type="text" class="form-control dropdown {{$verDropdownList}}" name="searchDestino"
                                                id="searchDestino" placeholder="Destino">
                        @if (!empty($listaDestinos))
                            <div class="dropdown-menu {{$verDropdownList}}" style="top:54px; left:15px" aria-labelledby="dropdownMenuButton">
                                @if ($listaDestinos->count() > 0)
                                    @foreach ($listaDestinos as $resultado)
                                        <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                                {{ $resultado->nombre_completo }}
                                        </a>
                                    @endforeach
                                @else
                                    <span class="mx-2">Sin resultados!!!</span>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="destino_id">Destino</label>
                        <span class="form-control">{{$nombre_completo}} </span>
                        @error('destino_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="fecha_pedido">Fecha Pedido</label>
                        <input class="form-control" type="date" wire:model="fecha_pedido" name="fecha_pedido">
                        @error('fecha_pedido') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    {{-- <x-adminlte-input-date wire:model.defer="fecha_pedido" name="fecha_pedido" label="Fecha Pedido" igroup-size="sm" label-class=""
                    :config="$config" placeholder="Fecha Pedido"
                    onchange="Livewire.emit('setFecha', this.value)"
                    >
                    <x-slot name="appendSlot">
                        <div class="input-group-text bg-dark">
                            <i class="fas fa-calendar-day"></i>
                        </div>
                    </x-slot>
                    </x-adminlte-input-date> --}}
                    <div class="form-group">
                        <label for="tipo_pago">Forma de Pago</label>
                        {!! html()->select('tipo_pago', $litaTipoPago)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_pago','id'=>'tipo_pago'])
                                  ->placeholder('Tipo de pago') !!}
                        @error('tipo_pago') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6 mx-0">
                            <div class="form-group">
                                <label for="cantidad">Cantidad</label>
                                <input wire:model="cantidad" type="number" class="form-control" id="cantidad" placeholder="Cantidad">@error('cantidad') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label for="cantidad">Capacidad Arrastre</label>
                                <span class="form-control form-control-sm">{{$sumaTotal}}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="tipo_vehiculo">Tipo Vehículo</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Trailer</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="trailer" class="form-control form-control-sm" type="number"
                                    id="trailer"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Direccional</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="direccional" class="form-control form-control-sm" type="number"
                                    id="direccional"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Alzapata</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="alzapata" class="form-control form-control-sm" type="number"
                                    id="alzapata"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Sencillo</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="sencillo" class="form-control form-control-sm" type="number"
                                    id="sencillo"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <input class="d-none" type="text" name="sumaTotal" wire:model='sumaTotal'>
                                @error('sumaTotal') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>

                        </div>
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div wire:ignore.self class="modal fade" id="updateDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Actualizar Pedido</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
					<input type="hidden" wire:model="selected_id">
                    <div class="form-group">
                        <label for="destino_id"></label>
                        {!! html()->select('destino_id', $listaDestinos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'destino_id','id'=>'destino_id'])
                                  ->placeholder('Destino') !!}
                        @error('destino_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <x-adminlte-input-date wire:model="fecha_pedido_update" name="fecha_pedido_update" label="Fecha" igroup-size="sm" label-class=""
                    :config="$config" placeholder="Fecha Pedido"
                    onchange="Livewire.emit('setFechaUpdate', this.value)"
                    >
                    <x-slot name="appendSlot">
                        <div class="input-group-text bg-dark">
                            <i class="fas fa-calendar-day"></i>
                        </div>
                    </x-slot>
                    </x-adminlte-input-date>
                    <div class="form-group">
                        <label for="tipo_pago"></label>
                        {!! html()->select('tipo_pago', $litaTipoPago)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_pago','id'=>'tipo_pago'])
                                  ->placeholder('Tipo de pago') !!}
                        @error('tipo_pago') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6 mx-0">
                            <div class="form-group">
                                <label for="cantidad">Cantidad</label>
                                <input wire:model="cantidad" type="number" class="form-control" id="cantidad" placeholder="Cantidad">@error('cantidad') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label for="cantidad">Capacidad Arrastre</label>
                                <span class="form-control form-control-sm">{{$sumaTotal}}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="tipo_vehiculo">Tipo Vehículo</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Trailer</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="trailer" class="form-control form-control-sm" type="number"
                                    id="trailer"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Direccional</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="direccional" class="form-control form-control-sm" type="number"
                                    id="direccional"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Alzapata</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="alzapata" class="form-control form-control-sm" type="number"
                                    id="alzapata"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tipo_vehiculo">Sencillo</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="sencillo" class="form-control form-control-sm" type="number"
                                    id="sencillo"
                                    wire:change='calcularCarga'
                                    >
                                </div>
                            </div>
                            <div class="row">
                                @error('sumaTotal') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary">Guardar</button>
            </div>
       </div>
    </div>
</div>
@push('js')

@endpush

@extends('adminlte::page')

@section('title', 'Pedidos Destino')

@section('content_header')
    <h1>Administrar Pedidos por Destinos</h1>
@stop

@section('content')
<div>
    <a target="_blank" href="{{route('rpt.impr.pedidos.pendientes')}}" class="btn btn-sm btn-info mb-3">Imprimir</a>
</div>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @livewire('pedidos-destinos')
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('plugins.TempusDominusBs4', true)
@section('js')
<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script>

        document.addEventListener('livewire:load', function () {
            window.livewire.on('closeModal', () => {
                $('#createDataModal').modal('hide');
            });
            window.livewire.on('closeModalEdit', () => {
                $('#updateDataModal').modal('hide');
            });
        });
    </script>
@stop

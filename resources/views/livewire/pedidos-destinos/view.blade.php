@section('title', __('Pedidos Destinos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Lista de Pedidos Destino </h4>
						</div>
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							{{-- <input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Pedidos Destino"> --}}
						</div>
						<div class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#createDataModal">
						<i class="fa fa-plus"></i>  Añadir Pedido
						</div>
					</div>
				</div>

				<div class="card-body">
						@include('livewire.pedidos-destinos.modals')
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<td>#</td>
								<th>Destino</th>
								<th>Fecha Pedido</th>
								<th>Cantidad</th>
								<th>Tipo Pago</th>
								<th>Trailers</th>
								<th>Direccionales</th>
								<th>Alzapatas</th>
								<th>Sencillos</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>

							@forelse($listaPedidos as $row)
                            @php
                                // dd(substr($row->fecha_pedido,0,10));
                                $fecha = DateTime::createFromFormat('Y-m-d', substr($row->fecha_pedido,0,10));
                                // dd($row);
                            @endphp
							<tr>
								<td class="text-right">{{ $loop->iteration }}</td>
								<td class="text-left">{{ $row->nombre_destino }}</td>
								<td class="text-right">{{ $fecha->format('d-m-Y') }}</td>
								<td class="text-right">{{ $row->cantidad }}</td>
								<td class="text-center">{{ $row->tipo_pago }}</td>
								<td class="text-right">{{ $row->Trailers }}</td>
								<td class="text-right">{{ $row->Direccionales }}</td>
								<td class="text-right">{{ $row->Alzapatas }}</td>
								<td class="text-right">{{ $row->Sencillos }}</td>
                                <td width="90">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Acciones
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" onclick="confirm('Confirma que desea eliminar el Pedidos? \nUna vez eliminado no se podra recuperar!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->pedido_destino_id}})"><i class="fa fa-trash"></i> Delete </a></li>
                                        </ul>
                                    </div>
                                </td>
							</tr>
							@empty
							<tr>
								<td class="text-center" colspan="100%">Sin datos </td>
							</tr>
							@endforelse
						</tbody>
					</table>

					{{-- <div class="float-end">{{ $listaPedidos->links() }}</div> --}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

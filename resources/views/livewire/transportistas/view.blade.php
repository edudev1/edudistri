@section('title', __('Transportistas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Lista de Transportistas</h4>
						</div>
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Transportista">
						</div>
						<div class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#createDataModal">
						<i class="fa fa-plus"></i>  Nuevo Transportista
						</div>
					</div>
				</div>

				<div class="card-body">
						@include('livewire.transportistas.modals')
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<td>#</td>
								<th>Ci</th>
								<th>Nit</th>
								<th>Nombres</th>
								<th>Apellidos</th>
								<th>Núm Celular</th>
								<th>Núm Ref.</th>
								<th>Dirección</th>
								<th>Fecha Alta</th>
								<td>ACCIONES</td>
							</tr>
						</thead>
						<tbody>
							@forelse($transportistas as $row)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $row->ci }}</td>
								<td>{{ $row->nit }}</td>
								<td>{{ $row->nombres }}</td>
								<td>{{ $row->apellidos }}</td>
								<td>{{ $row->num_celular }}</td>
								<td>{{ $row->num_celular_ref }}</td>
								<td>{{ $row->direccion }}</td>
                                @php
                                    // dd($row->created_at);
                                    // $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $row->created_at);
                                @endphp
								<td>{{ $row->created_at->format('d/m/Y') }}</td>
								<td width="90">
									<div class="dropdown">
										<a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
											Acciones
										</a>
										<ul class="dropdown-menu">
											<li><a data-bs-toggle="modal" data-bs-target="#updateDataModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a></li>
											<li><a class="dropdown-item" onclick="confirm('Confirma eliminar Transportista id {{$row->id}}? \nUna vez borrado no se podra recuparar!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a></li>
										</ul>
									</div>
								</td>
							</tr>
							@empty
							<tr>
								<td class="text-center" colspan="100%">No se encontraron datos </td>
							</tr>
							@endforelse
						</tbody>
					</table>
					<div class="float-end">{{ $transportistas->links() }}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

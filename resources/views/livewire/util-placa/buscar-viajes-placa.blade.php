<div>
    <div class="row mb-3">
        <div class="form-inline">
            <div class="input-group">
                <input wire:model.defer="buscarPlaca" class="form-control" type="search" placeholder="Placa" aria-label="Search">
                <div class="input-group-append">
                    <button wire:click="buscarDestinosPlaca" class="btn btn-primary">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @if ($verResultado)
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h3>Resultados </h3>
                </div>
                <div class="card-body">
                    <div class="table table-striped files">
                        <table>
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <th>Numero Carga</th>
                                    <th>Ciudad</th>
                                    <th>Fecha</th>
                                    <td>Placa</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $destinos
                                @endphp
                                @forelse($destinos as $row)
							<tr>
                                @php
                                    $fecha = DateTime::createFromFormat('Y-m-d', $row->fecha);
                                @endphp
								<td>{{ $loop->iteration }}</td>
								<td>{{ $row->numero_carga }}</td>
								<td>{{ $row->ciudad }}</td>
								<td>{{ $fecha->format('d-m-Y') }}</td>
								<td>{{ $row->placa }}</td>
							</tr>
							@empty
							<tr>
								<td class="text-center" colspan="100%">Sin datos </td>
							</tr>
							@endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endif

</div>

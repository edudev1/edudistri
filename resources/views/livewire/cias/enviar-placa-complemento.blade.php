<div class="col-md-6">
    {{-- Do your work, then step back. --}}
    <div class="card text-left">
        <div class="card-header">
            <h5>Buscar placa y enviar para asignación inmediata</h5>
        </div>
      <div class="card-body">
        <form class="form-horizontal" wire:submit.prevent="enviarPlacaComplemento">
            <div class="form-group row">
                <div class="col-md-6">
                    <input wire:model='keyPlaca' type="text" class="form-control form-control-sm dropdown {{$verDropdownList}}" name="searchPlaca" id="searchPlaca" placeholder="Buscar placa">
                    @if (!empty($listaPlacas))
                        <div class="dropdown-menu {{$verDropdownList}}" style="top:36px; left:7px" aria-labelledby="dropdownMenuButton">
                            @if ($listaPlacas->count() > 0)
                                @foreach ($listaPlacas as $resultado)

                                    <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                            {{ $resultado->placa }}
                                    </a>
                                @endforeach
                            @else
                                <span class="mx-2">Sin resultados!!!</span>
                            @endif

                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <span class="form-control form-control-sm">{{$placaTipo}}</span>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-sm btn-primary">enviar</button>
                </div>
            </div>
        </form>
        @if (session()->has('message'))
                <div wire:poll.6s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
        @endif
      </div>
    </div>
</div>

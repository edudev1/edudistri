<div class="card">
    <div class="card-header">
        <div style="display: flex; justify-content: space-between;">
            <div class="float-left">
                <h4>Lista placas para enviar </h4>
            </div>
            @if (session()->has('message'))
                <div wire:poll.6s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
            @endif
            <div class="col-md-2">
                <input wire:model='keyPlaca' type="text" class="form-control dropdown {{$verDropdownList}}" name="searchPlaca"
                                                id="searchPlaca" placeholder="Placa">
                @if (!empty($listaPlacas))
                    <div class="dropdown-menu {{$verDropdownList}}" style="top:36px; left:7px" aria-labelledby="dropdownMenuButton">
                        @if ($listaPlacas->count() > 0)
                            @foreach ($listaPlacas as $resultado)

                                <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                        {{ $resultado->placa }}
                                </a>
                            @endforeach
                        @else
                            <span class="mx-2">Sin resultados!!!</span>
                        @endif

                    </div>
                @endif
            </div>
            {{-- @if (session()->has('message'))
                <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
            @endif --}}
            {{-- <div class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#createDataModal">
            <i class="fa fa-plus"></i>  Buscar
            </div> --}}
        </div>
    </div>
    <!-- Edit Modal -->
<div wire:ignore.self class="modal fade" id="updateDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Actualizar Teléfono</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
					<input type="hidden" wire:model="transportista_id_selected">
                    <div class="form-group">
                        <label for="num_celular"></label>
                        <input wire:model="num_celular" type="text" class="form-control" id="num_celular" placeholder="num_celular">
                        @error('num_celular') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary">Guardar</button>
            </div>
       </div>
    </div>
</div>
    <div class="card-body">
        {{-- <div class="table-responsive"> --}}
        <div class="row justify-content-end">

            <div class="col-auto pb-3">
                <button class="btn btn-success" wire:click="enviarPlacas()" type="submit">Enviar Placas</button>
            </div>
            <div class="col-auto pb-3">
                <a class="btn btn-info" href="{{route('excel.exports.sindicado.placa.habilitada',[$cia_id])}}">Imprimir</a>
            </div>
        </div>
            <table class="table table-sm table-bordered table-responsive-md table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Placa</th>
                        <th>Cap.</th>
                        <th>Celular</th>
                        <th>Enviado</th>
                        <th>Con. Carga</th>
                        <th>Días Cola</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @foreach ($placasHabilitas as $item)
                        @php
                            $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u',$item->fecha);
                            $diasDiff = $fechaNow->diff($fecha);
                            // dd($item);
                        @endphp
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$fecha->format('d-m-Y H:i:s')}} </td>
                            <td>{{$item->placa}} </td>
                            <td>{{$item->capacidad_arrastre}} </td>
                            <td>{{$item->num_celular}} </td>
                            <td>{{$item->enviado?'SI':'NO'}}</td>
                            <td>
                                <div style="display: flex; justify-content:center; align-items:center">
                                    @livewire('cias.update-conocimiento-enviado', ['cia_habilitar_placa_id' => $item->id], key($item->id))
                                </div>
                            </td>
                            <td>{{$diasDiff->days}}</td>
                            <td>
                                <button class="btn btn-danger btn-sm"
                                        onclick="confirm('Confirma que desea eliminar la placa {{$item->placa}}?')||event.stopImmediatePropagation()"
                                        wire:click="eliminarPlaca({{ $item->id }})"
                                        >Eliminar
                                </button>
                                <button data-bs-toggle="modal" data-bs-target="#updateDataModal" class="btn btn-info btn-sm" wire:click="edit({{$item->transportista_id}})"><i class="fa fa-edit"></i> Editar </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        {{-- </div> --}}

    </div>
</div>

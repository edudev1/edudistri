<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <h5>Cantidad de vehiculos requeridos para los complementos</h5>
        </div>
        <div class="card-body">
            @if (count($listaComplementos) > 0)
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TipoVehiculo</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                @foreach ($listaComplementos as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$tipoVehiculos[$item->tipo_vehiculo_id]}}</td>
                        <td>{{$item->cantidad}}</td>
                    </tr>
                @endforeach
            </table>
            @else
                <p>No hay datos</p>
            @endif
        </div>
    </div>
</div>

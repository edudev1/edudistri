<div>
    <button data-bs-toggle="modal" data-bs-target="#createPlacaDataModal" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Nueva Placa </button>
    <div wire:ignore.self class="modal fade" id="createPlacaDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
           <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateModalLabel">Ingresar Placa</h5>
                    <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="placa">Placa</label>
                            <input  type="text" wire:model="placa" class="form-control" id="placa" placeholder="placa">
                            @error('placa') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                    </form>
                    @if (session()->has('message'))
                        <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" wire:click.prevent="ingresarNuevaPlaca()" class="btn btn-primary">Habilitar Placa</button>
                </div>
           </div>
        </div>
    </div>
</div>

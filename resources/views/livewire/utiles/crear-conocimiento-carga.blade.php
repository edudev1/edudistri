<div>
    <button class="btn btn-sm btn-success" wire:click="$emit('openModal', '{{ $modalId }}')">Revisar</button>
    <!-- Modal -->
    <div wire:ignore.self class="modal" id="{{ $modalId }}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createDataModalLabel">Habilitar Vehículo</h5>
                    <button wire:click.prevent="$emit('closeModal', '{{ $modalId }}')" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
               <div class="modal-body">
                @if (session()->has('message'))
                    <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                @endif
                    <form>
                        <div class="form-group">
                            <label for="placa">Placa</label>
                            <span class="form-control" >{{$placa}}</span>
                            {{-- <input wire:model="placa" type="text" class="form-control" id="placa" placeholder="Placa">
                            @error('placa') <span class="error text-danger">{{ $message }}</span> @enderror --}}
                        </div>
                        <div class="form-group">
                            <label for="numero_carga">Número de Carga</label>
                            <input wire:model="numero_carga" type="text" class="form-control" id="numero_carga" placeholder="Número Carga">@error('numero_carga') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="fecha_carguio">Fecha Carguio</label>
                            <input wire:model="fecha_carguio" type="date" class="form-control" id="fecha_carguio" placeholder="Fecha Carguío">
                            @error('fecha_carguio') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                    @error('fecha_carguio') <span class="error text-danger">{{ $message }}</span> @enderror
                    <div class="form-group">
                        <input wire:model='keyDestino' type="text" class="form-control dropdown {{$verDropdownList}}" name="searchDestino"
                                                id="searchDestino" placeholder="Buscar Destino">
                        @if (!empty($listaDestinos))
                            <div class="dropdown-menu {{$verDropdownList}}" style="top:54px; left:15px" aria-labelledby="dropdownMenuButton">
                                @if ($listaDestinos->count() > 0)
                                    @foreach ($listaDestinos as $resultado)
                                        <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                                {{ $resultado->nombre_completo }}
                                        </a>
                                    @endforeach
                                @else
                                    <span class="mx-2">Sin resultados!!!</span>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="destino_id">Destino Seleccionado</label>
                        <span class="form-control">{{$nombre_completo}} </span>
                        @error('destino_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" wire:click.prevent="$emit('closeModal', '{{ $modalId }}')" class="btn btn-secondary close-btn" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" wire:loading.attr="disabled" wire:click.prevent="store()" class="btn btn-primary">Habilitar</button>
                </div>
            </div>
        </div>
    </div>
</div>

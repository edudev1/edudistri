<!-- Add Modal -->
<div wire:ignore.self class="modal fade" id="createDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createDataModalLabel">Create Vehículo</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
           <div class="modal-body">
				<form>
                    <div class="form-group">
                        <input wire:model='keyTransportista' type="text" class="form-control dropdown {{$verDropdownList}}" name="searchTransportista"
                                                id="searchTransportista" placeholder="Buscar transportista por ci, nombres">
                        @if (!empty($listaTransportista))
                            <div class="dropdown-menu {{$verDropdownList}}" style="top:54px; left:15px" aria-labelledby="dropdownMenuButton">
                                @if ($listaTransportista->count() > 0)
                                    @foreach ($listaTransportista as $resultado)
                                        <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                                {{ $resultado->nombre_completo }}
                                        </a>
                                    @endforeach
                                @else
                                    <span class="mx-2">Sin resultados!!!</span>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="transportista_id">Transportista</label>
                        <span class="form-control">{{$nombre_completo}} </span>
                        @error('transportista_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="placa"></label>
                        <input wire:model="placa" type="text" class="form-control" id="placa" placeholder="placa">@error('placa') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="marca"></label>
                        <input wire:model="marca" type="text" class="form-control" id="marca" placeholder="marca">@error('marca') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="color"></label>
                        <input wire:model="color" type="text" class="form-control" id="color" placeholder="color">@error('color') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="num_chasis"></label>
                        <input wire:model="num_chasis" type="text" class="form-control" id="num_chasis" placeholder="Num Chasis">@error('num_chasis') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="codigo_motor"></label>
                        <input wire:model="codigo_motor" type="text" class="form-control" id="codigo_motor" placeholder="Codigo Motor">@error('codigo_motor') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="capacidad_arrastre"></label>
                        <input wire:model="capacidad_arrastre" type="number" class="form-control" id="capacidad_arrastre" placeholder="Capacidad Arrastre">@error('capacidad_arrastre') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="crpva"></label>
                        <input wire:model="crpva" type="text" class="form-control" id="crpva" placeholder="Crpva">@error('crpva') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="fecha_registro"></label>
                        <input wire:model="fecha_registro" type="date" class="form-control" id="fecha_registro" placeholder="Fecha Registro">@error('fecha_registro') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="tipo_vehiculo_id"></label>
                        {!! html()->select('tipo_vehiculo_id', $tiposvehiculos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_vehiculo_id','id'=>'tipo_vehiculo_id'])
                                  ->placeholder('Tipo Vehículo') !!}
                        @error('tipo_vehiculo_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="transporte_cia_id"></label>
                        {!! html()->select('transporte_cia_id', $companias)
                                  ->attributes(['class' => 'form-control','wire:model'=>'transporte_cia_id','id'=>'transporte_cia_id'])
                                  ->placeholder('Compania Transporte') !!}
                        @error('transporte_cia_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div wire:ignore.self class="modal fade" id="updateDataModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Actualizar Vehículo</h5>
                <button wire:click.prevent="cancel()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
					<input type="hidden" wire:model="selected_id">
                    <div class="form-group">
                        <input wire:model='keyTransportista' type="text" class="form-control dropdown {{$verDropdownList}}" name="searchTransportista"
                                                id="searchTransportista" placeholder="Buscar transportista por ci, nombres">
                        @if (!empty($listaTransportista))
                            <div class="dropdown-menu {{$verDropdownList}}" style="top:54px; left:15px" aria-labelledby="dropdownMenuButton">
                                @if ($listaTransportista->count() > 0)
                                    @foreach ($listaTransportista as $resultado)
                                        <a class="dropdown-item" href="#" wire:click="seleccionarResultado({{ $resultado->id }})">
                                                {{ $resultado->nombre_completo }}
                                        </a>
                                    @endforeach
                                @else
                                    <span class="mx-2">Sin resultados!!!</span>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="transportista_id">Transportista</label>
                        <span class="form-control">{{$nombre_completo}} </span>
                        @error('transportista_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="placa">Placa</label>
                        <input wire:model="placa" type="text" class="form-control" id="placa" placeholder="placa">@error('placa') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="marca">Marca</label>
                        <input wire:model="marca" type="text" class="form-control" id="marca" placeholder="marca">@error('marca') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="color">Color</label>
                        <input wire:model="color" type="text" class="form-control" id="color" placeholder="color">@error('color') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="num_chasis">Número Chasis</label>
                        <input wire:model="num_chasis" type="text" class="form-control" id="num_chasis" placeholder="Num Chasis">@error('num_chasis') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="codigo_motor">Código Motor</label>
                        <input wire:model="codigo_motor" type="text" class="form-control" id="codigo_motor" placeholder="Codigo Motor">@error('codigo_motor') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="capacidad_arrastre">Capacidad de Arrastre</label>
                        <input wire:model="capacidad_arrastre" type="number" class="form-control" id="capacidad_arrastre" placeholder="Capacidad Arrastre">@error('capacidad_arrastre') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="crpva">CRPVA</label>
                        <input wire:model="crpva" type="text" class="form-control" id="crpva" placeholder="Crpva">@error('crpva') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="tipo_vehiculo_id">Tipo de Vehículo</label>
                        {!! html()->select('tipo_vehiculo_id', $tiposvehiculos)
                                  ->attributes(['class' => 'form-control','wire:model'=>'tipo_vehiculo_id','id'=>'tipo_vehiculo_id'])
                                  ->placeholder('Tipo Vehículo') !!}
                        @error('tipo_vehiculo_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="transporte_cia_id">Compañia de Transporte</label>
                        {!! html()->select('transporte_cia_id', $companias)
                                  ->attributes(['class' => 'form-control','wire:model'=>'transporte_cia_id','id'=>'transporte_cia_id'])
                                  ->placeholder('Compania Transporte') !!}
                        @error('transporte_cia_id') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary">Guardar</button>
            </div>
       </div>
    </div>
</div>

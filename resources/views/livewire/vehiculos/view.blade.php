@section('title', __('Vehiculos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Lista Vehículos </h4>
						</div>
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar">
						</div>
						<div wire:click="abrirModalCreate" class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#createDataModal">
						<i class="fa fa-plus"></i>  Añadir Vehículo						</div>
					</div>
				</div>

				<div class="card-body">
						@include('livewire.vehiculos.modals')
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<td>#</td>
								<th>Placa</th>
								<th>Marca</th>
								<th>Color</th>
								<th>Num Chasis</th>
								<th>Codigo Motor</th>
								<th>Capacidad Arrastre</th>
								<th>Crpva</th>
								{{-- <th>Num Ci Propietario</th> --}}
								<th>Fecha Registro</th>
								<th>Tipo Vehiculo</th>
								<th>Compañia Transporte</th>
								<th>Transportista</th>
								<td>ACCIONES</td>
							</tr>
						</thead>
						<tbody>
							@forelse($vehiculos as $row)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $row->placa }}</td>
								<td>{{ $row->marca }}</td>
								<td>{{ $row->color }}</td>
								<td>{{ $row->num_chasis }}</td>
								<td>{{ $row->codigo_motor }}</td>
								<td>{{ $row->capacidad_arrastre }}</td>
								<td>{{ $row->crpva }}</td>
								{{-- <td>{{ $row->num_ci_propietario }}</td> --}}
                                @php
                                    $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $row->fecha_registro);
                                @endphp
								<td>{{ $fecha->format('d/m/Y') }}</td>
								<td>{{ $row->tipo }}</td>
								<td>{{ $row->nombre }}</td>
								<td>{{ $row->full_name }}</td>
								<td width="90">
									<div class="dropdown">
										<a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
											Acciones
										</a>
										<ul class="dropdown-menu">
											<li><a data-bs-toggle="modal" data-bs-target="#updateDataModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a></li>
											<li><a class="dropdown-item" onclick="confirm('Confirma eliminar Vehiculo id {{$row->id}}? \nUna vez eliminado no se podra recuperar!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a></li>
										</ul>
									</div>
								</td>
							</tr>
							@empty
							<tr>
								<td class="text-center" colspan="100%">No se encontraron datos </td>
							</tr>
							@endforelse
						</tbody>
					</table>
					<div class="float-end">{{ $vehiculos->links() }}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

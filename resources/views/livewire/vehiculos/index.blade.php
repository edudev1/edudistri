@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Administrar Vehículos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @livewire('vehiculos')
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

{{-- @section('plugins.select2', true) --}}

@section('js')
<script>
    document.addEventListener('livewire:load', function () {
        window.livewire.on('closeModal', () => {
            $('#createDataModal').modal('hide');
        });
        window.livewire.on('closeModalEdit', () => {
            $('#updateDataModal').modal('hide');
        });
    });
</script>
@stop

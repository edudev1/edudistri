@props(['items', 'selected', 'classes' => []])

<select {{ $attributes->merge($classes) }}>
    @foreach($items as $value => $option)
        <option value="{{ $value }}" {{ $value == $selected ? 'selected' : '' }}>{{ $option }}</option>
    @endforeach
</select>

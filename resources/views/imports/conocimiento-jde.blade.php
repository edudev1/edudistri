@extends('adminlte::page')

@section('title', 'acciones JDE')

@section('content_header')
<h1>cargar conocimiento de carga jde</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row mb-3 justify-content-center">
        <div class="col-md-12">
            <form action="{{ route('import.excel.conocimiento.jde.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="fexcel" required>
                <button class="btn btn-sm btn-primary" type="submit">Importar</button>
            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4>Lista conocimientos de carga del JDE </h4>
						</div>
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Datos">
						</div>
					</div>
				</div>

				<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<td>#</td>
								<th>numero_carga</th>
								<th>ciudad</th>
								<th>fecha</th>
								<td>placa</td>
							</tr>
						</thead>
						<tbody>
							@forelse($conocimientoCargaJde as $row)
							<tr>
                                @php
                                // dd(substr($row->fecha_pedido,0,10));
                                    $fecha = DateTime::createFromFormat('Y-m-d', $row->fecha);
                                    // dd($fecha);
                                @endphp
								<td>{{ $loop->iteration }}</td>
								<td>{{ $row->numero_carga }}</td>
								<td>{{ $row->ciudad }}</td>
								<td>{{ $fecha->format('d-m-Y') }}</td>
								<td>{{ $row->placa }}</td>
							</tr>
							@empty
							<tr>
								<td class="text-center" colspan="100%">Sin datos </td>
							</tr>
							@endforelse
						</tbody>
					</table>
                    <div class="float-end">{{ $conocimientoCargaJde->links() }}</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

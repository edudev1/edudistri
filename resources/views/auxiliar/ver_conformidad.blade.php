@extends('adminlte::page')

@section('title', 'Reportes de Conformidad de Carga')

@section('content_header')
<h1>Ver Conformidad de Carga</h1>
@stop

@section('content')
<div class="container-fluid">
    <form action="{{route('buscar.conformidad')}}" method="post">
        @csrf
        <div class="row justify-content-center">
                <fieldset class="col-md-4">
                    <legend>Fuente de datos</legend>
                    <div class="form-group">
                      <label for="">
                          <input type="radio" name="fuente" value="sdc" id="tabla1" checked>
                          SDC
                      </label>
                      <label for="">
                          <input type="radio" name="fuente" value="jde" id="tabla2">
                            JDE
                      </label>
                    </div>
                </fieldset>
                <fieldset class="col-md-4">
                    <legend>Buscar por</legend>
                    <div class="form-group">
                        <label for="">
                          <input type="radio" name="buscar_por" value="conformidad" id="buspor_1" checked> Conformidad
                        </label>
                        <label for="">
                          <input type="radio" name="buscar_por" value="placa" id="buspor_2" > Placa
                        </label>
                    </div>
                </fieldset>
                <fieldset class="col-md-4">
                    <legend>&nbsp;</legend>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                  <input class="form-control" type="text" name="search" placeholder="Conocimiento / Placa" required >
                            </div>
                        </div>
                        <div class="col-6">
                            <input class="btn btn-primary" type="submit" value="Buscar">
                        </div>
                    </div>

                </fieldset>
        </div>
    </form>
    @isset($datos)
    <div>
        <div class="card border-primary">
          <img class="card-img-top" src="holder.js/100px180/" alt="">
          <div class="card-body">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Número de carga</th>
                        <th>Fecha Registro</th>
                        <th>Placa</th>
                        <th>Destino</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datos as $item)
                        <tr>
                            <td>{{$item->numero_carga}}</td>
                            <td>{{$item->fecha}}</td>
                            <td>{{$item->placa}}</td>
                            <td>{{$item->ciudad}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
    </div>
    @endisset

</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

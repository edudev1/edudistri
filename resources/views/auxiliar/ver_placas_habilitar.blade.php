@extends('adminlte::page')

@section('title', 'Habilitar Placas')

@section('content_header')
<h1>Compañia de transporte {{$cia->nombre}} </h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
            {{-- @livewire('utiles.crear-conocimiento-carga', ['id' => 1]) --}}
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">
                        <div class="float-left">
                            <h4><i class="fab fa-laravel text-info"></i>
                            Lista de Placas a habilitar </h4>
                        </div>
                        @if (session()->has('message'))
                        <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                        @endif
                        <div class="float-rigth">
                            @livewire('auxiliar.habilitar-placa', ['cia_id'=> $cia->id])
                        </div>
                    </div>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Placa</th>
                                <th>Conocimiento</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=0;
                            @endphp
                            @foreach ($placasCia as $item)
                                <tr>
                                    @php
                                        $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $item->fecha);
                                        $cola_fecha = DateTime::createFromFormat('Y-m-d H:i:s', $item->cola_fecha);
                                    @endphp
                                    <td>{{++$i}}</td>
                                    <td>{{$fecha->format('d-m-Y')}}</td>
                                    <td>{{$item->placa}}</td>
                                    <td>
                                        {{$item->conocimiento_enviado=='1'?'SI':'NO'}}
                                    </td>
                                    <td>
                                        @if (is_numeric($item->cola_id))
                                        <form action="{{route('aux.colas.confirmar.placa')}}" method="post">
                                                <span>Placa en cola, conocimiento agregado en fecha {{$cola_fecha?$cola_fecha->format('d-m-Y'):''}}</span>
                                                @csrf
                                                <input type="hidden" name="cola_id" value="{{$item->cola_id}}">
                                                <input type="hidden" name="chp_id" value="{{$item->chp_id}}">
                                                <input type="hidden" name="placa" value="{{$item->placa}}">
                                                <input type="hidden" name="cia_id" value="{{$item->cia_id}}">
                                                <button type="submit" class="btn btn-warning btn-sm">Confirmar</button>

                                            </form>
                                        @else
                                        <div style="display: flex;">
                                            @livewire('utiles.crear-conocimiento-carga', ['id' => $item->chp_id, 'cia_id'=> $cia->id], key($item->chp_id))
                                            <form action="{{route('aux.colas.eliminar.placa')}}" method="post" onsubmit="return confirm('¿Realmente deseas quitar la placa de esta lista?');">
                                                @csrf
                                                <input type="hidden" name="cola_id" value="{{$item->cola_id}}">
                                                <input type="hidden" name="chp_id" value="{{$item->chp_id}}">
                                                <input type="hidden" name="placa" value="{{$item->placa}}">
                                                <input type="hidden" name="cia_id" value="{{$item->cia_id}}">
                                                <button type="submit" class="btn btn-danger btn-sm">Quitar Placa</button>
                                            </form>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('openModal', function (modalId) {
            document.getElementById(modalId).style.display = 'block';
        });

        Livewire.on('closeModal', function (modalId) {
            document.getElementById(modalId).style.display = 'none';
        });
    });
</script>
@stop

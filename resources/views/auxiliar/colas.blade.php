@extends('adminlte::page')

@section('title', 'Ver Colas')

@section('content_header')
<h1>Colas de vehículos</h1>
<div class="row">
    <div class="x">
        <a class="btn btn-sm btn-success" target="_blank" href="{{route('rpt.colas',['UVS'])}}"><i class="far fa-file-pdf"></i>&nbsp; COLA UVS</a>
        <a class="btn btn-sm btn-success" target="_blank" href="{{route('rpt.colas',['UVI'])}}"><i class="far fa-file-pdf"></i>&nbsp; COLA UVI</a>
        <a class="btn btn-sm btn-success" target="_blank" href="{{route('rpt.colas',['UVP'])}}"><i class="far fa-file-pdf"></i>&nbsp; COLA UVP</a>
        <a class="btn btn-sm btn-info" href="{{route('excel.exports.colas')}}"> <i class="far fa-file-excel"></i> Colas</a>
    </div>
</div>
@stop

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                @if (session()->has('message'))
                    <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h4>COLA UVS</h4>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-sm btn-info float-right" target="_blank" href="{{route('rpt.colas',['UVS'])}}">Imprimir</a>
                </div>
            </div>
            <table class="table table-bordered table-sm table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Placa</th>
                        <th>Capacidad</th>
                        <th>Sindicato</th>
                        <th width="10px" >Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @foreach ($colaUVS as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            @php
                                // dd($item->fecha);
                                $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $item->fecha);
                            @endphp
                            <td>{{$fecha->format('d/m/Y')}}</td>
                            <td>{{$item->placa}}</td>
                            <td>{{$item->capacidad}}</td>
                            <td>{{$item->nombre}}
                            </td>
                            <td>
                                <form action="{{route('aux.colas.delete.placa',[$item->id])}}" method="post">
                                    @csrf
                                    <button class="btn btn-primary btn-sm">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h4>COLA UVI</h4>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-sm btn-info float-right" target="_blank" href="{{route('rpt.colas',['UVI'])}}">Imprimir</a>
                </div>
            </div>
            <table class="table table-bordered table-sm table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Placa</th>
                        <th>Capacidad</th>
                        <th>Sindicato</th>
                        <th width="10px" >Acción</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @foreach ($colaUVI as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            @php
                                $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $item->fecha);
                            @endphp
                            <td>{{$fecha->format('d/m/Y')}}</td>
                            <td>{{$item->placa}}</td>
                            <td>{{$item->capacidad}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>
                                <form action="{{route('aux.colas.delete.placa',[$item->id])}}" method="post">
                                    @csrf
                                    <button class="btn btn-primary btn-sm">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h4>COLA UVP</h4>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-sm btn-info float-right" target="_blank" href="{{route('rpt.colas',['UVP'])}}">Imprimir</a>
                </div>
            </div>
            <table class="table table-bordered table-sm table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>placa</th>
                        <th>Capacidad</th>
                        <th>Sindicato</th>
                        <th width="10px" >Acción</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @foreach ($colaUVP as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            @php
                                $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $item->fecha);
                            @endphp
                            <td>{{$fecha->format('d/m/Y')}}</td>
                            <td>{{$item->placa}}</td>
                            <td>{{$item->capacidad}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>
                                <form action="{{route('aux.colas.delete.placa',[$item->id])}}" method="post">
                                    @csrf
                                    <button class="btn btn-primary btn-sm">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">

        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

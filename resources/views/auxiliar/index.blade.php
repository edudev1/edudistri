@extends('adminlte::page')

@section('title', 'Habilitar Placas')

@section('content_header')
<h1>Habilitar Vehículos Compañias</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- @livewire('utiles.crear-conocimiento-carga', ['id' => 1]) --}}
            <table class="table table-bordered table-sm table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Compañia de transporte</th>
                        <th>Cantidad</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @foreach ($listaPlacasCias as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->cantidad}}</td>
                            <td>
                                <a href="{{route('aux.placas.compania',[$item->id])}}" class="btn btn-sm btn-success">Ver Placas</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

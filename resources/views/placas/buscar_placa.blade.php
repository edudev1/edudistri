@extends('adminlte::page')

@section('title', 'Buscar Viajes')

@section('content_header')
<h1>Buscar viajes por placa</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @livewire('util-placa.buscar-viajes-placa')
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

@extends('reportes.master_rpt')

@section('css_print')

@stop

@section('header_print')
    <div class="logo">
        <img src="{{ asset('img/logo.png') }}" alt="Logo">
    </div>
    <div class="title">
        Reporte de programación Compañia <br>
        {{ $sindicato->nombre }}
    </div>
    <div class="user-info">
        Usuario: {{ Auth::user()->name }} <br>
        Fecha de impresión: {{ date('d/m/Y H:i:s') }}
    </div>
@stop

@section('footer_print')
    <table>
        <tr>
            <td>
                <p class="izq">
                    Sistema de Distribución de Carga
                </p>
            </td>
            <td>
                <p class="page">
                    Página
                </p>
            </td>
        </tr>
    </table>
@stop

@section('content_print')

    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>placa</th>
                <th>Vehículo</th>
                <th>Transportista</th>
                <th>Cantidad</th>
                <th>Destino</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 0;
                $cantidad_suma = 0;
                // dd($programacionesXCia);
            @endphp

            @foreach ($programacionesXCia as $ciaProgramacion)
                <tr>
                    <td>
                        <div class="der">{{ ++$i }}</div>
                    </td>
                    <td>
                        <div>{{ $ciaProgramacion->placa }}</div>
                    </td>
                    <td>
                        <div>{{ $ciaProgramacion->vehiculo }}</div>
                    </td>
                    <td>
                        <div>{{ $ciaProgramacion->transportista }}</div>
                    </td>
                    <td>
                        <div class="der">{{ $ciaProgramacion->cantidad }}</div>
                    </td>
                    <td>
                        <div>{{ $ciaProgramacion->destino }}</div>
                    </td>
                </tr>
                @php
                    $cantidad_suma += $ciaProgramacion->cantidad;
                @endphp
            @endforeach
            <tr>
                <td class="der" colspan="4"><b>Total</b></td>
                <td class="der">{{ $cantidad_suma }}</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>

@stop

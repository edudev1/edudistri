@extends('adminlte::page')

@section('title', 'Reportes Programaciones por sindicato')

@section('content_header')
<h1>Reportes de programaciones por sindicatos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card text-left">
            {{-- <img class="card-img-top" src="holder.js/100px180/" alt=""> --}}
            <form class="form-horizontal" action="{{route('rpt.programacion.cias')}}" method="POST">
                @csrf
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group m-2">
                        <label for="fecha">Fecha</label>
                        <input class="form-control" type="date" name="fecha" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group m-2">
                        <label for="sindicato">Sindicato</label>
                        {!! html()->select('sindicato', $sindicatos)
                                    ->attributes(['class' => 'form-control','id'=>'sindicato', 'required'=>'required'])
                                    ->placeholder('Seleccionar Sindicato') !!}
                        @error('sindicato') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group m-2">
                        <div class="form-group">
                            <label>&nbsp;</label>
                        </div>
                        <label>
                            <input type="radio" checked name="tipo_rpt" value="pdf"> Pdf
                        </label>
                        <label>
                            <input type="radio" name="tipo_rpt" value="xls"> Excel
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="form-group">
                            <label>&nbsp;</label>
                        </div>
                        <input type="submit" value="Generar">
                    </div>
                </div>
            </div>
            </form>
            <div class="card-body">
              @if (session()->has('message'))
                <div class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
              @endif
            </div>
          </div>

        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

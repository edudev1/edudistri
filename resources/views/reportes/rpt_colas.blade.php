@extends('reportes.master_rpt')

@section('css_print')

@stop

@section('header_print')
    <div class="logo">
        <img src="{{ asset('img/logo.png') }}" alt="Logo">
    </div>
    <div class="title">
        Reporte de colas {{$tipoCola}}
    </div>
    <div class="user-info">
        Usuario: {{ Auth::user()->name }} <br>
        Fecha de impresión: {{ date('d/m/Y') }}
    </div>
@stop

@section('footer_print')
<table>
    <tr>
      <td>
          <p class="izq">
            Sistema de Distribución de Carga
          </p>
      </td>
      <td>
        <p class="page">
          Página
        </p>
      </td>
    </tr>
  </table>
@stop

@section('content_print')
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Placa</th>
            <th>Nombre</th>
            <th>Cap.</th>
            <th>Compañia</th>
            <th>Fecha Carguido</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i=0;
        @endphp
        @foreach ($colaVehiculos as $item)
            <tr>
                <td><div class="der">{{++$i}}</div></td>
                <td><div class="der">{{$item->placa}}</div></td>
                <td>{{$item->full_name}}</td>
                <td><div class="der">{{$item->capacidad_arrastre}}</div></td>
                <td>{{$item->nombre}}</td>
                <td><div class="der">{{$item->fecha_carguio}}</div></td>
            </tr>
        @endforeach
    </tbody>
</table>
@stop

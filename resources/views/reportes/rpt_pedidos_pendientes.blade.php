@extends('reportes.master_rpt')

@section('css_print')

@stop

@section('header_print')
    <div class="logo">
        <img src="{{ asset('img/logo.png') }}" alt="Logo">
    </div>
    <div class="title">
        Reporte de programación
    </div>
    <div class="user-info">
        Usuario: {{ Auth::user()->name }} <br>
        Fecha de impresión: {{ date('d/m/Y') }}
    </div>
@stop

@section('footer_print')
<table>
    <tr>
      <td>
          <p class="izq">
            Sistema de Distribución de Carga
          </p>
      </td>
      <td>
        <p class="page">
          Página
        </p>
      </td>
    </tr>
  </table>
@stop

@section('content_print')
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Destino Pedido</th>
            <th>Fecha Pedido</th>
            <th>Cantidad</th>
            <th>Tipo Pedido</th>
            <th>S</th>
            <th>A</th>
            <th>D</th>
            <th>T</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i=0;
            $sum_cantidad = 0;
            $sum_Trailers = 0;
            $sum_Direccionales = 0;
            $sum_Alzapatas = 0;
            $sum_Sencillos = 0;
        @endphp
        @foreach ($pedidosDestino as $item)
        @php
            $fecha_pedido = DateTime::createFromFormat('Y-m-d', substr($item->fecha_pedido,0,10));
            $sum_cantidad += $item->cantidad;
            $sum_Trailers += $item->Trailers;
            $sum_Direccionales += $item->Direccionales;
            $sum_Alzapatas += $item->Alzapatas;
            $sum_Sencillos += $item->Sencillos;
        @endphp
            <tr>
                <td><div class="der">{{++$i}}</div></td>
                <td><div>{{$item->nombre_destino}}</div></td>
                <td><div class="der">{{$fecha_pedido->format('d-m-Y')}}</div></td>
                <td><div class="der">{{$item->cantidad}}</div></td>
                <td><div style="text-align: center;">{{$item->tipo_pago}}</div></td>
                <td><div class="der">{{$item->Trailers}}</div></td>
                <td><div class="der">{{$item->Direccionales}}</div></td>
                <td><div class="der">{{$item->Alzapatas}}</div></td>
                <td><div class="der">{{$item->Sencillos}}</div></td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td class="der">Total:</td>
            <td class="der">{{$sum_cantidad}}</td>
            <td class="der"></td>
            <td class="der">{{$sum_Trailers}}</td>
            <td class="der">{{$sum_Direccionales}}</td>
            <td class="der">{{$sum_Alzapatas}}</td>
            <td class="der">{{$sum_Sencillos}}</td>
        </tr>
    </tfoot>
</table>
@stop

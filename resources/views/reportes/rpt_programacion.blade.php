@extends('reportes.master_rpt')

@section('css_print')

@stop

@section('header_print')
    <div class="logo">
        <img src="{{ asset('img/logo.png') }}" alt="Logo">
    </div>
    <div class="title">
        Reporte de programación <br> {{$fecha}}
    </div>
    <div class="user-info">
        Usuario: {{ Auth::user()->name }} <br>
        Fecha de impresión: {{ date('d/m/Y') }}
    </div>
@stop

@section('footer_print')
<table>
    <tr>
      <td>
          <p class="izq">
            Sistema de Distribución de Carga
          </p>
      </td>
      <td>
        <p class="page">
          Página
        </p>
      </td>
    </tr>
  </table>
@stop

@section('content_print')
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Fecha a Programar</th>
            <th>Ciudad</th>
            <th>Cantidad</th>
            <th>Tipo Pedido</th>
            <th>S</th>
            <th>A</th>
            <th>D</th>
            <th>T</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i=0;
        @endphp
        @foreach ($programaciones as $item)
            <tr>
                <td><div class="der">{{++$i}}</div></td>
                <td><div>{{$item->fecha}}</div></td>
                <td>{{$item->nombre_destino}}</td>
                <td><div class="der">{{$item->sum_cantidad}}</div></td>
                <td><div style="text-align: center;">{{$item->tipo_pago}}</div></td>
                <td><div class="der">{{$item->Sencillos}}</div></td>
                <td><div class="der">{{$item->Alzapatas}}</div></td>
                <td><div class="der">{{$item->Direccionales}}</div></td>
                <td><div class="der">{{$item->Trailers}}</div></td>
            </tr>
        @endforeach
    </tbody>
</table>
@stop

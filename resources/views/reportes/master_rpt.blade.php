<html>

<head>
    <link rel="stylesheet" href="{{ asset('css/print.css') }}">
    @yield('css_print')
</head>

<body>
    <header>
        @yield('header_print')
    </header>
    <footer>
        @yield('footer_print')
    </footer>
    <div id="content">
        @yield('content_print')
    </div>
</body>

</html>

@extends('adminlte::page')

@section('title', 'Reportes Operador')

@section('content_header')
<h1>Reportes Operador</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <a href="#" class="btn btn-sm btn-primary">Ver programación</a>
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

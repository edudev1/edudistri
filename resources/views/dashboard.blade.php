@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Panel de control</h1>
@stop

@section('content')
    <p>BIENVENIDOS AL SISTEMA DE DISTRIBUCION DE CARGA </p>

        @if (Auth::user()->hasRole('Administrador'))
            <form action="{{route('dev.borrar.datos')}}" method="post">
                @csrf
                <button class="btn btn-primary">
                    Borrar datos
                </button>
            </form>
        @endif
    <p>
        @if (session()->has('message'))
            <div class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
        @endif
    </p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hola mundo!'); </script>
@stop

@extends('adminlte::page')

@section('title', 'Generar Programación')

@section('content_header')
<h1>Generar Programación</h1>
<div class="row m-2">
    @php
        $fecha = \Carbon\Carbon::now();
    @endphp
    <div class="group-form">
        <a class="btn btn-sm btn-info" href="{{route('ope.ver.programacion',[$fecha->format('d-m-Y')])}}">Ver Programación</a>
        <a class="btn btn-sm btn-primary" href="{{route('ope.ver.complemento',[$fecha->format('d-m-Y')])}}">Ver Complementos</a>
        <a class="btn btn-sm btn-success" href="{{route('ope.ver.complemento.publicado',[0])}}">Ver Publicados</a>
    </div>
</div>
@stop

@section('content')


<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
                    <form action="{{route('ope.generar.programacion')}}" method="post">
                        @csrf
                        <div class="row" style="display: flex; justify-content: space-between; align-items: center;">
                            <div class="col-sm-6">
                                <h4>Lista de Pedidos</h4>
                                @if (session()->has('message'))
                                    <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                                @endif
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="fecha_programacion" id="fecha_programacion" placeholder="Select Fecha">
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-primary form-control">Generar Programación</button>
                                    </div>
                            </div>
                        </div>
                    </form>
				</div>

				<div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                            <thead class="thead">
                                <tr>
                                    <td>#</td>
                                    <th>Destino</th>
                                    <th>Fecha Pedido</th>
                                    <th>Cantidad</th>
                                    <th>Tipo Pago</th>
                                    <th>Trailers</th>
                                    <th>Direccionales</th>
                                    <th>Alzapatas</th>
                                    <th>Sencillos</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($listaPedidos as $row)
                                @php
                                    // dd(substr($row->fecha_pedido,0,10));
                                    $fecha = DateTime::createFromFormat('Y-m-d', substr($row->fecha_pedido,0,10));
                                    // dd($row);
                                @endphp
                                <tr>
                                    <td class="text-right">{{ $loop->iteration }}</td>
                                    <td class="text-left">{{ $row->nombre_destino }}</td>
                                    <td class="text-right">{{ $fecha->format('d-m-Y') }}</td>
                                    <td class="text-right">{{ $row->cantidad }}</td>
                                    <td class="text-center">{{ $row->tipo_pago }}</td>
                                    <td class="text-right">{{ $row->Trailers }}</td>
                                    <td class="text-right">{{ $row->Direccionales }}</td>
                                    <td class="text-right">{{ $row->Alzapatas }}</td>
                                    <td class="text-right">{{ $row->Sencillos }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="100%">Sin datos </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <label for="">Pedidos</label>
          <table class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tipo Pago</th>
                    <th>Cantidad Vehículos</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=1;
                @endphp
                @foreach ($pedidosXTipo as $pedido)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$pedido->tipo_pago}}</td>
                        <td>{{$pedido->total_vehiculos}}</td>

                    </tr>
                @endforeach
            </tbody>
          </table>
          <div class="row col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
          </div>
        </div>
        <div class="col-md-6">
            <label for="">Complementos</label>
            <table class="table table-sm table-bordered">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Tipo Pago</th>
                      <th>Cantidad Vehículos</th>
                  </tr>
              </thead>
              <tbody>
                  @php
                      $i=1;
                  @endphp
                  @foreach ($pedidosComplementoXTipo as $complemento)
                      <tr>
                          <td>{{$i++}}</td>
                          <td>{{$complemento->tipo_pago}}</td>
                          <td>{{$complemento->total_vehiculos}}</td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
            <div class="row col-md-12">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
            </div>
          </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="">Número de Vehículos Habilitados</label>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tipo Cola</th>
                        <th>Tipo Vehiculo</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=1;
                    @endphp
                    @foreach ($numPlacasXColas as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->tipo_cola}}</td>
                            <td>{{$item->tipo}}</td>
                            <td>{{$item->cantidad}}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
<div class="errores">
    @php
        // dd(session('listaPedidos'));
    @endphp
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

@extends('adminlte::page')

@section('title', 'Ver programacion')

@section('content_header')
<h1>Ver programación</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    <form action="{{route('ope.ver.programacion',[$fecha])}}" method="get">
                        @csrf
                        <div class="row" style="display: flex; justify-content: space-between; align-items: center;">
                            <div class="col-sm-6">
                                <h4>Lista de vehículos programados</h4>
                                @if (session()->has('message'))
                                    <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                                @endif
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="fecha_pro" id="fecha" value="{{$fecha}}" required>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-primary form-control">Ver Programación</button>
                                    </div>
                            </div>
                        </div>
                    </form>
				</div>

				<div class="card-body">
                    <div class="table-responsive">
                        @if (count($vehiculosProgramados)>0)
                        <div>
                            <a target="_blank" href="{{route('rpt.impr.programacion',[$fecha])}}" class="btn btn-sm btn-info mb-3">Totales PDF</a>
                            <a target="_blank" href="{{route('rpt.impr.programacion.sindicato',[$fecha])}}" class="btn btn-sm btn-success mb-3">Programaciones Por Sindicato PDF</a>
                            <a href="{{route('rpt.excel.programacion',[$fecha])}}" class="btn btn-sm btn-warning mb-3">Programaciones Por Sindicato EXCEL</a>
                        </div>
                        <table class="table table-bordered table-sm">
                            <thead class="thead">
                                <tr>
                                    <td>#</td>
                                    <th>Destino</th>
                                    <th>Fecha Pro</th>
                                    <th>Cantidad</th>
                                    <th>Tipo Pago</th>
                                    <th>Tipo vheículo</th>
                                    <th>Placa</th>

                                </tr>
                            </thead>
                            <tbody>
                                @php

                                @endphp
                                @forelse($vehiculosProgramados as $row)
                                @php
                                    // dd(substr($row->fecha_pedido,0,10));
                                    $fecha = DateTime::createFromFormat('Y-m-d', substr($row->fecha,0,10));
                                    // dd($row);
                                @endphp
                                <tr>
                                    <td class="text-right">{{ $loop->iteration }}</td>
                                    <td class="text-left">{{ $row->nombre_destino }}</td>
                                    <td class="text-right">{{ $fecha->format('d-m-Y') }}</td>
                                    <td class="text-right">{{ $row->cantidad }}</td>
                                    <td class="text-center">{{ $row->tipo_pago }}</td>
                                    <td class="text-right">{{ $row->tipo }}</td>
                                    <td class="text-right">{{ $row->placa }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="100%">Sin datos </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @else
                            <div>No se encontro ningún resultado</div>
                        @endif

					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

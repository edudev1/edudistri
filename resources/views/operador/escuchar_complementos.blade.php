@extends('adminlte::page')

@section('title', 'Revisar complementos asignados')

@section('content_header')
<h1>Complementos asignados por las compañias de transporte</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @if (session()->has('message'))
            <div class="col-md-6">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Mensaje!</h5>
                    {{ session('message') }} 
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Cantidad de vehiculos requeridos para los complementos</h5>
                    </div>
                    <div class="card-body">
                        @if (count($listaComplementos) > 0)
                        <table class="table table-sm table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>TipoVehiculo</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            @foreach ($listaComplementos as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$tipoVehiculos[$item->tipo_vehiculo_id]}}</td>
                                    <td>{{$item->cantidad}}</td>
                                </tr>
                            @endforeach
                        </table>
                        @else
                            <p>No hay datos</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Cantidad de vehiculos enviados por las compañias</h5>
                    </div>
                    <div class="card-body">
                        @if (count($listaVehiculosEnviados) > 0)
                        <table class="table table-sm table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>TipoVehiculo</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            @foreach ($listaVehiculosEnviados as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$tipoVehiculos[$item->tipo_vehiculo_id]}}</td>
                                    <td>{{$item->cantidad}}</td>
                                </tr>
                            @endforeach
                        </table>
                        @else
                            <p>No hay datos</p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('footer')
<p class="mx-2 font-weight-bold">La página se recargara en <span id="valorIntervalo"></span> segundos</p>

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    var intervalo = 30000; // intervalo inicial en milisegundos
    var spanIntervalo = document.getElementById("valorIntervalo");

    function actualizarSpan() {
        spanIntervalo.textContent = intervalo/1000;
        intervalo -= 1000; // reducir en 1 segundo (1000 milisegundos)

        if (intervalo >= 0) {
            setTimeout(actualizarSpan, 1000); // llamada recursiva cada 1 segundo
        }
        if(intervalo <= 0){
            location.reload();
        }
    }

    actualizarSpan();
  </script>
@stop

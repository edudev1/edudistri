@extends('adminlte::page')

@section('title', 'Complementos')

@section('content_header')
<h1>Complementos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                        <div class="row" style="display: flex; justify-content: space-between; align-items: center;">
                            <div class="col-sm-6">
                                <h4>Listas de pedidos para complemento</h4>
                                @if (session()->has('message'))
                                    <div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
                                @endif
                            </div>
                            {{-- <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="fecha_pro" id="fecha" placeholder="Seleccione una Fecha">
                                </div>
                            </div> --}}
                            <div class="col-sm-3 mt-2">
                                <form action="{{route('ope.publicar.complemento')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="fecha_programacion">Fecha programación</label>
                                        <input class="form-control" type="date" name="fecha_programacion" id="fecha_programacion" value="{{now()->addDay()->format('Y-m-d')}}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-primary form-control">Publicar Complementos</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
				</div>

				<div class="card-body">
                    @if ($listaComplementos)
                        <div class="table-responsive">
                            <h5 class="my-3">Cantidad de vehiculos requeridos para complementos</h5>
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>TRAILERS</th>
                                        <th>DIRECCIONALES</th>
                                        <th>ALZAPATAS</th>
                                        <th>SENCILLOS</th>
                                        <th>VEHICULOS REQUERIDOS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $trailers = 0;
                                        $direccionales = 0;
                                        $alzapatas = 0;
                                        $sencillos = 0;
                                    @endphp
                                    @foreach ($listaComplementos as $item)
                                        @php
                                            $trailers += $item->Trailers;
                                            $direccionales += $item->Direccionales;
                                            $alzapatas += $item->Alzapatas;
                                            $sencillos += $item->Sencillos;
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td>1</td>
                                        <td class="text-right">{{$trailers}}</td>
                                        <td class="text-right">{{$direccionales}}</td>
                                        <td class="text-right">{{$alzapatas}}</td>
                                        <td class="text-right">{{$sencillos}}</td>
                                        <td class="text-right">{{$trailers + $direccionales + $alzapatas + $sencillos}}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    @endif
				    <div class="table-responsive">
                        <h5 class="my-3">Lista de complementos indicando su destino</h5>
                        <table class="table table-bordered table-sm">
                            <thead class="thead">
                                <tr>
                                    <td>#</td>
                                    <th>Destino</th>
                                    <th>Fecha Pedido</th>
                                    {{-- <th>Cantidad</th> --}}
                                    <th>Tipo Pago</th>
                                    <th>Trailers</th>
                                    <th>Direccionales</th>
                                    <th>Alzapatas</th>
                                    <th>Sencillos</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $trailers = 0;
                                    $direccionales = 0;
                                    $alzapatas = 0;
                                    $sencillos = 0;
                                @endphp
                                @forelse($listaComplementos as $row)
                                @php
                                    // dd(substr($row->fecha_pedido,0,10));
                                    $fecha = DateTime::createFromFormat('Y-m-d', substr($row->fecha_pedido,0,10));
                                    // dd($row);
                                    $trailers += $row->Trailers;
                                    $direccionales += $row->Direccionales;
                                    $alzapatas += $row->Alzapatas;
                                    $sencillos += $row->Sencillos;
                                @endphp
                                <tr>
                                    <td class="text-right">{{ $loop->iteration }}</td>
                                    <td class="text-left">{{ $row->nombre_destino }}</td>
                                    <td class="text-right">{{ $fecha->format('d-m-Y') }}</td>
                                    {{-- <td class="text-right">{{ $row->cantidad }}</td> --}}
                                    <td class="text-center">{{ $row->tipo_pago }}</td>
                                    <td class="text-right">{{ $row->Trailers }}</td>
                                    <td class="text-right">{{ $row->Direccionales }}</td>
                                    <td class="text-right">{{ $row->Alzapatas }}</td>
                                    <td class="text-right">{{ $row->Sencillos }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="100%">Sin datos </td>
                                </tr>
                                @endforelse
                                <tr>
                                    <td class="text-right"></td>
                                    <td class="text-left"></td>
                                    <td class="text-right"></td>
                                    <td class="text-center">Totales</td>
                                    <td class="text-right">{{ $trailers }}</td>
                                    <td class="text-right">{{ $direccionales }}</td>
                                    <td class="text-right">{{ $alzapatas }}</td>
                                    <td class="text-right">{{ $sencillos }}</td>
                                </tr>
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

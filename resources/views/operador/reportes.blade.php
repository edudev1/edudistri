@extends('adminlte::page')

@section('title', 'Reportes Operador')

@section('content_header')
<h1>Reportes Operador</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @can('reportes.aux.control')
        <div class="col-md-4">
            <div class="card text-left">
              <div class="card-body">
                <h4 class="card-title">Programaciones</h4>
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fa-solid fa-layer-group"></i></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-info d-block" href="{{route('ope.ver.programacion',[now()->format('d-m-Y')])}}">Programaciones por día</a>
                    </span>
                    </div>
                </div>
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-info d-block" href="{{route('rpt.programacion.cias.show')}}">Programaciones por Sindicato</a>
                    </span>
                    </div>
                </div>

              </div>
            </div>
        </div>
        @endcan
        @can('reportes.aux.control')
        <div class="col-md-4">
            <div class="card text-left">
              {{-- <img class="card-img-top" src="https://picsum.photos/200" alt="prueba"> --}}
              <div class="card-body">
                <h4 class="card-title">Colas</h4>
                <div class="info-box">
                    <span class="info-box-icon bg-success"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-success d-block" href="{{route('excel.exports.colas')}}">Colas</a>
                    </span>
                    </div>
                </div>
              </div>
            </div>
        </div>
        @endcan
        @can('reportes.aux.control')
        <div class="col-md-4">
            <div class="card text-left">
              {{-- <img class="card-img-top" src="https://picsum.photos/200" alt="prueba"> --}}
              <div class="card-body">
                <h4 class="card-title">Pedidos</h4>
                <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-warning d-block" target="_blank" href="{{route('rpt.impr.pedidos.pendientes')}}">Pedidos Pdf</a>
                    </span>
                    </div>
                </div>
                <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-warning d-block" href="{{route('excel.exports.pedidos')}}">Pedidos Excel</a>
                    </span>
                    </div>
                </div>
              </div>
            </div>
        </div>
        @endcan
        @can('rpt.conocimientos.cargas')
        <div class="col-md-4">
            <div class="card text-left">
              {{-- <img class="card-img-top" src="https://picsum.photos/200" alt="prueba"> --}}
              <div class="card-body">
                <h4 class="card-title">Conformidad de carga</h4>
                <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-warning d-block" href="{{route('ver.conformidad')}}">Ver conformidad de carga</a>
                    </span>
                    </div>
                </div>
                {{-- <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fa-solid fa-users-viewfinder"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">
                        <a class="btn btn-sm btn-warning d-block" href="{{route('excel.exports.pedidos')}}">Pedidos Excel</a>
                    </span>
                    </div>
                </div> --}}
              </div>
            </div>
        </div>
        @endcan
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop

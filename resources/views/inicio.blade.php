<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistema de Distribución de Carga</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('/custom_css/inicio.css')}}">
    </head>

<body>
    <div class="login-wrap">
        <div class="login-html">
            @if ($errors->any())
                    <div class="mensaje-error">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ __($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="title">Sistema de Distribución de Carga</div>
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Acceso</label>
            <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Logo</label>
            <div class="login-form">

                <form action="{{route('login')}}" method="post">
                    @csrf
                    <div class="sign-in-htm">
                        <div class="group">
                            <label for="email" class="label">Nombre de usuario</label>
                            <input id="email" type="text" class="input" name="email" placeholder="correo electronico">
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Contraseña</label>
                            <input id="pass" type="password" class="input" data-type="password" name="password">
                        </div>
                        <div class="group">
                            <input id="check" type="checkbox" class="check" checked>
                            <label for="check"><span class="icon"></span> Mantenerme conectado</label>
                        </div>
                        <div class="group">
                            <input type="submit" class="button" value="Ingresar">
                        </div>
                        <div class="hr"></div>
                        {{-- <div class="foot-lnk">
                            <a href="{{route('password.reset')}}">Has olvidado tu contraseña?</a>
                        </div> --}}
                    </div>
                </form>
                <div class="sign-up-htm">
                    <div class="logo">
                        <img src="{{asset('img/logo.png')}}" alt="logo de la emrpesa">
                    </div>
                    {{-- <div class="group">
                        <label for="user" class="label">Username</label>
                        <input id="user" type="text" class="input">
                    </div>
                    <div class="group">
                        <label for="pass" class="label">Password</label>
                        <input id="pass" type="password" class="input" data-type="password">
                    </div>
                    <div class="group">
                        <label for="pass" class="label">Repeat Password</label>
                        <input id="pass" type="password" class="input" data-type="password">
                    </div>
                    <div class="group">
                        <label for="pass" class="label">Email Address</label>
                        <input id="pass" type="text" class="input">
                    </div>
                    <div class="group">
                        <input type="submit" class="button" value="Sign Up">
                    </div>
                    <div class="hr"></div>
                    <div class="foot-lnk">
                        <label for="tab-1">Already Member?</a>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" crossorigin="anonymous">
</script> --}}
</body>
</html>

<?php

namespace App\Http\Livewire;

use App\Models\TipoVehiculo;
use App\Models\TransporteCia;
use App\Models\Transportista;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Vehiculo;
use Illuminate\Support\Facades\DB;

class Vehiculos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $placa, $marca, $color, $num_chasis, $codigo_motor, $capacidad_arrastre, $crpva, $num_ci_propietario, $fecha_registro, $tipo_vehiculo_id, $transporte_cia_id, $transportista_id;
	public $tiposvehiculos,$companias,$transportistas;

    public $keyTransportista, $verDropdownList='',$listaTransportista,$nombre_completo;

    protected $listeners = ['seleccionarResultado'];


	public function mount()
	{
		$this->tiposvehiculos = TipoVehiculo::all()->pluck('tipo','id');
		$this->companias = TransporteCia::all()->pluck('nombre','id');
		$this->transportistas = Transportista::orderBy('id','desc')->get()->pluck('full_name','id');
        // dd($this->transportistas);
        //
	}

    function updatingKeyWord(){
        $this->resetPage();
    }

    public function render()
    {
		$this->buscarTransportista();
        $keyWord = '%'.$this->keyWord .'%';
        return view('livewire.vehiculos.view', [
            'vehiculos' => DB::table('vehiculos as v')
                            ->select('v.id', 'v.placa', 'v.marca', 'v.color', 'v.num_chasis', 'v.codigo_motor', 'v.capacidad_arrastre', 'v.crpva', 'v.fecha_registro', 'tv.tipo', 'tc.nombre', DB::raw("concat(t.nombres, ' ', t.apellidos) as full_name"))
                            ->join('tipo_vehiculos as tv', 'tv.id', '=', 'v.tipo_vehiculo_id')
                            ->join('transporte_cias as tc', 'tc.id', '=', 'v.transporte_cia_id')
                            ->join('transportistas as t', 't.id', '=', 'v.transportista_id')
                            ->where('v.placa', 'like', $keyWord)
                            ->orWhere('v.marca', 'like', $keyWord)
                            ->orWhere('v.color', 'like', $keyWord)
                            ->orWhere('v.num_chasis', 'like', $keyWord)
                            ->orWhere('v.codigo_motor', 'like', $keyWord)
                            ->orWhere('v.capacidad_arrastre', 'like', $keyWord)
                            ->orWhere('v.crpva', 'like', $keyWord)
                            // ->orWhere(DB::raw('DATE(v.fecha_registro)'), '=', $keyWord)
                            ->orWhere('tv.tipo', 'like', $keyWord)
                            ->orWhere('tc.nombre', 'like', $keyWord)
                            ->orWhere('t.nombres', 'like', $keyWord)
                            ->orWhere('t.apellidos', 'like', $keyWord)
						    ->paginate(20),
        ]);
    }

    public function buscarTransportista()
    {
        $keyTransportista = '%'.$this->keyTransportista .'%';
        if(strlen($keyTransportista) > 2 ){
            $this->listaTransportista = Transportista::where('nombres', 'like', $keyTransportista)
                                        ->orWhere('apellidos', 'like', $keyTransportista)
                                        ->orWhere('ci', 'like', $keyTransportista)
                                        ->select(DB::raw("CONCAT(nombres, ' ', apellidos) AS nombre_completo, id"))
                                        ->orderByRaw('nombres ASC')
                                        ->limit(10)
                                        ->get();
            $this->verDropdownList = 'show';
        }else {
            $this->verDropdownList = '';
        }
    }

    public function seleccionarResultado($resultadoId)
    {
        $transportista = Transportista::find($resultadoId);
        $this->transportista_id = $transportista->id;
        $this->nombre_completo = $transportista->nombres . ' ' . $transportista->apellidos;
        $this->keyTransportista = '';
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->placa = null;
		$this->marca = null;
		$this->color = null;
		$this->num_chasis = null;
		$this->codigo_motor = null;
		$this->capacidad_arrastre = null;
		$this->crpva = null;
		$this->num_ci_propietario = null;
		$this->fecha_registro = null;
		$this->tipo_vehiculo_id = null;
		$this->transporte_cia_id = null;
		$this->transportista_id = null;
    }

    function abrirModalCreate() {
        $this->emit('initializeSelect2');
    }
    public function store()
    {
        $this->validate([
		'placa' => 'required',
		'capacidad_arrastre' => 'required',
		'crpva' => 'required',
		'fecha_registro' => 'required|date',
		'tipo_vehiculo_id' => 'required',
		'transporte_cia_id' => 'required',
		'transportista_id' => 'required',
        ]);

        Vehiculo::create([
			'placa' => $this-> placa,
			'marca' => $this-> marca,
			'color' => $this-> color,
			'num_chasis' => $this-> num_chasis,
			'codigo_motor' => $this-> codigo_motor,
			'capacidad_arrastre' => $this-> capacidad_arrastre,
			'crpva' => $this-> crpva,
			'num_ci_propietario' => $this-> num_ci_propietario,
			'fecha_registro' => $this-> fecha_registro,
			'tipo_vehiculo_id' => $this-> tipo_vehiculo_id,
			'transporte_cia_id' => $this-> transporte_cia_id,
			'transportista_id' => $this-> transportista_id
        ]);

        $this->resetInput();
		// $this->dispatchBrowserEvent('closeModal');
        $this->emit('closeModal');
		session()->flash('message', 'Vehiculo creado correctamente.');
    }

    public function edit($id)
    {
        $record = Vehiculo::findOrFail($id);
        $this->selected_id = $id;
		$this->placa = $record-> placa;
		$this->marca = $record-> marca;
		$this->color = $record-> color;
		$this->num_chasis = $record-> num_chasis;
		$this->codigo_motor = $record-> codigo_motor;
		$this->capacidad_arrastre = $record-> capacidad_arrastre;
		$this->crpva = $record-> crpva;
		$this->num_ci_propietario = $record-> num_ci_propietario;
		$this->fecha_registro = $record-> fecha_registro;
		$this->tipo_vehiculo_id = $record-> tipo_vehiculo_id;
		$this->transporte_cia_id = $record-> transporte_cia_id;
		// $this->transportista_id = $record-> transportista_id;
        $this->seleccionarResultado($record-> transportista_id);
    }

    public function update()
    {
        $this->validate([
		'placa' => 'required',
		'capacidad_arrastre' => 'required',
		'crpva' => 'required',
		'fecha_registro' => 'required',
		'tipo_vehiculo_id' => 'required',
		'transporte_cia_id' => 'required',
		'transportista_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Vehiculo::find($this->selected_id);
            $record->update([
			'placa' => $this-> placa,
			'marca' => $this-> marca,
			'color' => $this-> color,
			'num_chasis' => $this-> num_chasis,
			'codigo_motor' => $this-> codigo_motor,
			'capacidad_arrastre' => $this-> capacidad_arrastre,
			'crpva' => $this-> crpva,
			'num_ci_propietario' => $this-> num_ci_propietario,
			'fecha_registro' => $this-> fecha_registro,
			'tipo_vehiculo_id' => $this-> tipo_vehiculo_id,
			'transporte_cia_id' => $this-> transporte_cia_id,
			'transportista_id' => $this-> transportista_id
            ]);

            $this->resetInput();
            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('closeModalEdit');
			session()->flash('message', 'Vehiculo actualizado correctamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            Vehiculo::where('id', $id)->delete();
        }
    }
}

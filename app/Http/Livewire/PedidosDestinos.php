<?php

namespace App\Http\Livewire;

use App\Models\Dato;
use App\Models\Destino;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\PedidosDestino;
use App\Models\PedidoVehiculoCarga;
use App\Models\TipoVehiculo;
use DateTime;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class PedidosDestinos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $fecha_pedido, $tipo_pago, $cantidad, $atendido, $destino_id;
    public $litaTipoPago, $listaDestinos,$fecha_pedido_update,$keyDestino, $nombre_completo;
    public $capacidadArrasteAprox=[],$tipoVehiculos, $verDropdownList='';

    public $sumaTotal = 0, $trailer = 0, $direccional = 0,$alzapata = 0,$sencillo = 0;

    protected $listeners = ['setFecha','setFechaUpdate','seleccionarResultado'];
    // protected $rules = [
    //     'fecha_pedido' => 'date',
    //     'fecha_pedido_update' => 'date',
    // ];

    protected $rules = [
        // 'fecha_pedido' => 'required|date|after:today',
		'tipo_pago' => 'required',
		'cantidad' => 'required|numeric|min:100',
		'destino_id' => 'required',
		'sumaTotal' => 'required|numeric|min:1',
    ];
    protected $ruleCreate = [
        'fecha_pedido' => 'required|date|after_or_equal:today',
    ];
    protected $ruleUpdate = [
        'fecha_pedido_update' => 'required|date',
    ];

    public function messages()
    {
        return [
            'fecha_pedido_update.required' => 'El campo fecha de pedido es obligatorio.',
            'fecha_pedido_update.date' => 'El campo fecha de pedido debe ser una fecha valida.',
            'fecha_pedido.required' => 'El campo fecha de pedido es obligatorio.',
            'fecha_pedido.date' => 'El campo fecha de pedido debe ser una fecha valida.',
            'fecha_pedido.after' => 'El campo fecha debe ser posterior a la de hoy.',
            'tipo_pago.required' => 'Debe seleccionar un tipo de pago.',
            'cantidad.required' => 'El campo cantidad es obligatorio.',
            'cantidad.numeric' => 'El campo cantidad debe ser un n+umero entero.',
            'cantidad.min' => 'El campo cantidad debe tener como minimo 100 unidades.',
            'destino_id.required' => 'Seleccione un destino.',
            'sumaTotal.min' => 'Debe existir al menos un tipo de vehículo.',
            'sumaTotal.numeric' => 'corregir suma total.',
            'sumaTotal.required' => 'Debe existir al menos un tipo de vehículo.',
        ];
    }

    public function updated($propertyName) {
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->litaTipoPago = Dato::where('tipo','tipo-pago')->get()->pluck('valor','valor');
        // $this->listaDestinos = Destino::all()->pluck('nombre_destino','id');
        $this->capacidadArrasteAprox = [
            'trailer' => '560',
            'direccional' => '400',
            'alzapata' => '300',
            'sencillo' => '200'
        ];
        $this->tipoVehiculos = TipoVehiculo::pluck('id', 'tipo')->toArray();
    }

    public function buscarDestino()
    {
        $keyDestino = '%'.$this->keyDestino .'%';
        if(strlen($keyDestino) > 2 ){
            $this->listaDestinos = Destino::select(DB::raw("CONCAT(departamento, ' - ', nombre_destino) AS nombre_completo, id"))
                                        ->where('nombre_destino', 'like', $keyDestino)
                                        ->orWhere('departamento', 'like', $keyDestino)
                                        ->orderByRaw('departamento ASC, nombre_destino ASC')
                                        ->limit(10)
                                        ->get();
            $this->verDropdownList = 'show';
        }else {
            $this->verDropdownList = '';
        }
    }

    public function seleccionarResultado($resultadoId)
    {
        $destino = null;
        foreach ($this->listaDestinos as $value) {
            if($value->id == $resultadoId){
                $destino = $value;
                break;
            }
        }
        if($destino){
            $this->destino_id = $destino->id;
            $this->nombre_completo = $destino->departamento . " - " . $destino->nombre_destino;
            $this->keyDestino = '';
        }
    }

    public function setFecha($data)
    {
        $this->fecha_pedido = $data;
        // dd($this->fecha_pedido);
    }
    public function setFechaUpdate($data)
    {
        $this->fecha_pedido_update = $data;
    }

    public function calcularCarga(){
        $this->sumaTotal = $this->trailer * $this->capacidadArrasteAprox['trailer']  + $this->direccional * $this->capacidadArrasteAprox['direccional'] + $this->alzapata * $this->capacidadArrasteAprox['alzapata'] + $this->sencillo * $this->capacidadArrasteAprox['sencillo'];
    }
    public function render()
    {
		$this->buscarDestino();
        $listaPedidos = $this->listaPedidosDestino();
        return view('livewire.pedidos-destinos.view', [
                        'listaPedidos' => $listaPedidos
        ]);
    }

    function buscarPedidosNoAtendidos($atendido=0, $asignado=0, $complemento=0) {
        $pedidosDestino = DB::table('pedidos_destino as pd')
                        ->select('pd.id as pedido_destino_id', 'pd.fecha_pedido', 'pd.tipo_pago', 'pd.cantidad', 'd.nombre_destino')
                        ->join('pedidos_vehiculos_carga as pvc', 'pd.id', '=', 'pvc.pedidos_destino_id')
                        ->join('destinos as d', 'd.id', '=', 'pd.destino_id')
                        ->where('pd.atendido', '=', $atendido)
                        ->where('pvc.asignado', '=', $asignado)
                        ->where('pvc.complemento', '=', $complemento)
                        ->get();
    }

    function listaPedidosDestino(){
        return DB::select("SELECT
                    pedido_destino_id,
                    nombre_destino,
                    fecha_pedido,
                    cantidad,
                    tipo_pago,
                    SUM(CASE WHEN tipo = 'TRAILER' THEN total_tipo_vehiculo ELSE 0 END) AS Trailers,
                    SUM(CASE WHEN tipo = 'DIRECCIONAL' THEN total_tipo_vehiculo ELSE 0 END) AS Direccionales,
                    SUM(CASE WHEN tipo = 'ALZAPATA' THEN total_tipo_vehiculo ELSE 0 END) AS Alzapatas,
                    SUM(CASE WHEN tipo = 'SENCILLO' THEN total_tipo_vehiculo ELSE 0 END) AS Sencillos
                FROM
                (
                    SELECT
                        pd.id AS pedido_destino_id,
                        d.nombre_destino,
                        pd.fecha_pedido,
                        pd.cantidad,
                        pd.tipo_pago,
                        tv.tipo,
                        COUNT(tv.id) AS total_tipo_vehiculo
                    FROM
                        pedidos_destino pd
                    LEFT JOIN
                        pedidos_vehiculos_carga pvc ON pd.id = pvc.pedidos_destino_id
                    LEFT JOIN
                        tipo_vehiculos tv ON pvc.tipo_vehiculo_id = tv.id
                    INNER JOIN
                        destinos d ON pd.destino_id = d.id
                    WHERE pd.atendido = 0
                    and pvc.asignado = 0
                    and pvc.complemento  = 0
                    -- AND NOT EXISTS (
                    --     SELECT 1
                    --     FROM programaciones p
                    --     WHERE p.pedidos_vehiculo_carga_id = pvc.id
                    -- )
                    GROUP BY
                        pd.id, d.nombre_destino, pd.fecha_pedido, pd.cantidad, pd.tipo_pago, tv.tipo
                ) AS src
                GROUP BY
                    pedido_destino_id, nombre_destino, fecha_pedido, cantidad, tipo_pago
                ORDER BY
                    fecha_pedido, pedido_destino_id, tipo_pago ASC");
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->fecha_pedido = null;
		$this->fecha_pedido_update = null;
		$this->tipo_pago = null;
		$this->cantidad = null;
		$this->atendido = null;
		$this->destino_id = null;
        $this->sumaTotal = 0;
        $this->trailer = 0;
        $this->direccional = 0;
        $this->alzapata = 0;
        $this->sencillo = 0;
        $this->nombre_completo = '';
    }

    public function store()
    {
        // dd($this->fecha_pedido);
        $this->validate(array_merge($this->rules, $this->ruleCreate));
        DB::beginTransaction();
        try {
            $pedidoDestino = PedidosDestino::create([
                'fecha_pedido' => $this-> fecha_pedido,
                'tipo_pago' => $this-> tipo_pago,
                'cantidad' => $this-> cantidad,
                'atendido' => $this-> atendido,
                'destino_id' => $this-> destino_id
            ]);
            if($this->trailer>0){
                $this->crearPedidoVehiculoCarga($this->trailer, $this->capacidadArrasteAprox['trailer'], $pedidoDestino->id, 'TRAILER');
            }
            if($this->direccional>0){
                $this->crearPedidoVehiculoCarga($this->direccional, $this->capacidadArrasteAprox['direccional'], $pedidoDestino->id, 'DIRECCIONAL');
            }
            if($this->alzapata>0){
                $this->crearPedidoVehiculoCarga($this->alzapata, $this->capacidadArrasteAprox['alzapata'], $pedidoDestino->id, 'ALZAPATA');
            }
            if($this->sencillo>0){
                $this->crearPedidoVehiculoCarga($this->sencillo, $this->capacidadArrasteAprox['sencillo'], $pedidoDestino->id, 'SENCILLO');
            }
            DB::commit();
		    session()->flash('message', 'Pedidos creados satisfactoriamente.');
        } catch (\Exception $e) {
            session()->flash('message', $e->getMessage());
            DB::rollBack();
        }
        $this->resetInput();
        $this->emit('closeModal');
    }

    public function crearPedidoVehiculoCarga($cantTipoVehiculo, $carga, $pedido_destino_id, $tipoVehiculo){
        for ($i=1; $i <= $cantTipoVehiculo; $i++) {
            $res=PedidoVehiculoCarga::create([
                'carga_vehiculo' => $carga,
                'pedidos_destino_id' => $pedido_destino_id,
                'tipo_vehiculo_id' => $this->tipoVehiculos[$tipoVehiculo],
            ]);
        }

    }

    public function edit($id)
    {
        $record = PedidosDestino::findOrFail($id);

        $date = new DateTime($record->fecha_pedido);
        $this->selected_id = $id;
		$this->fecha_pedido_update = $date->format('d-m-Y');
		$this->tipo_pago = $record-> tipo_pago;
		$this->cantidad = $record-> cantidad;
		$this->atendido = $record-> atendido;
		$this->destino_id = $record-> destino_id;
    }

    public function update()
    {
        $this->validate(array_merge($this->rules, $this->ruleUpdate));

        if ($this->selected_id) {
			$record = PedidosDestino::find($this->selected_id);
            // dd($this-> fecha_pedido_update);
            $record->update([
			'fecha_pedido' => $this-> fecha_pedido_update,
			'tipo_pago' => $this-> tipo_pago,
			'cantidad' => $this-> cantidad,
			'atendido' => $this-> atendido,
			'destino_id' => $this-> destino_id
            ]);

            $this->resetInput();
            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('closeModalEdit');
			session()->flash('message', 'Pedido actualizado.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            //borrar todos los id references en la table pedidos_vehiculos_carga
            DB::table('pedidos_vehiculos_carga')
            ->where('pedidos_destino_id', $id)
            ->delete();
            PedidosDestino::where('id', $id)->delete();
        }
    }
}

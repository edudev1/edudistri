<?php

namespace App\Http\Livewire\Cias;

use App\Models\CiasHabilitarPlaca;
use App\Models\PedidosDestino;
use App\Models\PedidoVehiculoCarga;
use App\Models\Programacion;
use App\Models\Vehiculo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use PhpParser\Node\Stmt\TryCatch;

class EnviarPlacaComplemento extends Component
{
    public $keyPlaca,$verDropdownList='', $listaPlacas, $cia_id, $placaTipo, $vehiculoIdSelect;


    function mount() {
        $cia = Auth::user()->transporteCias->first();
        $this->cia_id = $cia->id;
        // $this->fechaNow = Carbon::now();
    }

    public function render()
    {
        $this->buscarPlaca();
        return view('livewire.cias.enviar-placa-complemento');
    }

    public function buscarPlaca()
    {
        $keyPlaca = '%'.$this->keyPlaca .'%';
        if(strlen($keyPlaca) > 2 ){
            $this->listaPlacas = Vehiculo::where('placa', 'LIKE', $keyPlaca)
                                        ->where('activo', '1')
                                        ->where('transporte_cia_id', $this->cia_id)
                                        ->selectRaw("id, placa")
                                        ->get();
            $this->verDropdownList = 'show';
            // $this->cliente = null;
        }else {
            $this->verDropdownList = '';
            // $this->cliente = Persona::find($this->clienteSelected);
        }
    }

    public function seleccionarResultado($resultadoId)
    {
        // Asigna el resultado seleccionado a la propiedad $resultadoSeleccionado
        $vehiculo = Vehiculo::find($resultadoId);
        $programado = $this->buscarPlacaProgramada($vehiculo->id);
        // dd($programado);
        if (is_null($programado)) {
            $this->placaTipo = $vehiculo->placa . ' - ' . $vehiculo->tipoVehiculo->tipo;
            $this->keyPlaca = '';
            $this->vehiculoIdSelect = $resultadoId;
            // $this->render();
        }
        else{
            session()->flash('message', 'La placa no puede ser seleccionada porque tiene una programación pendiente.');
            $this->keyPlaca = '';
            $this->placaTipo = '';
            $this->vehiculoIdSelect = 0;
        }

    }

    function buscarPlacaProgramada($vehiculo_id){
        return DB::table('programaciones as p')
                    ->select('p.vehiculo_id', 'v.placa', 'tv.tipo')
                    ->join('pedidos_vehiculos_carga as pvc', 'p.pedidos_vehiculo_carga_id', '=', 'pvc.id')
                    ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
                    ->join('vehiculos as v', 'p.vehiculo_id', '=', 'v.id')
                    ->join('tipo_vehiculos as tv', 'v.tipo_vehiculo_id', '=', 'tv.id')
                    ->where('p.anulado', 0)
                    ->where('p.enviado_jde', 0)
                    ->where('p.vehiculo_id', $vehiculo_id)
                    ->first();
    }

    function enviarPlacaComplemento() {
        /** obtener todos los pedidos por tipo de vehiculo**/
        $vehiculo = Vehiculo::findOrFail($this->vehiculoIdSelect);
        $listaComplementos = $this->buscarComplentos($vehiculo->tipo_vehiculo_id);
        if (count($listaComplementos) > 0){
            // dd($listaComplementos); //considerar los vehiculos con capacidad menor y los tipos de pago para asignar un vehiculo
            $mensaje='';
            foreach($listaComplementos as $pedidoVC){
                try {
                    // dd($vehiculo);
                    DB::beginTransaction();
                    $cargaAsignada = 0;
                    if($pedidoVC->tipo_pago == 'PS'){
                        if($vehiculo->capacidad_arrastre >= $pedidoVC->carga_vehiculo){
                            $cargaAsignada = $pedidoVC->carga_vehiculo;
                        }else{
                            throw new \Exception("La capacidad de carga del vehiculo es menor al pedido solicitado.");
                        }
                    }
                    if($pedidoVC->tipo_pago == 'PA'){
                        $cargaAsignada = $vehiculo->capacidad_arrastre;
                    }
                    //guardar todos los registros nesasarios
                    $programa = Programacion::create([
                                    'codigo' => uniqid('pro-', true),
                                    'fecha' => $pedidoVC->fecha_programacion,
                                    'cantidad' => $cargaAsignada,
                                    'tipo_asignacion' => 'Complemento',
                                    'user_id' => Auth::id(),
                                    'pedidos_vehiculo_carga_id' => $pedidoVC->id,
                                    'vehiculo_id' => $vehiculo->id
                                ]);
                    // do
                    $pedidoVehiculoCarga = PedidoVehiculoCarga::find($pedidoVC->id);
                    $pedidoVehiculoCarga->asignado = 1;
                    $pedidoVehiculoCarga->save();
                    $this->verificarPedidoAtendido($pedidoVehiculoCarga->pedidos_destino_id);
                    DB::commit();
                    session()->flash('message', 'La placa fue asignada correctamente.');
                    $this->keyPlaca = '';
                    $this->placaTipo = '';
                    $this->vehiculoIdSelect = 0;
                    $this->emit('refrescarListaComplementos');
                    break;
                } catch (\Exception $e) {
                    DB::rollback();
                    $mensaje .=$e->getMessage();
                    session()->flash('message', $mensaje);
                    CONTINUE;
                }

            }
        }else{
            session()->flash('message', 'La placa no puede ser seleccionada porque no existe un pedido con su tipo de vehiculo.');
        }

    }

    /**
     * verificar si todos los PedidosVehiculoCarga fueron asignados para modificar atendido=1 en pedidos_destino
     **/
    function verificarPedidoAtendido($pedidosDestinoId){
        $cantidad = DB::table('pedidos_vehiculos_carga as pvc')
                        ->where('pvc.asignado', 0)
                        ->where('pvc.pedidos_destino_id', $pedidosDestinoId)
                        ->count();
        // dd($cantidad);
        if($cantidad == 0){
            $pedidoDestino = PedidosDestino::find($pedidosDestinoId);
            $pedidoDestino->atendido = 1;
            $pedidoDestino->save();
        }

    }

    private function buscarComplentos($tipoVehiculoId, $complemento=1, $publicar=1){
        return DB::table('pedidos_vehiculos_carga as pvc')
                    ->leftJoin('programaciones as p', 'pvc.id', '=', 'p.pedidos_vehiculo_carga_id')
                    ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
                    ->where('pvc.complemento', $complemento)
                    ->where('pvc.publicar', $publicar)
                    ->where('pvc.tipo_vehiculo_id', $tipoVehiculoId)
                    ->whereNull('p.pedidos_vehiculo_carga_id')
                    ->selectRaw('pvc.*, pd.tipo_pago')
                    // ->groupByRaw('pvc.tipo_vehiculo_id')
                    ->get();

    }
}

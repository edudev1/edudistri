<?php

namespace App\Http\Livewire\Cias;

use App\Models\CiasHabilitarPlaca;
use App\Models\Transportista;
use App\Models\Vehiculo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class HabilitarPlacas extends Component
{
    use WithPagination;
	protected $paginationTheme = 'bootstrap';

    public $cia_id, $keyPlaca, $listaPlacas, $placasEnvio=[],$fechaNow;
    public $verDropdownList = '';
    public $num_celular, $transportista_id_selected;


    function mount() {
        $cia = Auth::user()->transporteCias->first();
        $this->cia_id = $cia->id;
        $this->fechaNow = Carbon::now();
    }
    public function render()
    {
        $this->buscarPlaca();
        return view('livewire.cias.habilitar-placas',[
            'placasHabilitas' => DB::table('cias_habilitar_placas as chp')
                                ->join('vehiculos as v', 'chp.placa', '=', 'v.placa')
                                ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
                                ->join('transportistas as t', 'v.transportista_id', '=', 't.id')
                                ->select('chp.*', 'v.id as vehiculo_id', 'v.transportista_id', 'v.capacidad_arrastre', 't.num_celular')
                                ->where('chp.programado', 0)
                                ->where('tc.id', $this->cia_id)
                                ->get(),
        ]);
    }

    public function buscarPlaca()
    {
        $keyPlaca = '%'.$this->keyPlaca .'%';
        if(strlen($keyPlaca) > 2 ){
            $this->listaPlacas = Vehiculo::where('placa', 'LIKE', $keyPlaca)
                                        ->where('activo', '1')
                                        ->where('transporte_cia_id', $this->cia_id)
                                        ->selectRaw("id, placa")
                                        ->get();
            $this->verDropdownList = 'show';
            // $this->cliente = null;
        }else {
            $this->verDropdownList = '';
            // $this->cliente = Persona::find($this->clienteSelected);
        }
    }

    public function seleccionarResultado($resultadoId)
    {
        // Asigna el resultado seleccionado a la propiedad $resultadoSeleccionado
        $vehiculo = Vehiculo::find($resultadoId);
        $placaHabilitada = CiasHabilitarPlaca::where('placa', $vehiculo->placa)
                                             ->where('programado',0)
                                             ->first();
        if(is_null($placaHabilitada)){
            $this->placasEnvio[] = $vehiculo;
            CiasHabilitarPlaca::create(['placa' => $vehiculo->placa,'cia_id'=>$this->cia_id]);
            $this->keyPlaca = '';
            $this->render();
        }else{
            session()->flash('message', 'La placa ya se encuentra en la lista.');
        }
    }

    function eliminarPlaca(CiasHabilitarPlaca $ciaPlacaId) {
        if ($ciaPlacaId->programado == 0) {
            $ciaPlacaId->delete();
        }else {
            session()->flash('message', 'No se pudo borrar porque la placa ya fue programada.');
        }
    }

    function enviarPlacas(){
        $result = DB::table('cias_habilitar_placas')
                    ->where('enviado', 0)
                    ->update(['enviado' => 1]);
        session()->flash('message', 'Se enviaron '. $result . ' placas, para su revisión');
    }

    function edit($transportistaId){
        $transportista = Transportista::find($transportistaId);
        $this->num_celular = $transportista->num_celular;
        $this->transportista_id_selected = $transportista->id;
    }

    function update(){
        $transportista = Transportista::find($this->transportista_id_selected );
        $transportista->num_celular = $this->num_celular;
        $transportista->save();
        $this->cancel();
        $this->emit('closeModalEdit');
    }

    function cancel() {
        $this->num_celular = 0;
        $this->transportista_id_selected = 0;
        $this->emit('closeModalEdit');
    }

}

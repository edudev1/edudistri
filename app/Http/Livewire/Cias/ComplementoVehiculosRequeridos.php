<?php

namespace App\Http\Livewire\Cias;

use App\Models\TipoVehiculo;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ComplementoVehiculosRequeridos extends Component
{
    public $tipoVehiculos = [];

    protected $listeners = ['refrescarListaComplementos'=> '$refresh'];

    function mount(){
        $this->tipoVehiculos = TipoVehiculo::get()->pluck('tipo','id')->toArray();
    }
    public function render()
    {
        $listaComplementos = DB::table('pedidos_vehiculos_carga as pvc')
        ->leftJoin('programaciones as p', 'pvc.id', '=', 'p.pedidos_vehiculo_carga_id')
        ->where('pvc.complemento', 1)
        // ->where('pvc.publicar', 0)
        ->whereNull('p.pedidos_vehiculo_carga_id')
        ->selectRaw('pvc.tipo_vehiculo_id, count(pvc.tipo_vehiculo_id) as cantidad')
        ->groupByRaw('pvc.tipo_vehiculo_id')
        ->get();


        return view('livewire.cias.complemento-vehiculos-requeridos',['listaComplementos'=>$listaComplementos]);
    }

    // public function refrescarListaComplementos() {
    //     $this->refresh();
    // }
}

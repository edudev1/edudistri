<?php

namespace App\Http\Livewire\Cias;

use App\Models\CiasHabilitarPlaca;
use Livewire\Component;

class UpdateConocimientoEnviado extends Component
{
    public $cia_habilitar_placa_id, $isChecked;
    public $cia_habilitar_placa;

    function mount(){
        $this->cia_habilitar_placa = CiasHabilitarPlaca::findOrFail($this->cia_habilitar_placa_id);
        $this->isChecked = $this->cia_habilitar_placa->conocimiento_enviado;
    }

    public function render()
    {
        // dd($this->cia_habilitar_placa_id);
        return view('livewire.cias.update-conocimiento-enviado');
    }

    public function actualizarConocimientoEnviado() {
        $this->cia_habilitar_placa->conocimiento_enviado = $this->isChecked?1:0;
        // dd($this->cia_habilitar_placa);
        $this->cia_habilitar_placa->save();
        // session()->flash('message', 'Se actualizo el campo conocimiento enviado de la placa .'. $this->cia_habilitar_placa->placa);
    }
}

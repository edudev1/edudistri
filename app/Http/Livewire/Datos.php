<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Dato;

class Datos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipo, $valor, $activo;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.datos.view', [
            'datos' => Dato::latest()
						->orWhere('tipo', 'LIKE', $keyWord)
						->orWhere('valor', 'LIKE', $keyWord)
						->orWhere('activo', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->tipo = null;
		$this->valor = null;
		$this->activo = null;
    }

    public function store()
    {
        $this->validate([
		'tipo' => 'required',
		'valor' => 'required',
		'activo' => 'required',
        ]);

        Dato::create([
			'tipo' => $this-> tipo,
			'valor' => $this-> valor,
			'activo' => $this-> activo
        ]);

        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Dato creado correctamente.');
    }

    public function edit($id)
    {
        $record = Dato::findOrFail($id);
        $this->selected_id = $id;
		$this->tipo = $record-> tipo;
		$this->valor = $record-> valor;
		$this->activo = $record-> activo;
    }

    public function update()
    {
        $this->validate([
		'tipo' => 'required',
		'valor' => 'required',
		'activo' => 'required|numeric|min:0|max:1',
        ]);

        if ($this->selected_id) {
			$record = Dato::find($this->selected_id);
            $record->update([
			'tipo' => $this-> tipo,
			'valor' => $this-> valor,
			'activo' => $this-> activo
            ]);

            $this->resetInput();
            $this->emit('closeModalEdit');
			session()->flash('message', 'Dato actualizado correctamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            Dato::where('id', $id)->delete();
        }
    }
}

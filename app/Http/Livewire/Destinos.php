<?php

namespace App\Http\Livewire;

use App\Models\Dato;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Destino;

class Destinos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipo_destino, $nombre_destino, $departamentoSelected;
    public $lista_destinos, $listaDepartamentos;

    public function mount()
    {
        $this->lista_destinos = Dato::where('tipo','tipo-destino')->get()->pluck('valor','valor');
        $this->listaDepartamentos = Dato::where('tipo','departamentos')->get()->pluck('valor','valor');
    }
    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.destinos.view', [
            'destinos' => Destino::orderBy('id', 'DESC')
						->orWhere('tipo_destino', 'LIKE', $keyWord)
						->orWhere('nombre_destino', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->tipo_destino = null;
		$this->nombre_destino = null;
    }

    public function store()
    {
        $this->validate([
		'tipo_destino' => 'required',
		'nombre_destino' => 'required',
		'departamentoSelected' => 'required',
        ]);

        Destino::create([
			'tipo_destino' => $this-> tipo_destino,
			'nombre_destino' => $this-> nombre_destino,
			'departamento' => $this-> departamentoSelected
        ]);

        $this->resetInput();
		// $this->dispatchBrowserEvent('closeModal');
        $this->emit('closeModal');
		session()->flash('message', 'Destino creado satisfactoriamente.');
    }

    public function edit($id)
    {
        $record = Destino::findOrFail($id);
        $this->selected_id = $id;
		$this->tipo_destino = $record-> tipo_destino;
        $this->departamentoSelected = $record->departamento;
		$this->nombre_destino = $record-> nombre_destino;
    }

    public function update()
    {
        $this->validate([
		'tipo_destino' => 'required',
		'departamentoSelected' => 'required',
		'nombre_destino' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Destino::find($this->selected_id);
            $record->update([
			'tipo_destino' => $this-> tipo_destino,
			'nombre_destino' => $this-> nombre_destino,
			'departamento' => $this-> departamentoSelected
            ]);

            $this->resetInput();
            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('closeModalEdit');
			session()->flash('message', 'Destino Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            Destino::where('id', $id)->delete();
        }
    }
}

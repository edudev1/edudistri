<?php

namespace App\Http\Livewire\Utiles;

use App\Models\CiasHabilitarPlaca;
use App\Models\Cola;
use App\Models\ConocimientoCarga;
use App\Models\ConocimientoCargaJde;
use App\Models\Destino;
use App\Models\TransporteCia;
use App\Models\Vehiculo;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CrearConocimientoCarga extends Component
{
    public $modalId, $cia;
    public $inputValue;
    public $numero_carga, $placa,  $fecha_carguio, $destinoSelected,$cias_habilitar_placas;
    /**buscar destino */
    public $listaDestinos;
    public $verDropdownList,$destino_id,$nombre_completo,$keyDestino;
    /**end buscar destiono */
    // public $enviarForm;

    protected $listeners = ['setDestino','setFecha'];

    public function rules()
    {
        return [
            'numero_carga' => 'required|numeric|unique:conocimiento_cargas,numero_carga',
            'fecha_carguio' => 'required|date',
            'destino_id' => 'required|integer', //ultimo_destino_id
            // 'cias_habilitar_placas_id' => 'required|integer', //
        ];
    }

    public function messages()
    {
        return [
            'numero_carga.required' => 'El número de carga es obligatorio.',
            'numero_carga.numeric' => 'El número de carga debe ser numérico.',
            'numero_carga.unique' => 'El número de carga ya existe en la base de datos.',
            'placa.required' => 'La placa es obligatoria.',
            'placa.string' => 'La placa debe ser una cadena de texto.',
            'fecha_carguio.required' => 'La fecha de carguío es obligatoria.',
            'fecha_carguio.date' => 'La fecha de carguío debe tener un formato válido.',
            'destino_id.required' => 'El último destino es obligatorio.',
            // 'destinoSelected.integer' => 'El último destino debe ser un número entero.',
            // 'cias_habilitar_placas_id.required' => 'Las compañías habilitadas para placas son obligatorias.',
            // 'cias_habilitar_placas_id.array' => 'Las compañías habilitadas para placas deben ser un arreglo.',
        ];
    }

    public function mount($id, $cia_id)
    { //el id obtenido es chp_id entonce es id de cias_habilitar_placas
        // $this->enviarForm = 1;
        $this->cia = TransporteCia::find($cia_id);
        $this->cias_habilitar_placas = CiasHabilitarPlaca::find($id);
        $this->placa = $this->cias_habilitar_placas->placa;
        $this->cargarDatosConocimientoJde($this->placa);
        $this->modalId = $id;
        $this->listaDestinos = Destino::select('id','departamento','nombre_destino')
                                    //   ->orderBy('departamento', 'asc')
                                    //   ->orderBy('nombre_destino', 'asc')
                                    ->select(DB::raw("CONCAT(departamento, ' - ', nombre_destino) AS nombre_completo, id"))
                                    ->orderByRaw('departamento ASC, nombre_destino ASC')
                                    ->get();
                                    //->pluck('nombre_completo','id');
        // dd($this->listaDestinos);
        //                               $listaDestinos = $listaDestinos->map(function ($item) {
        //     $nombreCompleto = $item->departamento . ' ' . $item->nombre_destino;
        //     $item->nombreCompleto = $nombreCompleto;
        //     return $item;
        // });
        // $this->listaDestinos = $listaDestinos->pluck('nombreCompleto', 'id');
        //   DB::raw("CONCAT(departamento, ' - ', nombre_destino) AS nombre_completo"), 'id'->pluck('nombre_completo', 'id');
    }

    function cargarDatosConocimientoJde($placa) {
        //buscamos en conocimiento de carga jde por placa
        $conocimientoJde = ConocimientoCargaJde::where('placa',$placa)->orderBy('fecha','desc')->first();
        // dd($conocimientoJde);
        if ($conocimientoJde) {
            //buscamos un id destino
            $this->numero_carga = $conocimientoJde->numero_carga;
            $this->fecha_carguio = $conocimientoJde->fecha;
            $destino = Destino::where('nombre_destino', $conocimientoJde->ciudad)->first();
            // dd($destino);
            if ($destino) {
                $this->destino_id = $destino->id;
                $this->nombre_completo = $destino->departamento . " - " . $destino->nombre_destino;
                $this->keyDestino = '';
            }
        }


    }
/** buscar destino */
public function buscarDestino()
    {
        $keyDestino = '%'.$this->keyDestino .'%';
        if(strlen($keyDestino) > 2 ){
            $this->listaDestinos = Destino::select(DB::raw("CONCAT(departamento, ' - ', nombre_destino) AS nombre_completo, id"))
                                        ->where('nombre_destino', 'like', $keyDestino)
                                        ->orderByRaw('departamento ASC, nombre_destino ASC')
                                        ->limit(10)
                                        ->get();
            $this->verDropdownList = 'show';
        }else {
            $this->verDropdownList = '';
        }
    }

    public function seleccionarResultado($resultadoId)
    {
        $destino = null;
        foreach ($this->listaDestinos as $value) {
            if($value->id == $resultadoId){
                $destino = $value;
                break;
            }
        }
        if($destino){
            $this->destino_id = $destino->id;
            $this->nombre_completo = $destino->departamento . " - " . $destino->nombre_destino;
            $this->keyDestino = '';
        }
    }

/** hasta aqui buscar destino */
    function setDestino($data){
        // dd("llega");
        $this->destinoSelected = $data;
    }
    public function render()
    {
        $this->buscarDestino();
        return view('livewire.utiles.crear-conocimiento-carga');
    }

    public function setFecha($data)
    {
        $this->fecha_carguio = $data;
    }

    function store(){
        $this->validate();
        // $this->enviarForm = 0;
        //todos los campos estan validados y existen
        //obtenemos el ultidmo destino
        $vehiculo = Vehiculo::where('placa', $this->placa)->where('activo', '1')->first();
        $ultimoDestino = Destino::find($this->destino_id);
        $tipoCola = config('app.cola_destino');

        DB::beginTransaction();
        try {
            //verificar que solo se ingrese un conocimiento de carga unico en el codigo
            $conocimientoCarga = ConocimientoCarga::create([
                'numero_carga' => $this->numero_carga,
                'placa' => $this->placa,
                'fecha_carguio' => $this->fecha_carguio,
                'habilitado' => 1,
                'ultimo_destino_id' => $this->destino_id,
                'cias_habilitar_placas_id' => $this->cias_habilitar_placas->id,
            ]);
            //crear registro de cola
            $cola = Cola::create([
                'tipo_cola' => $tipoCola[$ultimoDestino->tipo_destino],
                'fecha' => $this->fecha_carguio,
                'vehiculo_id' => $vehiculo->id,
                'conocimiento_carga_id' => $conocimientoCarga->id,
            ]);
            $this->cias_habilitar_placas->programado = 1;
            $this->cias_habilitar_placas->save();
            DB::commit();
            session()->flash('message', 'Se habilitó la placa: '.$this->placa );
            // $this->emit('closeModal');
            return redirect()->route('aux.placas.compania',[$this->cia->id]);
        } catch (\Exception $e) {
            // En caso de error, deshacemos la transacción $e->getMessage()
            DB::rollback();
            session()->flash('message', 'Ocurrió un error! Verifique no haya ingresado información repetida ' );

        }
    }
}

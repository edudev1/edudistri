<?php

namespace App\Http\Livewire\Auxiliar;

use App\Models\CiasHabilitarPlaca;
use App\Models\TransporteCia;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class HabilitarPlaca extends Component
{
    public $cia_id, $placa;
    public $cia;

    public function rules()
    {
        return [
            'placa' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'placa' => 'Debe ingresar un placa',
        ];
    }

    public function mount() {
        $this->cia = TransporteCia::find($this->cia_id);
    }

    public function render()
    {
        return view('livewire.auxiliar.habilitar-placa');
    }

    function cancel() {
        $this->emit('closeModalEdit');
    }

    function ingresarNuevaPlaca() {
        $this->validate();
        $placaCia = DB::table('transporte_cias as tc')
                    ->join('vehiculos as v', 'tc.id', '=', 'v.transporte_cia_id')
                    ->select('tc.id as tc_id','v.id as vehiculo_id', 'v.placa')
                    ->where('v.activo', '1')
                    ->where('tc.id', $this->cia->id)
                    ->where('v.placa', $this->placa)
                    ->first();
        // dd($placaCia);
        if(is_null($placaCia)){
            session()->flash('message', 'La placa no pertenece al este sindicato');
        } else {
            $placaEnLista = DB::table('cias_habilitar_placas as chp')
                        ->select('chp.id as chp_id', 'chp.fecha', 'chp.placa', 'chp.cia_id', 'chp.conocimiento_enviado', 'c.id as cola_id', 'c.tipo_cola', 'c.created_at as cola_fecha', 'c.asignado', 'c.preasignado')
                        ->join('transporte_cias as tc', 'chp.cia_id', '=', 'tc.id')
                        ->join('vehiculos as v', function ($join) {
                            $join->on('tc.id', '=', 'v.transporte_cia_id')
                                ->on('chp.placa', '=', 'v.placa');
                        })
                        ->leftJoin('colas as c', 'v.id', '=', 'c.vehiculo_id')
                        ->where('chp.enviado', '1')
                        ->where('chp.programado', '0')
                        ->where('tc.id', $this->cia->id)
                        ->where('v.placa', $this->placa)
                        ->first();
            if(is_null($placaEnLista)){
                $prueba = CiasHabilitarPlaca::create(['placa' => $this->placa,'cia_id'=>$this->cia_id,
                                                    'enviado' => '1', 'conocimiento_enviado' => '1']);
                // dd($prueba);
                session()->flash('message', 'Se añadio una nueva placa para su revisión');
                return redirect()->route('aux.placas.compania',[$this->cia->id]);
            }else{
                session()->flash('message', 'La placa ya se encuentra en la lista de este sindicato');
            }
        }
    }
}

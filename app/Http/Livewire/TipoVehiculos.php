<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TipoVehiculo;

class TipoVehiculos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipo, $descripcion;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.tipo-vehiculos.view', [
            'tipoVehiculos' => TipoVehiculo::orWhere('tipo', 'LIKE', $keyWord)
						->orWhere('descripcion', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->tipo = null;
		$this->descripcion = null;
    }

    public function store()
    {
        $this->validate([
		'tipo' => 'required',
		'descripcion' => 'required',
        ]);

        TipoVehiculo::create([
			'tipo' => $this-> tipo,
			'descripcion' => $this-> descripcion
        ]);

        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'TipoVehiculo Successfully created.');
    }

    public function edit($id)
    {
        $record = TipoVehiculo::findOrFail($id);
        $this->selected_id = $id;
		$this->tipo = $record-> tipo;
		$this->descripcion = $record-> descripcion;
    }

    public function update()
    {
        $this->validate([
		'tipo' => 'required',
		'descripcion' => 'required',
        ]);

        if ($this->selected_id) {
			$record = TipoVehiculo::find($this->selected_id);
            $record->update([
			'tipo' => $this-> tipo,
			'descripcion' => $this-> descripcion
            ]);

            $this->resetInput();
            $this->emit('closeModalEdit');
			session()->flash('message', 'TipoVehiculo Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            TipoVehiculo::where('id', $id)->delete();
        }
    }
}

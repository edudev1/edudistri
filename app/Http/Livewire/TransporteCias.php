<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TransporteCia;

class TransporteCias extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $nombre, $direccion, $num_telefono, $email, $created_at;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.transporte-cias.view', [
            'transporteCias' => TransporteCia::latest()
                        ->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('direccion', 'LIKE', $keyWord)
						->orWhere('num_telefono', 'LIKE', $keyWord)
						->orWhere('email', 'LIKE', $keyWord)
						// ->orWhere('created_at', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->nombre = null;
		$this->direccion = null;
		$this->num_telefono = null;
		$this->email = null;
		$this->created_at = null;
    }

    public function store()
    {
        $this->validate([
		'nombre' => 'required',
		// 'created_at' => 'required',
        ]);

        TransporteCia::create([
			'nombre' => $this-> nombre,
			'direccion' => $this-> direccion,
			'num_telefono' => $this-> num_telefono,
			'email' => $this-> email,
			// 'created_at' => now()
        ]);

        $this->resetInput();
		// $this->dispatchBrowserEvent('closeModal');
        $this->emit('closeModal');
		session()->flash('message', 'TransporteCia Successfully created.');
    }

    public function edit($id)
    {
        $record = TransporteCia::findOrFail($id);
        $this->selected_id = $id;
		$this->nombre = $record-> nombre;
		$this->direccion = $record-> direccion;
		$this->num_telefono = $record-> num_telefono;
		$this->email = $record-> email;
		$this->created_at = $record-> created_at;
    }

    public function update()
    {
        $this->validate([
		'nombre' => 'required',
		// 'created_at' => 'required',
        ]);

        if ($this->selected_id) {
			$record = TransporteCia::find($this->selected_id);
            $record->update([
			'nombre' => $this-> nombre,
			'direccion' => $this-> direccion,
			'num_telefono' => $this-> num_telefono,
			'email' => $this-> email,
			// 'created_at' => now()
            ]);

            $this->resetInput();
            // $this->dispatchBrowserEvent('closeModal');
            $this->emit('closeModalEdit');
			session()->flash('message', 'TransporteCia actualizado correctamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            TransporteCia::where('id', $id)->delete();
        }
    }
}

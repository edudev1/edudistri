<?php

namespace App\Http\Livewire\UtilPlaca;

use App\Models\ConocimientoCargaJde;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class BuscarViajesPlaca extends Component
{
    use WithPagination;
	protected $paginationTheme = 'bootstrap';

    public $destinos, $verResultado, $buscarPlaca;

    public function mount() {
        $this->verResultado = false;
    }

    public function render()
    {
        return view('livewire.util-placa.buscar-viajes-placa');
    }

    public function buscarDestinosPlaca(){
        $this->destinos = ConocimientoCargaJde::where('placa', $this->buscarPlaca)->orderBy('fecha', 'desc')->get();
        // dd($this->destinos);
        $this->verResultado=false;
        if($this->destinos->count()) {
            $this->verResultado=true;
        }
        $this->buscarPlaca = "";
        $this->render();
    }
}

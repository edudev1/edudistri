<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Transportista;

class Transportistas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $ci, $nit, $nombres, $apellidos, $num_celular, $num_celular_ref, $direccion, $created_at;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.transportistas.view', [
            'transportistas' => Transportista::latest()
						->orWhere('ci', 'LIKE', $keyWord)
						->orWhere('nit', 'LIKE', $keyWord)
						->orWhere('nombres', 'LIKE', $keyWord)
						->orWhere('apellidos', 'LIKE', $keyWord)
						->orWhere('num_celular', 'LIKE', $keyWord)
						->orWhere('num_celular_ref', 'LIKE', $keyWord)
						->orWhere('direccion', 'LIKE', $keyWord)
						// ->orWhere('created_at', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
    }

    private function resetInput()
    {
		$this->ci = null;
		$this->nit = null;
		$this->nombres = null;
		$this->apellidos = null;
		$this->num_celular = null;
		$this->num_celular_ref = null;
		$this->direccion = null;
		$this->created_at = null;
    }

    public function store()
    {
        $this->validate([
		'ci' => 'required',
		'nombres' => 'required',
		'apellidos' => 'required',
		// 'created_at' => 'required',
        ]);

        Transportista::create([
			'ci' => $this-> ci,
			'nit' => $this-> nit,
			'nombres' => $this-> nombres,
			'apellidos' => $this-> apellidos,
			'num_celular' => $this-> num_celular,
			'num_celular_ref' => $this-> num_celular_ref,
			'direccion' => $this-> direccion,
			// 'created_at' => $this-> created_at
        ]);

        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Transportista Successfully created.');
    }

    public function edit($id)
    {
        $record = Transportista::findOrFail($id);
        $this->selected_id = $id;
		$this->ci = $record-> ci;
		$this->nit = $record-> nit;
		$this->nombres = $record-> nombres;
		$this->apellidos = $record-> apellidos;
		$this->num_celular = $record-> num_celular;
		$this->num_celular_ref = $record-> num_celular_ref;
		$this->direccion = $record-> direccion;
		// $this->created_at = $record-> created_at;
    }

    public function update()
    {
        $this->validate([
		'ci' => 'required',
		'nombres' => 'required',
		'apellidos' => 'required',
		// 'created_at' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Transportista::find($this->selected_id);
            $record->update([
			'ci' => $this-> ci,
			'nit' => $this-> nit,
			'nombres' => $this-> nombres,
			'apellidos' => $this-> apellidos,
			'num_celular' => $this-> num_celular,
			'num_celular_ref' => $this-> num_celular_ref,
			'direccion' => $this-> direccion,
			// 'created_at' => $this-> created_at
            ]);

            $this->resetInput();
            $this->emit('closeModalEdit');
			session()->flash('message', 'Transportista Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            Transportista::where('id', $id)->delete();
        }
    }
}

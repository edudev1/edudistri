<?php

namespace App\Http\Controllers;

use App\Imports\ConocimientoCargaJdeImports;
use App\Models\ConocimientoCargaJde;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportJdeController extends Controller
{
    public function importConocimientoJdeShow()
    {
        $conocimientoCargaJde = ConocimientoCargaJde::orderBy('fecha','desc')->paginate(20);
        // dd($conocimientoCargaJde->first());
        return view('imports.conocimiento-jde')->with(compact('conocimientoCargaJde'));
    }

    public function importConocimientoJdeStore(Request $request)
    {
        // dd($request->all());
        $archivo = $request->file('fexcel');

        Excel::import(new ConocimientoCargaJdeImports, $archivo);
        // return view('imports.conocimiento-jde');
        return redirect()->route('import.excel.conocimiento.jde.show');
    }
}

<?php

namespace App\Http\Controllers\Operador;

use App\Http\Controllers\Controller;
use App\Models\CiasHabilitarPlaca;
use App\Models\Cola;
use App\Models\ConocimientoCarga;
use App\Models\PedidosDestino;
use App\Models\PedidoVehiculoCarga;
use App\Models\Programacion;
use App\Models\TipoVehiculo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OperadorController extends Controller
{
    function programacion() {
        $listaPedidos = DB::select("SELECT
        pedido_destino_id,
        nombre_destino,
        fecha_pedido,
        cantidad,
        tipo_pago,
        SUM(CASE WHEN tipo = 'TRAILER' THEN total_tipo_vehiculo ELSE 0 END) AS Trailers,
        SUM(CASE WHEN tipo = 'DIRECCIONAL' THEN total_tipo_vehiculo ELSE 0 END) AS Direccionales,
        SUM(CASE WHEN tipo = 'ALZAPATA' THEN total_tipo_vehiculo ELSE 0 END) AS Alzapatas,
        SUM(CASE WHEN tipo = 'SENCILLO' THEN total_tipo_vehiculo ELSE 0 END) AS Sencillos
    FROM
    (
        SELECT
            pd.id AS pedido_destino_id,
            d.nombre_destino,
            pd.fecha_pedido,
            pd.cantidad,
            pd.tipo_pago,
            tv.tipo,
            COUNT(tv.id) AS total_tipo_vehiculo
        FROM
            pedidos_destino pd
        LEFT JOIN
            pedidos_vehiculos_carga pvc ON pd.id = pvc.pedidos_destino_id
        LEFT JOIN
            tipo_vehiculos tv ON pvc.tipo_vehiculo_id = tv.id
        INNER JOIN
            destinos d ON pd.destino_id = d.id
        WHERE pd.atendido = 0
        and pvc.asignado = 0
        and pvc.complemento  = 0
        AND NOT EXISTS (
            SELECT 1
            FROM programaciones p
            WHERE p.pedidos_vehiculo_carga_id = pvc.id
        )
        GROUP BY
            pd.id, d.nombre_destino, pd.fecha_pedido, pd.cantidad, pd.tipo_pago, tv.tipo
    ) AS src
    GROUP BY
        pedido_destino_id, nombre_destino, fecha_pedido, cantidad, tipo_pago
    ORDER BY
        fecha_pedido, pedido_destino_id, tipo_pago ASC");


        $pedidosXTipo = DB::table('pedidos_destino as pd')
                            ->select('pd.tipo_pago', DB::raw('count(*) as total_vehiculos'))
                            ->join('pedidos_vehiculos_carga as pvc', 'pvc.pedidos_destino_id', '=', 'pd.id')
                            ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
                            ->where('pd.atendido', 0)
                            ->where('pvc.asignado', 0)
                            ->where('pvc.complemento', 0)
                            ->groupBy('pd.tipo_pago')
                            ->get();
        $pedidosComplementoXTipo = DB::table('pedidos_destino as pd')
                            ->select('pd.tipo_pago', DB::raw('count(*) as total_vehiculos'))
                            ->join('pedidos_vehiculos_carga as pvc', 'pvc.pedidos_destino_id', '=', 'pd.id')
                            ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
                            ->where('pd.atendido', 0)
                            ->where('pvc.asignado', 0)
                            ->where('pvc.complemento', 1)
                            ->groupBy('pd.tipo_pago')
                            ->get();
        $numPlacasXColas = DB::table('colas as c')
                                ->join('vehiculos as v', 'v.id', '=', 'c.vehiculo_id')
                                ->join('tipo_vehiculos as tv', 'tv.id', '=', 'v.tipo_vehiculo_id')
                                ->where('c.preasignado', 0)
                                ->where('c.asignado', 0)
                                ->groupBy('c.tipo_cola', 'tv.tipo')
                                ->orderBy('c.tipo_cola', 'asc')
                                ->select('c.tipo_cola', 'tv.tipo', DB::raw('COUNT(*) as cantidad'))
                                ->get();
        // dd($numPlacasXColas);
        return view('operador.programacion')->with(compact('pedidosXTipo','pedidosComplementoXTipo','numPlacasXColas','listaPedidos'));
    }

    function generarProgramacion(Request $request){
        $reglas = [
                    'fecha_programacion' => 'required|date|after_or_equal:today',
                  ];
        $mensajes = [
                        'fecha_programacion.required' => 'La fecha es obligatoria.',
                        'fecha_programacion.date' => 'El valor proporcionado para la fecha no es una fecha válida.',
                        'fecha_programacion.after_or_equal' => 'La fecha debe ser igual o posterior a hoy.',
                    ];
        $validador = Validator::make($request->all(), $reglas,$mensajes);
        if ($validador->fails()) {
            return redirect()->back()->withErrors($validador)->withInput();
        }
        // dd($request->all());
        $tiposPagos = ['PS','PA'];
        $fechaProgramacion=$request->get('fecha_programacion');
        $tipoDestinos = ['LP','LI','LS'];
        // $tipoDestinos = ['LI','LS'];
        // $tipoDestinos = ['LS'];
        // $tipoPago = 'PS';
        foreach ($tiposPagos as $tipoPago) {
            foreach ($tipoDestinos as $tipoDestino) {
                $this->asignarCargaVehiculo($fechaProgramacion,$tipoPago,$tipoDestino);
            }
        }

        return redirect()->route('ope.ver.programacion',[$fechaProgramacion]);
    }

    function asignarCargaVehiculo($fechaProgramacion,$tipoPago, $tipoDestino) {
        $pedidosDestinos = $this->buscarPedidosDestinoNumVehiculos($tipoPago, $tipoDestino,$fechaProgramacion);
        // dd($pedidosDestinos);
        $pedidos_destino_id = 0;
        $restoFaltaSumado = 0;
        foreach ($pedidosDestinos as $pedido) {
            switch ($tipoDestino) {
                case 'LS':
                    $vehiculo = $this->buscarVehiculoEnCola('UVP', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                    if(is_null($vehiculo)){
                        $vehiculo = $this->buscarVehiculoEnCola('UVI', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                        if(is_null($vehiculo)){
                            $vehiculo = $this->buscarVehiculoEnCola('UVS', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                            if(is_null($vehiculo)){
                                //si no se encuentra en ninguna cola crear complemento y saltar a la siguiente iteraccion CONTINUE
                                $pedidoVehiculoCarga = PedidoVehiculoCarga::find($pedido->pvc_id);
                                $pedidoVehiculoCarga->complemento = 1;
                                $pedidoVehiculoCarga->save();
                                continue 2;
                            }
                        }
                    }
                    break;

                default:
                    $vehiculo = $this->buscarVehiculoEnCola('UVS', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                    if(is_null($vehiculo)){
                        $vehiculo = $this->buscarVehiculoEnCola('UVI', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                        if(is_null($vehiculo)){
                            $vehiculo = $this->buscarVehiculoEnCola('UVP', $pedido->tipo_vehiculo_id, $pedido->carga_vehiculo, $tipoPago);
                            if(is_null($vehiculo)){
                                //si no se encuentra en ninguna cola crear complemento y saltar a la siguiente iteraccion CONTINUE
                                $pedidoVehiculoCarga = PedidoVehiculoCarga::find($pedido->pvc_id);
                                $pedidoVehiculoCarga->complemento = 1;
                                $pedidoVehiculoCarga->save();
                                continue 2;
                            }
                        }
                    }
                    break;
            }

            //cuando existe un vehiculo en cualquier cola crear programacion
            //verificar si existe una programacion registrada con la placa
            //la placa no debe estar en una programacion activa compara en programaciones
            // que vehiculo_id y pedidos_vehiculo_carga_id no esten en la tabla

            //validar que el vehiculo no este en una programacion con fecha mayor o igual a hoy
            //ojo no funciona se deberia tener una cola de placas validadas sin errores ni duplicados
            // dd($pedido);
            // dd($vehiculo);
            //verificar si pedido_destino_id es iguial, entonces es el mismo pedido
            if($pedido->pedidos_destino_id != $pedidos_destino_id){
                $pedidos_destino_id = $pedido->pedidos_destino_id;
                $restoFaltaSumado = 0;
            }

            try {
                DB::beginTransaction();
                $fecha_programacion = Carbon::createFromFormat('Y-m-d', $fechaProgramacion);
                $cantidad = $pedido->carga_vehiculo;
                //el vehiculo elegido llega con una capacidad igual o mayor al solicitado
                if($tipoPago == 'PA'){
                    $cantidad = $this->calcularCantidadBolsas($restoFaltaSumado,$vehiculo->capacidad_arrastre, $pedido->carga_vehiculo);
                }
                $programa = Programacion::create([
                                            'codigo' => uniqid('pro-', true),
                                            'fecha' => $fecha_programacion->format('Y-m-d H:i:s.u'),
                                            'cantidad' => $cantidad,
                                            'tipo_asignacion' => 'Normal',
                                            'user_id' => Auth::id(),
                                            'pedidos_vehiculo_carga_id' => $pedido->pvc_id,
                                            'vehiculo_id' => $vehiculo->vehiculo_id
                                        ]);
                // dd($programa);
                // en colas preasignado=1 y en conocimiento_carga carga_asignada=1
                $cola = Cola::find($vehiculo->cola_id);
                $cola->preasignado = 1;
                $cola->save();
                $conocimientoCarga = ConocimientoCarga::find($vehiculo->conocimiento_carga_id);
                $conocimientoCarga->carga_asignada = 1;
                $conocimientoCarga->save();
                //cambiar pedidos_destino, pedidos_vehiculos_carga
                $pedidoVehiculoCarga = PedidoVehiculoCarga::find($pedido->pvc_id);
                $pedidoVehiculoCarga->asignado = 1;
                $pedidoVehiculoCarga->save();
                //verificar si todos los PedidosVehiculoCarga fueron asignados para modificar atendido=1 en pedidos_destino
                $this->verificarPedidoAtendido($pedidoVehiculoCarga->pedidos_destino_id);
                DB::commit();
                // session()->flash('message', 'Se habilitó la placa: ');
            } catch (\Exception $e) {
                DB::rollback();
                dd($e->getMessage());
                // session()->flash('message', $e->getMessage());
            }

        }
    }
    /** obtener la cantidad a programar si tipo de pago es PA
     * considerar que maximo de carga solicitada en el pedido
     * si la carga es mayor a la capacidad de arrastre del vehiculo
     * asiganar la capacidad de arrastre y sumar la carga no asignada a resto_faltante_entrega
     * si la capacidad de arrastre es mayor a la carga entonces verificar si existe resto_faltante_entrega
     * si resto_faltante_es contiene a la diferencia entre capacidad_de arrastre y carga entonces
     * sumar a la cantidad la diferencia entre capacidad_arraste y carga y restr la diferencia a resto_faltante
     * si resto_faltante es menor a la diferencia entre capacidad_arraste y carga sumar a la cantidad el resto fantalte
     * y porner resto faltante igual a cero
     *
     */

    function calcularCantidadBolsas(&$restoFaltaSumado,$capArrastre, $cargaVehiculo){
        // $cargaTotal, no se necesita saber el total de la carga porque se aumenta carga solo si hay restoFaltaSumado
        if($cargaVehiculo == $capArrastre){
            return $cargaVehiculo;
        }
        if($cargaVehiculo > $capArrastre){
            $restoFaltaSumado += $cargaVehiculo-$capArrastre;
            return $capArrastre;
        }
        //capacidadArrastre mayor a la carga solicitada del vehiculo
        $diffCantidad = $capArrastre - $cargaVehiculo;
        if($restoFaltaSumado >= $diffCantidad){
            $restoFaltaSumado = $restoFaltaSumado - $diffCantidad;
            return $capArrastre;
        }
        else{
            //restoSumado es menor a la diferencia de carga
            $cantidad = $cargaVehiculo + $restoFaltaSumado;// la carga vehiculo sera menor a la capacidad de arraste
            $restoFaltaSumado = 0;
            return $cantidad;
        }
    }

    /**
     * verificar si todos los PedidosVehiculoCarga fueron asignados para modificar atendido=1 en pedidos_destino
     **/
    function verificarPedidoAtendido($pedidosDestinoId){
        $cantidad = DB::table('pedidos_vehiculos_carga as pvc')
                        ->where('pvc.asignado', 0)
                        ->where('pvc.pedidos_destino_id', $pedidosDestinoId)
                        ->count();
        // dd($cantidad);
        if($cantidad == 0){
            $pedidoDestino = PedidosDestino::find($pedidosDestinoId);
            $pedidoDestino->atendido = 1;
            $pedidoDestino->save();
        }

    }
    /**
     * busca un vehiculo que tenga el tipo vehiculo requerido
     * que la capacidad de arrastre cumpla con la carga
     * y si seleccionar un capacidad de arraste igual a la carga exacta
     */
    function buscarVehiculoEnCola($tipoCola,$tipoVehiculoId, $carga, $tipoPago){
        //buscamos todos los vehiculos habiltados por tipo de cola, una vez encontrado se cambia estadopreasignado = 1 en la cola
        $placasHabilitadas = DB::table('colas as c')
                                ->select('c.id as cola_id', 'c.vehiculo_id', 'v.placa', 'v.tipo_vehiculo_id', 'v.capacidad_arrastre', 'cc.fecha_carguio', 'cc.id as conocimiento_carga_id', 'cc.cias_habilitar_placas_id')
                                ->join('vehiculos as v', 'c.vehiculo_id', '=', 'v.id')
                                ->join('conocimiento_cargas as cc', 'c.conocimiento_carga_id', '=', 'cc.id')
                                ->where('c.tipo_cola', $tipoCola)
                                ->where('v.tipo_vehiculo_id', $tipoVehiculoId)
                                ->where('c.preasignado', '0')
                                ->orderBy('cc.fecha_carguio', 'asc')
                                ->get();
        // dd($placasHabilitadas);
        if(count($placasHabilitadas)>0){
            if($tipoPago == 'PS'){
                foreach ($placasHabilitadas as $vehiculo) {
                    //considerando que las cargas por tipo de vehiculo nunca exederan la capacidad de arraste maxima
                    if($vehiculo->capacidad_arrastre >= $carga){
                        return $vehiculo;
                    }
                }
            }else{
                foreach ($placasHabilitadas as $vehiculo) {
                    //retornamos cada vehiculo ya que es el tipo requerido y puede llevar la carga que se solicita o menos de ella
                    //en todo caso la carga no debe ser mayor a la capacidad de arrastre
                    // if($vehiculo->capacidad_arrastre >= $carga){
                        return $vehiculo;
                    // }
                }
            }


        }
        return null;
    }
    /**
     * busca todos los pedidos destinos por tipo destino, tipo pago
     * ademas trae la cantidad de tipos de vehiculos necesarios para cumplir el pedido
     */
    function buscarPedidosDestinoNumVehiculos($tipoPago, $tipoDestino,$fechaProgramacion){
        // dd($fechaProgramacion);
        return DB::table('destinos as d')
        ->select('d.tipo_destino', 'pd.fecha_pedido', 'pd.tipo_pago', 'pd.destino_id', 'pvc.carga_vehiculo', 'pvc.pedidos_destino_id', 'pvc.tipo_vehiculo_id', 'pvc.id as pvc_id')
        ->join('pedidos_destino as pd', 'pd.destino_id', '=', 'd.id')
        ->join('pedidos_vehiculos_carga as pvc', 'pvc.pedidos_destino_id', '=', 'pd.id')
        ->where('d.tipo_destino', $tipoDestino)
        ->where('pd.tipo_pago', $tipoPago)
        ->where('pd.fecha_pedido', '<=', $fechaProgramacion) //pedidos que sean menores o igual a la fecha ingresada por el usuario
        ->where('pd.atendido', 0)
        ->where('pvc.asignado', 0)
        ->orderBy('pd.fecha_pedido', 'asc')
        ->get();
    }

    function reportes() {
        return view('operador.reportes');
    }

    public function verProgramacion(Request $request, $fecha)
    {
        // dd([$request->all(),$fecha]);
        if(count($request->all()) > 0){
            $fecha = $request->input('fecha_pro');
        }
        // dd($fecha);

        $vehiculosProgramados = DB::table('pedidos_destino as pd')
                    ->select('d.nombre_destino', 'pd.fecha_pedido', 'p.fecha', 'pd.tipo_pago', 'tv.tipo', 'p.cantidad', 'v.placa')
                    ->leftJoin('pedidos_vehiculos_carga as pvc', 'pd.id', '=', 'pvc.pedidos_destino_id')
                    ->leftJoin('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
                    ->leftJoin('programaciones as p', 'pvc.id', '=', 'p.pedidos_vehiculo_carga_id')
                    ->join('vehiculos as v', 'p.vehiculo_id', '=', 'v.id')
                    ->join('destinos as d', 'pd.destino_id', '=', 'd.id')
                    ->where( DB::raw("DATE(p.fecha)"), '=', DB::raw("DATE('$fecha')"))
                    ->orderBy('pd.fecha_pedido')
                    ->orderBy('pd.tipo_pago')
                    ->get();

        // dd($vehiculosProgramados);
        return view('operador.ver-programacion')->with(compact('fecha','vehiculosProgramados'));
    }

    public function verComplemento(Request $request, $fecha) {
        if(count($request->all()) > 0){
            $fecha = $request->input('fecha_pro');
        }
        $listaComplementos = DB::select("SELECT
        pedido_destino_id,
        nombre_destino,
        fecha_pedido,
        cantidad,
        tipo_pago,
        SUM(CASE WHEN tipo = 'TRAILER' THEN total_tipo_vehiculo ELSE 0 END) AS Trailers,
        SUM(CASE WHEN tipo = 'DIRECCIONAL' THEN total_tipo_vehiculo ELSE 0 END) AS Direccionales,
        SUM(CASE WHEN tipo = 'ALZAPATA' THEN total_tipo_vehiculo ELSE 0 END) AS Alzapatas,
        SUM(CASE WHEN tipo = 'SENCILLO' THEN total_tipo_vehiculo ELSE 0 END) AS Sencillos
    FROM
    (
        SELECT
            pd.id AS pedido_destino_id,
            d.nombre_destino,
            pd.fecha_pedido,
            pd.cantidad,
            pd.tipo_pago,
            tv.tipo,
            COUNT(tv.id) AS total_tipo_vehiculo
        FROM
            pedidos_destino pd
        LEFT JOIN
            pedidos_vehiculos_carga pvc ON pd.id = pvc.pedidos_destino_id
        LEFT JOIN
            tipo_vehiculos tv ON pvc.tipo_vehiculo_id = tv.id
        INNER JOIN
            destinos d ON pd.destino_id = d.id
        WHERE pd.atendido = 0
        and pvc.asignado = 0
        and pvc.complemento  = 1
        AND NOT EXISTS (
            SELECT 1
            FROM programaciones p
            WHERE p.pedidos_vehiculo_carga_id = pvc.id
        )
        GROUP BY
            pd.id, d.nombre_destino, pd.fecha_pedido, pd.cantidad, pd.tipo_pago, tv.tipo
    ) AS src
    GROUP BY
        pedido_destino_id, nombre_destino, fecha_pedido, cantidad, tipo_pago
    ORDER BY
        fecha_pedido, pedido_destino_id, tipo_pago ASC");
        return view('operador.ver-complemento')->with(compact('fecha','listaComplementos'));

    }

    function publicarComplemento(Request $request){
        // dd($request->all());
        // modificar para considerar no modificar pedidos anteriores
        $filas = DB::table('pedidos_vehiculos_carga')
                    ->where('complemento', 1)
                    ->where('publicar', 0)
                    ->update(['publicar' => 1, 'fecha_programacion' => $request->get('fecha_programacion')]);
        //complementos
        // return view('operador.escuchar_complementos')->with(compact('filas', 'listaComplementos'));
        session()->flash('message', 'Se publicaron '. $filas . ' registros.');
        return redirect()->route('ope.ver.complemento.publicado');
    }

    function verComplementoPublicado($filas=0){
        $listaComplementos = DB::table('pedidos_vehiculos_carga as pvc')
                            ->leftJoin('programaciones as p', 'pvc.id', '=', 'p.pedidos_vehiculo_carga_id')
                            ->where('pvc.complemento', 1)
                            // ->where('pvc.publicar', 0)
                            ->whereNull('p.pedidos_vehiculo_carga_id')
                            ->selectRaw('pvc.tipo_vehiculo_id, count(pvc.tipo_vehiculo_id) as cantidad')
                            ->groupByRaw('pvc.tipo_vehiculo_id')
                            ->get();
        $listaVehiculosEnviados = DB::table('pedidos_vehiculos_carga as pvc')
                            ->leftJoin('programaciones as p', 'pvc.id', '=', 'p.pedidos_vehiculo_carga_id')
                            ->where('pvc.complemento', 1)
                            // ->where('pvc.publicar', 0)
                            ->whereNotNull('p.pedidos_vehiculo_carga_id')
                            ->selectRaw('pvc.tipo_vehiculo_id, count(pvc.tipo_vehiculo_id) as cantidad')
                            ->groupByRaw('pvc.tipo_vehiculo_id')
                            ->get();
        $tipoVehiculos = TipoVehiculo::get()->pluck('tipo','id')->toArray();
        return view('operador.escuchar_complementos')->with(compact('filas', 'listaComplementos','tipoVehiculos','listaVehiculosEnviados'));
    }
}

<?php

namespace App\Http\Controllers\ExcelExport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserExcelController extends Controller
{
    //
    public function exportUsers() {
        return Excel::download(new UsersExport, 'usuarios.xlsx');
    }
}

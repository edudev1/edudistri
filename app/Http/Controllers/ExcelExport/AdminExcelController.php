<?php

namespace App\Http\Controllers\ExcelExport;

use App\Exports\Cias\PlacasHabilitadasCiasExport;
use App\Exports\Colas\ColasExport;
use App\Exports\Pedidos\PedidosExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AdminExcelController extends Controller
{
    public function exportColas() {
        return Excel::download(new ColasExport(), 'colas.xlsx');
    }

    public function exportPedidos() {
        return Excel::download(new PedidosExport(), 'pedidos.xlsx');
    }

    public function exportPlacasHabilitadasSindicato($cia_id) {
        $datos['cia_id'] = $cia_id;
        return Excel::download(new PlacasHabilitadasCiasExport($datos), 'placasHabilitadas.xlsx');
    }
}

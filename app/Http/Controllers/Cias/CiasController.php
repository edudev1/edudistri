<?php

namespace App\Http\Controllers\Cias;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CiasController extends Controller
{
    function index() {
        // $cia = Auth::user()->transporteCias->first();
        return view('cias.index');
    }
    function historialEnvios() {
        return view('cias.historial_envios');
    }

    function historialEnviosBuscar(Request $request){
        // dd($request->all());
        $fechaIni = $request->get('fecha_ini');// Carbon::createFromFormat('Y-m-d', $request->get('fecha_ini'));
        $fechaFin = $request->get('fecha_fin');
        if(is_null($fechaFin)){
            $fechaFin = $fechaIni;
        }else{
            $fechaFin = Carbon::createFromFormat('Y-m-d', $fechaFin);
        }
        $listaPlacas = DB::table('cias_habilitar_placas as chp')
                        // ->select('*')
                        ->selectRaw('chp.id, chp.placa, chp.enviado, chp.programado, chp.cia_id, date(chp.fecha) AS fecha')
                        ->where('chp.enviado', 1)
                        ->where('chp.cia_id', session()->get('cia_id'))
                        ->whereBetween('chp.fecha', [$fechaIni, $fechaFin])
                        ->get();
        // dd($listaPlacas);
        return view('cias.historial_envios')->with(compact('listaPlacas'));
    }

    function verVomplementos() {

        return view('cias.ver_complementos');//->with(compact('listaComplementos','tipoVehiculos'))
    }
    public function verPlacasHabilitadas(){
        return view('cias.placas_habilitadas_auxiliar');
    }
    public function buscarPlacasHabilitadas(Request $request) {
        $placasHabilitadas =  DB::table('colas as c')
                                ->select('c.id as cola_id', 'c.tipo_cola', DB::raw("DATE_FORMAT(c.created_at, '%d-%m-%Y') as fecha_habilitado"),
                                    'v.id as vehiculo_id', 'v.placa', 'tv.tipo', 'cc.numero_carga',
                                    DB::raw("DATE_FORMAT(cc.fecha_carguio, '%d-%m-%Y') as fecha_carguio"),
                                    'd.nombre_destino as ultimo_destino', 'tc.id as cia_id', 'tc.nombre')
                                ->join('vehiculos as v', 'c.vehiculo_id', '=', 'v.id')
                                ->join('tipo_vehiculos as tv', 'v.tipo_vehiculo_id', '=', 'tv.id')
                                ->join('conocimiento_cargas as cc', 'c.conocimiento_carga_id', '=', 'cc.id')
                                ->join('destinos as d', 'cc.ultimo_destino_id', '=', 'd.id')
                                ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
                                ->whereDate('c.created_at', '=', '2023-10-23')
                                ->where('tc.id', '=', Session::get('cia_id'))
                                ->get();
        // dd($placasHabilitadas);
        return view('cias.placas_habilitadas_auxiliar')->with(compact('placasHabilitadas'));
    }
}

<?php

namespace App\Http\Controllers\Reportes;

use App\Exports\Programaciones\CiasExport;
use App\Http\Controllers\Controller;
use App\Models\TransporteCia;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportesProgramacionController extends Controller
{
    public function programacionesXSindicatoView(){
        $sindicatos = TransporteCia::orderBy('nombre')->get()->pluck('nombre','id');
        return view('reportes.programaciones.rpt_x_sindicato')->with(compact('sindicatos'));
    }

    public function programacionesXSindicato(Request $request) {
        // dd($request->all());
        $sindicato = TransporteCia::findOrFail($request->sindicato);
        $fecha = $request->fecha;
        if($request->tipo_rpt === 'pdf'){
            $programacionesXCia = $this->buscarProgramacionesXCias($sindicato->id, $fecha);
            return PDF::loadView('reportes.programaciones.rpt_programacion_x_cias',['programacionesXCia' => $programacionesXCia, 'fecha' => $fecha, 'sindicato'=>$sindicato])->download($sindicato->nombre .' '.$fecha.'.pdf');
        }else {
            return Excel::download(new CiasExport(['fecha'=>$fecha, 'cia_id'=>$sindicato->id]), $sindicato->nombre .' '.$fecha.'.xlsx');
        }
        // return back()->with('message', '¡Reporte en proceso de creación!');
    }

    function buscarProgramacionesXCias($cia_id, $fecha)  {
        return DB::table('programaciones as p')
        ->join('pedidos_vehiculos_carga as pvc', 'p.pedidos_vehiculo_carga_id', '=', 'pvc.id')
        ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
        ->join('destinos as d', 'pd.destino_id', '=', 'd.id')
        ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
        ->join('vehiculos as v', 'p.vehiculo_id','=','v.id')
        ->join('transportistas as t', 'v.transportista_id', '=', 't.id' )
        ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
        ->select('tc.nombre as nombre_cia', 'v.placa', DB::raw("concat_ws('-', tv.tipo, v.marca, v.color) as vehiculo"), DB::raw("concat_ws(' ', t.nombres, t.apellidos) as transportista"), 'p.cantidad', 'd.nombre_destino as destino')
        ->where('p.anulado', 0)
        ->whereDate('p.fecha', $fecha)
        ->where('tc.id',$cia_id)
        ->orderBy('tc.nombre', 'asc')
        ->orderBy('d.nombre_destino', 'asc')
        ->get();
    }

}

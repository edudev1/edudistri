<?php

namespace App\Http\Controllers\Reportes;

use App\Exports\Programaciones\ProgramacionAllExport;
use App\Http\Controllers\Controller;
use App\Models\Cola;
use App\Models\TransporteCia;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{
    function programacion() {

        return view('reportes.programacion');
    }

    function colasTipo($tipo) {
        $tipoCola = $tipo;
        $colaVehiculos = Cola::select('vehiculos.placa', DB::raw("CONCAT_WS(' ', transportistas.nombres, transportistas.apellidos) AS full_name"), 'vehiculos.capacidad_arrastre', 'transporte_cias.nombre', 'conocimiento_cargas.fecha_carguio')
                            ->join('vehiculos', 'colas.vehiculo_id', '=', 'vehiculos.id')
                            ->join('transportistas', 'vehiculos.transportista_id', '=', 'transportistas.id')
                            ->join('transporte_cias', 'vehiculos.transporte_cia_id', '=', 'transporte_cias.id')
                            ->join('conocimiento_cargas', 'colas.conocimiento_carga_id', '=', 'conocimiento_cargas.id')
                            ->where('colas.tipo_cola', $tipoCola)
                            ->where('colas.asignado', 0)
                            ->orderBy('conocimiento_cargas.fecha_carguio', 'asc')
                            ->get();
        // dd($colaVehiculos->first());
        return PDF::loadView('reportes.rpt_colas',['tipoCola'=>$tipoCola, 'colaVehiculos' => $colaVehiculos])->stream('colas'.$tipo.'.pdf');
        // return view('reportes.rpt_colas')->with(compact('tipoCola','colaVehiculos'));
    }

    function imprimirProgramacion($fecha){
        $programaciones = DB::table(function ($subquery) use ($fecha) {
            $subquery->select(
                'pd.id AS pedido_destino_id',
                'd.nombre_destino',
                DB::raw('DATE(p.fecha) as fecha'),
                DB::raw('SUM(p.cantidad) as cantidad'),
                'pd.tipo_pago',
                'tv.tipo',
                DB::raw('COUNT(tv.id) AS total_tipo_vehiculo')
            )
            ->from('programaciones as p')
            ->join('pedidos_vehiculos_carga as pvc', 'p.pedidos_vehiculo_carga_id', '=', 'pvc.id')
            ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
            ->join('destinos as d', 'pd.destino_id', '=', 'd.id')
            ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
            ->where('p.anulado', 0)
            ->whereDate('p.fecha', $fecha)
            ->groupBy('pd.id', 'd.nombre_destino', 'p.fecha', 'p.cantidad', 'pd.tipo_pago', 'tv.tipo');
        }, 'src')
        ->select(
            'src.pedido_destino_id',
            'src.nombre_destino',
            DB::raw('DATE(src.fecha) as fecha'),
            DB::raw('SUM(src.cantidad) as sum_cantidad'),
            'src.tipo_pago',
            DB::raw('SUM(CASE WHEN src.tipo = \'SENCILLO\' THEN src.total_tipo_vehiculo ELSE 0 END) AS Sencillos'),
            DB::raw('SUM(CASE WHEN src.tipo = \'ALZAPATA\' THEN src.total_tipo_vehiculo ELSE 0 END) AS Alzapatas'),
            DB::raw('SUM(CASE WHEN src.tipo = \'DIRECCIONAL\' THEN src.total_tipo_vehiculo ELSE 0 END) AS Direccionales'),
            DB::raw('SUM(CASE WHEN src.tipo = \'TRAILER\' THEN src.total_tipo_vehiculo ELSE 0 END) AS Trailers')
        )
        ->groupBy('src.pedido_destino_id', 'src.nombre_destino', 'src.fecha', 'src.tipo_pago')
        ->orderBy('src.pedido_destino_id')
        ->orderBy('src.tipo_pago', 'ASC')
        ->get();
        // dd($programaciones );
        // return view('reportes.rpt_programacion')->with(compact('programaciones'));
        return PDF::loadView('reportes.rpt_programacion',['programaciones' => $programaciones, 'fecha' => $fecha])->stream('programacion_'.$fecha.'.pdf');

    }
    function imprimirProgramacionDetalle($fecha){
        $programacionesXCia = Collect();
        $sindicatos = TransporteCia::all();
        foreach ($sindicatos as $sindicato) {
            $programacionesXCia->push($this->buscarProgramacionesXCias($sindicato->id, $fecha));
        }
        // dd($programaciones );
        // return view('reportes.rpt_programacion')->with(compact('programaciones'));
        return PDF::loadView('reportes.rpt_programacion_todos',['programacionesXCia' => $programacionesXCia, 'fecha' => $fecha])->stream('programacion_'.$fecha.'.pdf');

    }

    function buscarProgramacionesXCias($cia_id, $fecha)  {
        return DB::table('programaciones as p')
        ->join('pedidos_vehiculos_carga as pvc', 'p.pedidos_vehiculo_carga_id', '=', 'pvc.id')
        ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
        ->join('destinos as d', 'pd.destino_id', '=', 'd.id')
        ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
        ->join('vehiculos as v', 'p.vehiculo_id','=','v.id')
        ->join('transportistas as t', 'v.transportista_id', '=', 't.id' )
        ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
        ->select('tc.nombre as nombre_cia', 'v.placa', DB::raw("concat_ws('-', tv.tipo, v.marca, v.color) as vehiculo"), DB::raw("concat_ws(' ', t.nombres, t.apellidos) as transportista"), 'p.cantidad', 'd.nombre_destino as destino')
        ->where('p.anulado', 0)
        ->whereDate('p.fecha', $fecha)
        ->where('tc.id',$cia_id)
        ->orderBy('tc.nombre', 'asc')
        ->orderBy('d.nombre_destino', 'asc')
        ->get();
    }

    public function imprimirPedidosPendientes(){
        $pedidosDestino = DB::select("SELECT
                    pedido_destino_id,
                    nombre_destino,
                    fecha_pedido,
                    cantidad,
                    tipo_pago,
                    SUM(CASE WHEN tipo = 'TRAILER' THEN total_tipo_vehiculo ELSE 0 END) AS Trailers,
                    SUM(CASE WHEN tipo = 'DIRECCIONAL' THEN total_tipo_vehiculo ELSE 0 END) AS Direccionales,
                    SUM(CASE WHEN tipo = 'ALZAPATA' THEN total_tipo_vehiculo ELSE 0 END) AS Alzapatas,
                    SUM(CASE WHEN tipo = 'SENCILLO' THEN total_tipo_vehiculo ELSE 0 END) AS Sencillos
                FROM
                (
                    SELECT
                        pd.id AS pedido_destino_id,
                        d.nombre_destino,
                        pd.fecha_pedido,
                        pd.cantidad,
                        pd.tipo_pago,
                        tv.tipo,
                        COUNT(tv.id) AS total_tipo_vehiculo
                    FROM
                        pedidos_destino pd
                    LEFT JOIN
                        pedidos_vehiculos_carga pvc ON pd.id = pvc.pedidos_destino_id
                    LEFT JOIN
                        tipo_vehiculos tv ON pvc.tipo_vehiculo_id = tv.id
                    INNER JOIN
                        destinos d ON pd.destino_id = d.id
                    WHERE pd.atendido = 0
                    and pvc.asignado = 0
                    and pvc.complemento  = 0
                    -- AND NOT EXISTS (
                    --     SELECT 1
                    --     FROM programaciones p
                    --     WHERE p.pedidos_vehiculo_carga_id = pvc.id
                    -- )
                    GROUP BY
                        pd.id, d.nombre_destino, pd.fecha_pedido, pd.cantidad, pd.tipo_pago, tv.tipo
                ) AS src
                GROUP BY
                    pedido_destino_id, nombre_destino, fecha_pedido, cantidad, tipo_pago
                ORDER BY
                    fecha_pedido, pedido_destino_id, tipo_pago ASC");
        // return view('reportes.rpt_pedidos_pendientes')->with(compact('pedidosDestino'));
        return PDF::loadView('reportes.rpt_pedidos_pendientes',['pedidosDestino' => $pedidosDestino])->stream('pedidos_destino'.now()->format('d-m-Y').'.pdf');


    }

    public function programacionExcel($fecha){

        return Excel::download(new ProgramacionAllExport(['fecha'=>$fecha]), 'programacion todos '.$fecha.'.xlsx');
    }
}

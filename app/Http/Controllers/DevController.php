<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DevController extends Controller
{
public function borrarDatosTablas() {

    $tablas = [
        'programaciones'
        ,'pedidos_vehiculos_carga'
        ,'pedidos_destino'
        ,'conocimiento_cargas'
        ,'cias_habilitar_placas'
        ,'colas'
    ];
    DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
    DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    session()->flash('message', 'Datos borrados correctamente.');
    return redirect('/dashboard');
}
}

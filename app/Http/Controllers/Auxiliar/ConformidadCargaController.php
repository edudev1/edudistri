<?php

namespace App\Http\Controllers\Auxiliar;

use App\Http\Controllers\Controller;
use App\Models\ConocimientoCargaJde;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConformidadCargaController extends Controller
{
    function verConformidad() {
        return view('auxiliar.ver_conformidad');
    }

    function buscarConformidad(Request $request) {
        // dd($request->all());
        $fuente =  $request->fuente;
        $buscar_por =  $request->buscar_por;
        $buscado = $request->search;
        if($fuente == "sdc"){
            $datos = $this->buscarConocimientosSDC($buscar_por, $buscado);
        }else { //jde
            $datos = $this->buscarConocimientosJDE($buscar_por, $buscado);
        }
        return view('auxiliar.ver_conformidad')->with(compact('datos'));
    }

    private function buscarConocimientosJDE($buscar_por, $buscado) {
        $query = ConocimientoCargaJde::query();
        if($buscar_por == "placa"){
            $query->where('placa', $buscado);
        }else //conformidad
        {
            $query->where('numero_carga', $buscado);
        }
        $query->orderBy('fecha', 'desc');
        return $query->get();
    }

    private function buscarConocimientosSDC($buscar_por, $buscado) {
        $query = DB::table('conocimiento_cargas as cc')
                    ->join('destinos as d','cc.ultimo_destino_id', '=', 'd.id' )
                    ->select('cc.numero_carga', 'd.nombre_destino as ciudad', 'cc.fecha_carguio as fecha', 'cc.placa');
        // dd($query->get());
        if($buscar_por == "placa"){
            $query->where('placa', $buscado);
        }else //conformidad
        {
            $query->where('numero_carga', $buscado);
        }
        return $query->where('habilitado', '1')
                     ->orderBy('fecha_carguio', 'desc')
                     ->get();
    }
}

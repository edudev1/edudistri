<?php

namespace App\Http\Controllers\Auxiliar;

use App\Http\Controllers\Controller;
use App\Models\CiasHabilitarPlaca;
use App\Models\Cola;
use App\Models\ConocimientoCarga;
use App\Models\TransporteCia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuxiliarController extends Controller
{
    function index() {
        $listaPlacasCias = DB::table('cias_habilitar_placas as chp')
                               ->join('vehiculos as v', 'chp.placa', '=', 'v.Placa' )
                               ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
                               ->where('chp.enviado', '1')
                               ->where('chp.programado', '0')
                               ->select('tc.id','tc.nombre', DB::raw('count(*) as cantidad'))
                               ->groupBy('tc.id','tc.nombre')
                               ->get();
        // dd($placaVehiculoCias);
        // $listaPlacasRevisar = CiasHabilitarPlaca::where('enviado', '1')
        //                                         ->where('programado', 0)
        //                                         ->get();
        return view('auxiliar.index')->with(compact('listaPlacasCias'));
    }

    function verPlacasCompania(TransporteCia $cia) {
        $placasCia = DB::table('cias_habilitar_placas as chp')
                        ->select('chp.id as chp_id', 'chp.fecha', 'chp.placa', 'chp.cia_id', 'chp.conocimiento_enviado', 'c.id as cola_id', 'c.tipo_cola', 'c.created_at as cola_fecha', 'c.asignado', 'c.preasignado')
                        ->join('transporte_cias as tc', 'chp.cia_id', '=', 'tc.id')
                        ->join('vehiculos as v', function ($join) {
                            $join->on('tc.id', '=', 'v.transporte_cia_id')
                                ->on('chp.placa', '=', 'v.placa');
                        })
                        ->leftJoin('colas as c', 'v.id', '=', 'c.vehiculo_id')
                        ->where('chp.enviado', '1')
                        ->where('chp.programado', '0')
                        ->where('tc.id', $cia->id)
                        ->get();
        // dd($placasCia);
    return view('auxiliar.ver_placas_habilitar')->with(compact('placasCia', 'cia'));
    }



    function colas() {
        $colaUVS = $this->buscarCola('UVS');
        $colaUVI = $this->buscarCola('UVI');
        $colaUVP = $this->buscarCola('UVP');
        return view('auxiliar.colas')->with(compact('colaUVS','colaUVI','colaUVP'));
    }

    protected function buscarCola($tipoCola) {
        return Cola::select('colas.id', 'colas.tipo_cola', 'colas.fecha', 'colas.asignado', 'colas.preasignado', 'colas.conocimiento_carga_id', 'transporte_cias.nombre', 'vehiculos.placa', 'vehiculos.capacidad_arrastre as capacidad')
        ->join('vehiculos', 'colas.vehiculo_id', '=', 'vehiculos.id')
        ->join('transporte_cias', 'vehiculos.transporte_cia_id', '=', 'transporte_cias.id')
        ->where('colas.tipo_cola', $tipoCola)
        ->where('colas.asignado', '0')
        ->where('colas.preasignado', '0')
        ->orderBy('colas.fecha', 'asc')
        ->get();
    }

    public function deletePlacaCola(Cola $cola) {
        /**
         * 1.- cias_habilitar_placas->programado = 1;
         * 2.- eliminar conocimiento de carga
         * 3.- eliminar cola
         */
        DB::beginTransaction();
        try {
            $conocimientoCarga = ConocimientoCarga::find($cola->conocimiento_carga_id);
            $placaEliminar = $conocimientoCarga->placa;
            $cias_habilitar_placas = CiasHabilitarPlaca::find($conocimientoCarga->cias_habilitar_placas_id);
            $cias_habilitar_placas->programado = 0;
            $cias_habilitar_placas->save();
            $cola->delete();
            $conocimientoCarga->delete();
            DB::commit();
            session()->flash('message', 'Se elimino la placa: '.$placaEliminar );
            // $this->emit('closeModal');
        } catch (\Exception $e) {
            // En caso de error, deshacemos la transacción $e->getMessage()
            DB::rollback();
            session()->flash('message', 'Ocurrió un error! No se puede eliminar la placa '.$placaEliminar . ' de la cola ' . $e->getMessage() );

        }
        return redirect()->route('aux.colas.index');
    }

    public function confirmarPlacaEnviada(Request $request){
        // dd($request->all());
        $chp = CiasHabilitarPlaca::findOrFail($request->chp_id);
        if($chp->placa === $request->placa){
            $chp->programado = 1;
            $chp->save();
            session()->flash('message', 'Se volvio a confirmar la placa: '.$chp->placa  );
        }
        return redirect()->route('aux.placas.compania', [$chp->cia_id]);
    }

    public function eliminarPlacaEnviada(Request $request) {
        $ciasHabiPlaca = CiasHabilitarPlaca::findorFail($request->chp_id);
        $ciasHabiPlaca->delete();
        session()->flash('message', 'Se eliminó la placa: '.$ciasHabiPlaca->placa  );
        return redirect()->route('aux.placas.compania', [$ciasHabiPlaca->cia_id]);
    }


}

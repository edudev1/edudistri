<?php

namespace App\Providers;

use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Schema::defaultStringLength(191);

        Event::listen(Authenticated::class, function ($event) {
            // Obtén el usuario autenticado
            $user = $event->user;
            // dd($user->myRol());
            if($user->myRol() == 'Sindicato'){
                $cia = $user->transporteCias->first();
                session(['cia_id' => $cia->id]);
                session(['cia_name' => $cia->nombre]);
                session(['cia_dir' => $cia->direccion]);
                session(['cia_tel' => $cia->num_telefono]);
                session(['cia_email' => $cia->email]);
            }
        });

    }
}

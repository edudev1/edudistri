<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConocimientoCarga extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $table = 'conocimiento_cargas';
    protected $dates = ['fecha_carguio'];

    protected $fillable = [
        'numero_carga',
        'placa',
        'fecha_carguio',
        'habilitado',
        'ultimo_destino_id',
        'cias_habilitar_placas_id'
    ];
}

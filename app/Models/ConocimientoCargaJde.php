<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConocimientoCargaJde extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'conocimiento_cargas_jde';
    protected $dates = ['fecha'];

    protected $fillable = [
        'numero_carga',
        'ciudad',
        'fecha',
        'placa',
    ];
}

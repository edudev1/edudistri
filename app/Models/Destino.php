<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
	use HasFactory;

    public $timestamps = false;

    protected $table = 'destinos';

    protected $fillable = ['tipo_destino','nombre_destino','departamento'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidosDestinos()
    {
        return $this->hasMany('App\Models\PedidosDestino', 'destino_id', 'id');
    }

}

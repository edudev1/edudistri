<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programacion extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $table = 'programaciones';
    protected $dates = ['fecha'];
    protected $fillable = [
                            'codigo',
                            'fecha',
                            'cantidad',
                            'tipo_asignacion',
                            'user_id',
                            'pedidos_vehiculo_carga_id',
                            'vehiculo_id'
                        ];







}

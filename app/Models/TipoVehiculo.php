<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoVehiculo extends Model
{
	use HasFactory;

    public $timestamps = false;

    protected $table = 'tipo_vehiculos';
    // protected $dates = ['']
    protected $fillable = ['tipo','descripcion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'tipo_vehiculo_id', 'id');
    }

}

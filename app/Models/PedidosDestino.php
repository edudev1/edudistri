<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidosDestino extends Model
{
	use HasFactory;

    public $timestamps = false;

    protected $table = 'pedidos_destino';
    protected $dates = ['fecha_pedido'];
    protected $fillable = ['fecha_pedido','tipo_pago','cantidad','destino_id'];


    public function getFechaPedidoAttribute()
    {
        return Carbon::parse($this->attributes['fecha_pedido']);//->format('Y-m-d')
    }

    public function destino()
    {
        return $this->hasOne(Destino::class, 'id', 'destino_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidosVehiculosCargas()
    {
        return $this->hasMany(PedidoVehiculoCarga::class, 'pedidos_destino_id', 'id');
    }

    public function scopeComplemento(Builder $query, $complemento): void
    {
        $query->where('complemento', $complemento);
    }

}

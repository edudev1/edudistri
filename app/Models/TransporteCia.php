<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransporteCia extends Model
{
	use HasFactory;

    public $timestamps = true;
    // protected $dates = ['created_at'];
    protected $table = 'transporte_cias';
    protected $fillable = ['nombre','direccion','num_telefono','email','created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'transporte_cia_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_transporte_cias', 'transporte_cias_id', 'user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CiasHabilitarPlaca extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'cias_habilitar_placas';
    protected $dates = ['fecha'];

    protected $fillable = [
        'placa',
        'cia_id',
        'enviado',
        'conocimiento_enviado',
    ];
}

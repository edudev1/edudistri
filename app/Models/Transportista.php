<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
	use HasFactory;

    public $timestamps = true;

    protected $table = 'transportistas';
    protected $dates = ['created_at'];
    protected $fillable = ['ci','nit','nombres','apellidos','num_celular','num_celular_ref','direccion','created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'transportista_id', 'id');
    }

    public function getFullNameAttribute()
    {
        return $this->nombres . ' ' . $this->apellidos;
    }

}

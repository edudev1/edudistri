<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cola extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $table = 'colas';
    protected $dates = ['fecha'];

    protected $fillable = [
        'tipo_cola',
        'fecha',
        'vehiculo_id',
        'observacion',
        'conocimiento_carga_id',
        ];
}

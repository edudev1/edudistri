<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidoVehiculoCarga extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'pedidos_vehiculos_carga';
    protected $dates = ['fecha_pedido'];
    protected $fillable = [
                        'carga_vehiculo',
                        'pedidos_destino_id',
                        'tipo_vehiculo_id',
                        ];

    public function pedidoDestino()
    {
        return $this->belongsTo(PedidoDestino::class);
    }

}

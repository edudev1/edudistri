<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Collective\Html\Eloquent\FormAccessible;

class Vehiculo extends Model
{
	use HasFactory;
    // use App\Models\FormAccessible;

    public $timestamps = false;

    protected $table = 'vehiculos';
    protected $dates = ['fecha_registro'];

    protected $fillable = ['placa','marca','color','num_chasis','codigo_motor','capacidad_arrastre','crpva','num_ci_propietario','fecha_registro','tipo_vehiculo_id','transporte_cia_id','transportista_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tipoVehiculo()
    {
        return $this->hasOne('App\Models\TipoVehiculo', 'id', 'tipo_vehiculo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transporteCia()
    {
        return $this->hasOne('App\Models\TransporteCia', 'id', 'transporte_cia_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transportista()
    {
        return $this->hasOne('App\Models\Transportista', 'id', 'transportista_id');
    }

}

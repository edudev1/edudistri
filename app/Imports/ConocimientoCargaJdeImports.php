<?php

namespace App\Imports;

use App\Models\ConocimientoCargaJde;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ConocimientoCargaJdeImports implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row['fecha']);
        $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha']);
        // dd($date);
        return new ConocimientoCargaJde([
            'numero_carga' => $row['numero_carga'],
            'ciudad' => $row['ciudad'],
            'fecha' => $date->format('Y-m-d'),
            'placa' => $row['placa'],
        ]);
    }

    // public function bindValue(Cell $cell, $value)
    // {
    //     // Verificar si el valor de la celda es una fecha válida
    //     if (is_numeric($value)) {
    //         $cell->setValueExplicit(Date::excelToDateTimeObject($value)->format('Y-m-d'), Cell::TYPE_STRING2);
    //     }

    //     return parent::bindValue($cell, $value);
    // }
}

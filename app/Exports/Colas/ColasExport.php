<?php

namespace App\Exports\Colas;

use App\Models\Cola;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
// use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ColasExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $parametros;

    function __construct(Array $datos = [])
    {
        $this->parametros = $datos;
    }

    public function headings(): array
    {
        return [
            'TIPO COLA',
            'FECHA',
            'PLACA',
            'BOLSAS',
            'SINDICATO',
        ];
    }

    public function title(): string
    {
        return 'Colas'; // cambiar el nombre de la hoja aquí
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:Z1')->getFont()->setBold(true);
    }

    public function collection()
    {
        return Cola::select('colas.tipo_cola', DB::raw("DATE_FORMAT(colas.fecha, '%d-%m-%Y') as fecha"), 'vehiculos.placa', 'vehiculos.capacidad_arrastre as capacidad', 'transporte_cias.nombre')
                    ->join('vehiculos', 'colas.vehiculo_id', '=', 'vehiculos.id')
                    ->join('transporte_cias', 'vehiculos.transporte_cia_id', '=', 'transporte_cias.id')
                    ->where('colas.asignado', '0')
                    ->where('colas.preasignado', '0')
                    ->orderByRaw("FIELD(colas.tipo_cola, 'UVS', 'UVI', 'UVP')")
                    ->orderBy('colas.fecha', 'asc')
                    ->get();
    }

}

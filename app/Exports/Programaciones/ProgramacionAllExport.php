<?php

namespace App\Exports\Programaciones;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
// use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProgramacionAllExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $parametros;

    function __construct(Array $datos)
    {
        $this->parametros = $datos;
    }

    public function headings(): array
    {
        return [
            'SINDICATO',
            'PLACA',
            'VEHICULO',
            'TRANSPORTISTA',
            'FECHA',
            'TIPO',
            'BOLSAS',
            'DESTINO',
        ];
    }

    public function title(): string
    {
        return 'Programacion'; // Puedes cambiar el nombre de la hoja aquí
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:Z1')->getFont()->setBold(true);
    }

    public function collection()
    {
         return DB::table('programaciones as p')
         ->join('pedidos_vehiculos_carga as pvc', 'p.pedidos_vehiculo_carga_id', '=', 'pvc.id')
         ->join('pedidos_destino as pd', 'pvc.pedidos_destino_id', '=', 'pd.id')
         ->join('destinos as d', 'pd.destino_id', '=', 'd.id')
         ->join('tipo_vehiculos as tv', 'pvc.tipo_vehiculo_id', '=', 'tv.id')
         ->join('vehiculos as v', 'p.vehiculo_id','=','v.id')
         ->join('transportistas as t', 'v.transportista_id', '=', 't.id' )
         ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
         ->select('tc.nombre as nombre_cia', 'v.placa', DB::raw("concat_ws('-', tv.tipo, v.marca, v.color) as vehiculo"), DB::raw("concat_ws(' ', t.nombres, t.apellidos) as transportista"), DB::raw("DATE_FORMAT(p.fecha, '%d-%m-%Y') as fecha"), 'p.tipo_asignacion as tipo', 'p.cantidad', 'd.nombre_destino as destino')
         ->where('p.anulado', 0)
         ->whereDate('p.fecha', $this->parametros['fecha'])
        //  ->where('tc.id',$this->parametros['cia_id'])
         ->orderBy('tc.nombre', 'asc')
         ->orderBy('d.nombre_destino', 'asc')
         ->get();

    }

}

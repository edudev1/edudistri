<?php

namespace App\Exports\Pedidos;

use App\Models\Cola;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
// use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PedidosExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $parametros;

    function __construct(Array $datos = [])
    {
        $this->parametros = $datos;
    }

    public function headings(): array
    {
        return [
            'ID',
            'DESTINO',
            'FECHA',
            'CANTIDAD',
            'TIPO PAGO',
            'TRAILERS',
            'DIRECCIONALES',
            'ALZAPATAS',
            'SENCILLOS',
        ];
    }

    public function title(): string
    {
        return 'Pedidos'; // Puedes cambiar el nombre de la hoja aquí
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:Z1')->getFont()->setBold(true);
    }

    public function collection()
    {
        $result = DB::select("SELECT
                                pedido_destino_id,
                                nombre_destino,
                                DATE_FORMAT(fecha_pedido, '%d-%m-%Y') as fecha_pedido,
                                cantidad,
                                tipo_pago,
                                SUM(CASE WHEN tipo = 'TRAILER' THEN total_tipo_vehiculo ELSE 0 END) AS Trailers,
                                SUM(CASE WHEN tipo = 'DIRECCIONAL' THEN total_tipo_vehiculo ELSE 0 END) AS Direccionales,
                                SUM(CASE WHEN tipo = 'ALZAPATA' THEN total_tipo_vehiculo ELSE 0 END) AS Alzapatas,
                                SUM(CASE WHEN tipo = 'SENCILLO' THEN total_tipo_vehiculo ELSE 0 END) AS Sencillos
                            FROM
                            (
                                SELECT
                                    pd.id AS pedido_destino_id,
                                    d.nombre_destino,
                                    pd.fecha_pedido,
                                    pd.cantidad,
                                    pd.tipo_pago,
                                    tv.tipo,
                                    COUNT(tv.id) AS total_tipo_vehiculo
                                FROM
                                    pedidos_destino pd
                                INNER JOIN
                                    pedidos_vehiculos_carga pvc ON pd.id = pvc.pedidos_destino_id
                                INNER JOIN
                                    tipo_vehiculos tv ON pvc.tipo_vehiculo_id = tv.id
                                INNER JOIN
                                    destinos d ON pd.destino_id = d.id
                                WHERE pd.atendido = 0
                                and pvc.asignado = 0
                                and pvc.complemento  = 0
                                GROUP BY
                                    pd.id, d.nombre_destino, pd.fecha_pedido, pd.cantidad, pd.tipo_pago, tv.tipo
                            ) AS src
                            GROUP BY
                                pedido_destino_id, nombre_destino, fecha_pedido, cantidad, tipo_pago
                            ORDER BY
                                fecha_pedido, pedido_destino_id, tipo_pago ASC");
        // dd(collect($result));
        return collect($result);
    }

}

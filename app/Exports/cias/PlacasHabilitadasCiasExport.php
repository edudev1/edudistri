<?php

namespace App\Exports\Cias;

use App\Models\Cola;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
// use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PlacasHabilitadasCiasExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $parametros;

    function __construct(Array $datos = [])
    {
        $this->parametros = $datos;
    }

    public function headings(): array
    {
        return [
            'NUM',
            'FECHA',
            'PLACA',
            'CAPACIDAD',
            'CELULAR',
            'CONOCIMIENTO CARGA',
        ];
    }

    public function title(): string
    {
        return 'Pedidos'; // Puedes cambiar el nombre de la hoja aquí
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:Z1')->getFont()->setBold(true);
    }

    public function collection()
    {
        $result = DB::table('cias_habilitar_placas as chp')
                ->join('vehiculos as v', 'chp.placa', '=', 'v.placa')
                ->join('transporte_cias as tc', 'v.transporte_cia_id', '=', 'tc.id')
                ->join('transportistas as t', 'v.transportista_id', '=', 't.id')
                ->select(DB::raw('ROW_NUMBER() OVER (ORDER BY chp.id ) AS indice'), DB::raw("DATE_FORMAT(chp.fecha, '%d-%m-%Y') as fecha"), 'chp.placa', 'v.capacidad_arrastre', 't.num_celular', DB::raw('CASE WHEN chp.conocimiento_enviado = 1 THEN "SI" ELSE "NO" END AS cono_carga'))
                ->where('chp.programado', 0)
                ->where('chp.enviado', 1)
                ->where('tc.id', $this->parametros['cia_id'])
                ->get();
        // dd(collect($result));
        return collect($result);
    }

}
